#!/bin/sh

set -e

DIR=`realpath $(pwd)`

#echo "download archive"
#SRC_URL="https://gitlab.com/tiotags/hin10/-/archive/master/hin10-master.tar.bz2"
#curl "$SRC_URL" | tar --strip-components=1 -jx

echo "untar archive"
tar --strip-components=1 -jxf /app/hin10.tar.bz2

echo "applying patch"
touch /app/scripts/fix-implicit.patch
git apply --allow-empty /app/scripts/fix-implicit.patch

echo "building"
export CFLAGS="${CFLAGS} -fdiagnostics-color=always -DBASIC_USE_MUSL"
mkdir -p $DIR/build
cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -B build/
ninja -C build/

echo "initialize files"
#sh external/tests/run.sh external/tests/tests/00_server_present.sh

echo "finished"


