
set -e

tmp_path="$BENCH_BINARY_DIR/temp.bin"
out_dir="$BENCH_HTDOCS_DIR/$BENCH_HTDOCS_TEST_DIR"
medium_path="$out_dir/$TEST_FILE_MEDIUM"
large_path="$out_dir/$TEST_FILE_LARGE"
huge_dir="$out_dir/huge/"
text_path="$out_dir/$TEST_FILE_TEXT"
forbidden_path="$out_dir/forbidden.html"

mkdir -p "$out_dir"

if [ ! -f "$tmp_path" ]; then
  echo "generating random data $tmp_path"
  dd if=/dev/urandom of=$tmp_path bs=1M count=5
fi

if [ ! -f "$medium_path" ]; then
  echo "generating medium file $medium_path"
  for i in {1..3}
  do
    cat $tmp_path >> $medium_path
  done
fi

if [ ! -f "$large_path" ]; then
  echo "generating large file $large_path"
  for i in {1..100}
  do
    cat $tmp_path >> $large_path
  done
fi

if [ ! -d "$huge_dir" ] || [ ! -f "$huge_dir/$TEST_FILE_HUGE_PATH" ]; then
  echo "generating a huge dir $huge_dir for path $TEST_FILE_HUGE_PATH"
  list=${TEST_FILE_HUGE_PATH//\//$'\n'}
  cur_dir=$huge_dir
  salt="asfxnot"
  for cur_name in $list
  do
    mkdir -p $cur_dir
    for j in {1..128}
    do
      name=($(echo "${salt}${j}${salt}" | md5sum -))
      of_path=$cur_dir/$name.bin
      dd if=$tmp_path of=$of_path bs=512 count=1 skip=$j &> /dev/null
    done
    cur_dir="$cur_dir/$cur_name"
  done
  of_path="$huge_dir/$TEST_FILE_HUGE_PATH"
  dd if=$tmp_path of=$of_path bs=512 count=1 &> /dev/null
fi

if [ ! -f "$text_path" ]; then
  echo "generating jquery copy $text_path"
  curl -o $text_path https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js
fi

if [ ! -f "$forbidden_path" ]; then
  echo "generating forbidden $forbidden_path"
  echo "Hello world" > $forbidden_path
  chmod -rwx $forbidden_path
  #chown nobody:nobody $forbidden_path
fi


