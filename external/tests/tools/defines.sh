

start_test () {
  printf "\n***\n"
  printf "started module $1\n$2\n"
  printf "***\n\n"
  export module=$1
  module_desc=$2
  out_path=$BENCH_BINARY_DIR/${name}_${module}.bin
}

end_test () {
  echo "finished module $module"
}

check_error () {
  RET_CODE=$?
  echo "return was"
  echo "$RET"
  echo "***"
  if echo "$RET" | grep "HTTP/1.. $ERROR_CODE " > /dev/null; then
    echo "found proper return code $ERROR_CODE"
  else
    echo "command didn't find the required error code $ERROR_CODE"
    exit 1
  fi
}
