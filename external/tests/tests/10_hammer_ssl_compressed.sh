
export BENCH_CON=$(($BENCH_CON/100))
export BENCH_NUM=$(($BENCH_NUM/100))

URL=https://$BENCH_HOST:$BENCH_PORTS/$BENCH_HTDOCS_TEST_DIR/jquery.js

RET="$(ab $BENCH_AB_FLAGS -k -H "Accept-Encoding: deflate" -c $BENCH_CON -n $BENCH_NUM $URL 2>&1)"
RET_CODE=$?

export RET
export RET_CODE

sh $TOOL_DIR/hammer.sh
