
set -e

source $TOOL_DIR/defines.sh

do_partial_req () {
  echo "$msg"
  sleep 0.2
  if kill -0 $pid 2> /dev/null; then
    echo "alive"
  else
    echo "dead"
    return
  fi
  stdbuf -oL printf "$msg" > "$FIFO_PATH" &
}

start_test "slowloris" "test a slow loris like request"
ERROR_CODE=429
URL="/"
RET_PATH=${BENCH_BINARY_DIR}/${name}_${module}.out
FIFO_PATH=$BENCH_BINARY_DIR/${name}_${module}.in

rm -f "$FIFO_PATH"
mkfifo "$FIFO_PATH"
tail -f "$FIFO_PATH" | nc $BENCH_HOST $BENCH_PORT > "$RET_PATH" &
pid=$!
sleep 0.1

echo "1"
msg="GET $URL HTTP/1.1\r\n"
do_partial_req

sleep 0.2

echo "2"
msg="Host: $BENCH_HOST:$BENCH_PORT\r\n"
do_partial_req

echo "3"
msg="Connection: close\r\n"
do_partial_req

echo "4"
msg="User-Agent: hello world\r\n"
do_partial_req

echo "5"
msg="Cookie: yum=true\r\n"
do_partial_req

echo "6"
msg="Referer: http://localhost/\r\n"
do_partial_req

echo "7"
msg="\r\n"
do_partial_req

echo "8"

if kill -0 $pid 2> /dev/null; then
  sleep 1
fi

rm "$FIFO_PATH"
RET=$(cat $RET_PATH)
check_error
end_test

