
set -e

source $TOOL_DIR/defines.sh

do_req () {
  RES=$(echo "$REQ" | nc $BENCH_HOST $BENCH_PORT)
  echo "$RES"
  echo "$RES" | grep "HTTP/1.1 $EXPECTED"
}

do_aborted_req () {
  echo "$REQ" | nc $NC_FLAGS $BENCH_HOST $BENCH_PORT &> /dev/null
  printf "."
}

start_test "simple_get" "test a simple get request"
URL="/"
EXPECTED="200 OK"
REQ=$(printf "GET $URL HTTP/1.1\r\n\
Host: $BENCH_HOST:$BENCH_PORT\r\n\
Connection: close\r\n\
\r\n")
do_req
echo "GET request OK"
end_test

start_test "options" "test an options request"
URL="/"
EXPECTED="204 No Content"
REQ=$(printf "OPTIONS $URL HTTP/1.1\r\n\
Host: $BENCH_HOST:$BENCH_PORT\r\n\
Connection: close\r\n\
\r\n")
do_req
echo "OPTIONS request OK"
end_test

start_test "aborted_conn" "test an aborted connection"
URL="/$BENCH_HTDOCS_TEST_DIR/$TEST_FILE_LARGE"
NC_FLAGS="-q 0"
REQ=$(printf "GET $URL HTTP/1.1\r\n\
Host: $BENCH_HOST:$BENCH_PORT\r\n\
Connection: close\r\n\
\r\n")

echo "TESTING aborted connections 1 ..."
NC_FLAGS="-q 0"
for VAR in {1..$BENCH_CON}; do
  do_aborted_req
done
echo ""
echo "TESTING aborted connections 1 OK"

echo "TESTING aborted connections 2 ..."
NC_FLAGS="-q 1"
for VAR in {1..$BENCH_CON}; do
  do_aborted_req
done
echo ""
echo "TESTING aborted connections 2 OK"
end_test

#REQ=$(printf "GET / HTTP/1.1\r\n\
#Connection: close\r\n\
#\r\n")
#do_req
#echo "GET HTTP/1.1 no host ok"

#echo "GET deflate request"
#echo "GET gzip request"
#echo "GET keep-alive request"
#echo "HEAD request"
#echo "GET 304 request"
