
set +e

source $TOOL_DIR/defines.sh

check_out_file_empty () {
  file_sz="`wc -c < $out_path`"
  if [ "$file_sz" -ne 0 ]; then
    echo "ERROR! $module request returned non empty file of $file_sz bytes"
    exit 1
  else
    echo "$module is properly empty"
  fi
}

start_test "404" "generate a 404 status"
ERROR_CODE=404
RET="$(curl -v "http://$BENCH_HOST:$BENCH_PORT/$BENCH_HTDOCS_TEST_DIR/404" 2>&1)"
check_error
end_test

start_test "403" "generate a 403 status"
ERROR_CODE=403
RET="$(curl -v "http://$BENCH_HOST:$BENCH_PORT/$BENCH_HTDOCS_TEST_DIR/forbidden.html" 2>&1)"
check_error
end_test

start_test "post" "don't accept POST on a static resource"
ERROR_CODE=405
POST="-F hello=world"
RET="$(curl -v $POST "http://$BENCH_HOST:$BENCH_PORT/" 2>&1)"
check_error
end_test

start_test "head" "don't send bytes on a HEAD request"
ERROR_CODE=200
RET="$(curl -v --request HEAD -0 "http://$BENCH_HOST:$BENCH_PORT/" 2>&1 1> $out_path)"
check_error
check_out_file_empty
end_test

start_test "etag" "generate a 304 status"
ERROR_CODE=304
ETAG_PATH=${BENCH_BINARY_DIR}/${name}_${module}.etag
RET="$(curl --fail --etag-compare $ETAG_PATH --etag-save $ETAG_PATH "http://$BENCH_HOST:$BENCH_PORT/" 2>&1)"
echo "Etag is '`cat $ETAG_PATH`'"
RET="$(curl -v --etag-compare $ETAG_PATH --etag-save $ETAG_PATH "http://$BENCH_HOST:$BENCH_PORT/" 2>&1 1>$out_path)"
false
check_error
check_out_file_empty
end_test

start_test "ifmodifiedsince" "generate a 304 status"
ERROR_CODE=304
LAST_MODIFIED="${BENCH_HTDOCS_DIR}/index.html"
RET="$(curl -v -z "$LAST_MODIFIED" "http://$BENCH_HOST:$BENCH_PORT/" 2>&1 1>$out_path)"
check_error
check_out_file_empty
end_test

start_test "trailing" "send a trailing slash redirect"
ERROR_CODE=301
RET="$(curl -v -0 "http://$BENCH_HOST:$BENCH_PORT/bin" 2>&1 1> $out_path)"
check_error
check_out_file_empty
end_test

start_test "method not supported" "respond with error if the method is unknown"
ERROR_CODE=501
RET="$(curl -v --request MEET "http://$BENCH_HOST:$BENCH_PORT/" 2>&1 1> $out_path)"
check_error
end_test

start_test "version not supported" "respond with error if the version is unkown"
ERROR_CODE=505
SEND_REQ=`printf "GET / HTTP/2\r\nHost: $BENCH_HOST\r\n\r\n"`
RET="$(echo "$SEND_REQ" | nc $BENCH_HOST $BENCH_PORT)"
check_error
end_test

echo "test finished successfully"

