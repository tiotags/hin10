
URL=https://$BENCH_HOST:$BENCH_PORTS/

RET="$(ab $BENCH_AB_FLAGS -k -c $BENCH_CON -n $BENCH_NUM $URL 2>&1)"
RET_CODE=$?

export RET
export RET_CODE

sh $TOOL_DIR/hammer.sh
