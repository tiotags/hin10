

URL=http://$BENCH_HOST:$BENCH_PORT/

ulimit -S -n `ulimit -Hn`

export BENCH_CON=$((4*$BENCH_CON))
export BENCH_NUM=$((4*$BENCH_NUM))

export module="no_keepalive"
RET="$(ab $BENCH_AB_FLAGS -c $BENCH_CON -n $BENCH_NUM $URL)"
RET_CODE=$?

export RET
export RET_CODE

sh $TOOL_DIR/hammer.sh


export module="keepalive"
RET="$(ab $BENCH_AB_FLAGS -k -c $BENCH_CON -n $BENCH_NUM $URL)"
RET_CODE=$?

export RET
export RET_CODE

sh $TOOL_DIR/hammer.sh


