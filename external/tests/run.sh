#!/bin/bash

CWD_DIR=`pwd`
export SCRIPT_DIR="$( realpath ` dirname -- "$0" ` )/"
export ROOT_DIR="$( realpath $SCRIPT_DIR/../../ )"

RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

function print_crash {
  if [ $server_crashed -eq 0 ]; then
    printf "\nServer ${RED}crashed${NC} !!! check \`less ${BENCH_RESULTS_DIR}server.log\`\n"
    server_crashed=1
  fi
}

function cleanup {
  echo "killing hinsightd $PID"
  kill -2 $PID &>> ${DEBUG_LOG_PATH}
  #killall hinsightd
}

export BENCH_HOST=${BENCH_HOST:-localhost}
export BENCH_PORT=${BENCH_PORT:-8080}
export BENCH_PORTS=${BENCH_PORTS:-8081}
export BENCH_REMOTE=${BENCH_REMOTE:-http://localhost:28082/}
export BENCH_CON=${BENCH_CON:-1000}
export BENCH_NUM=${BENCH_NUM:-100000}

export BENCH_ROOT_DIR=${BENCH_ROOT_DIR:-${ROOT_DIR}/build/tests}
export TOOL_DIR=${SCRIPT_DIR}/tools/
export CONFIG_DIR=${BENCH_ROOT_DIR}/config/
export BENCH_BUILD_DIR=${BENCH_BUILD_DIR:-${ROOT_DIR}/build}
export BENCH_HTDOCS_DIR=${BENCH_HTDOCS_DIR:-${ROOT_DIR}/htdocs/}
export BENCH_WORK_DIR=${BENCH_WORK_DIR:-${ROOT_DIR}/workdir/}
export BENCH_RESULTS_DIR=${BENCH_RESULTS_DIR:-${BENCH_ROOT_DIR}/logs/}
export BENCH_TEST_LOG_DIR=${BENCH_TEST_LOG_DIR:-${BENCH_ROOT_DIR}/tests/}
export BENCH_BINARY_DIR=${BENCH_BINARY_DIR:-${BENCH_ROOT_DIR}/downloads}
export BENCH_CURL_FLAGS=${BENCH_CURL_FLAGS:-"-v -k --fail"}
export BENCH_AB_FLAGS=${BENCH_AB_FLAGS:-""}
export BENCH_BINARY_NAME=${BENCH_BINARY_NAME:-"hinsightd"}
export BENCH_HTDOCS_TEST_DIR="bin"

export TEST_FILE_MEDIUM=medium.bin
export TEST_FILE_LARGE=large.bin
export TEST_FILE_TEXT=jquery.js
export TEST_FILE_HUGE_PATH=dg34t3geve45yegre/32r523fwed4t34tgefgr/asfdctfwfesa.html

SSLDIR=workdir/ssl/
DEBUG_LOG_PATH=${BENCH_RESULTS_DIR}server.log

complete=0
total=0
server_crashed=0

time_start="$(date +%s.%N)"

mkdir -p $BENCH_ROOT_DIR/{downloads,logs,tests} ${BENCH_WORK_DIR}/ssl

if [ ! -f ${BENCH_WORK_DIR}/ssl/key.pem ]; then
  openssl req -x509 -newkey rsa:2048 -keyout ${BENCH_WORK_DIR}/ssl/key.pem -out ${BENCH_WORK_DIR}/ssl/cert.pem -sha256 -days 365 -nodes -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=example.com"
fi

$TOOL_DIR/temp_files.sh
rm -f ${BENCH_TEST_LOG_DIR}/access.log
mkdir -p $(dirname "$DEBUG_LOG_PATH")
touch "$DEBUG_LOG_PATH"

wait

$BENCH_BUILD_DIR/$BENCH_BINARY_NAME --log $DEBUG_LOG_PATH --cert $SSLDIR/cert.pem $SSLDIR/key.pem --port $BENCH_PORT --ports $BENCH_PORTS --vhost localhost htdocs/ --no_cmdline &

PID=$!

{ tail -f -n +0 ${DEBUG_LOG_PATH} | grep -qe "hin serve ..." && echo "started up"; } &
sleep 10 && false &

wait -n
ret_code="$?"
if [ $ret_code -ne 0 ]; then
  print_crash
  exit -1
fi

trap cleanup EXIT

printf "\ntesting $BENCH_HOST:$BENCH_PORT with -c $BENCH_CON -n $BENCH_NUM on `date`\n" >> $BENCH_RESULTS_DIR/bench.txt

run_test () {
  export name=`basename $file`
  export name="${name%.*}"
  export test_dir=$BENCH_ROOT_DIR/
  ((total++))
  echo "Test $name started on `date`" &> ${BENCH_TEST_LOG_DIR}/$name.log
  printf "run  \t$name"
  if [ $server_crashed -eq 0 ]; then
    sh $file &>> ${BENCH_TEST_LOG_DIR}$name.log
    exit_code=$?
    if ! kill -s 0 $PID &>> ${BENCH_RESULTS_DIR}/server.log; then
      exit_code=1
      echo "Server crashed" >> ${BENCH_TEST_LOG_DIR}/$name.log
      print_crash
    fi
  else
    exit_code=1
  fi
  if [ $exit_code -eq 0 ]; then
    printf "\r${GREEN}success$NC\t$name\n"
    ((complete++))
  else
    printf "\r${RED}failed$NC\t$name (more info 'less ${BENCH_TEST_LOG_DIR}$name.log')\n"
  fi
}

if [ -n "$1" ]; then
  for fn in "$@"
  do
    file=$CWD_DIR/$fn
    run_test
  done
else
  for file in $SCRIPT_DIR/tests/*.sh; do
    run_test
  done
fi

kill -2 $PID &>> ${BENCH_RESULTS_DIR}/server.log

time_end="$(date +%s.%N)"
time_diff=`echo "$time_end-$time_start" | bc`

echo "Successfully finished $complete/$total tests in $time_diff seconds"

if [ $complete != $total ]; then
  exit $(($total-$complete))
fi

wait

