/*
 * basic_libs, libraries used for other projects, including, pattern matching, timers and others
 * written by Alexandru C
 * You may not use this software except in compliance with the License.
 * You may obtain a copy of the License at: docs/LICENSE.txt
 * documentation is in the docs folder
 */

#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "basic_args.h"
#include "basic_mem.h"
#include "basic_pattern.h"
#include "basic_types.h"

BASIC_EXPORT int basic_args_cmp (const char * base, ...) {
  va_list ap;
  va_start (ap, base);
  int ret = 0;
  while (1) {
    const char * ptr = va_arg (ap, const char *);
    if (ptr == NULL) { break; }
    if (strcmp (base, ptr) == 0) { ret = 1; break; }
  }
  va_end (ap);
  return ret;
}

BASIC_EXPORT const char * basic_args_get (basic_args_t * args) {
  int num = args->index + 1;
  if (num >= args->argc) {
    return NULL;
  }
  const char * param = args->argv[num];
  if (param == NULL || *param == '-') {
    return NULL;
  }
  args->index++;
  return args->argv[num];
}

static int argv_process1 (basic_args_t * args) {
  const char * base = args->argv[args->index];
  const char * ptr = base;
  if (*ptr != '-') return 0;
  ptr++;
  if (*ptr == '-') {
    ptr++;
    //printf ("long option '%s'\n", ptr);
    return args->callback (args, base);
  }
  char buf[4];
  buf[0] = '-';
  buf[2] = '\0';
  while (*ptr) {
    buf[1] = *ptr;
    //printf ("option '%s'\n", buf);
    int ret = args->callback (args, buf);
    if (ret) return ret;
    ptr++;
  }
  return 0;
}

BASIC_EXPORT int basic_args_process (int argc, const char * argv[], basic_args_callback_t callback) {
  basic_args_t args;
  args.argc = argc;
  args.argv = argv;
  args.callback = callback;
  for (args.index = 1; args.index < args.argc; args.index++) {
    int ret = argv_process1 (&args);
    if (ret) return ret;
  }
  return 0;
}

static int get_line (FILE * fp, char ** str) {
  char buffer[1024];
  unsigned int buf_pos = 0;
  do {
    int c = fgetc (fp);
    if (c == '\r') continue;
    if (feof (fp)) { break; }
    if (c == '\n') {
      if (buf_pos == 0) continue;
      break;
    }
    if (buf_pos >= sizeof buffer - 1) continue;
    buffer[buf_pos] = c;
    buf_pos++;
  } while (1);
  buffer[buf_pos] = '\0';
  if (buf_pos == 0) {
    if (feof (fp)) { return -1; }
    return buf_pos;
  }
  *str = strdup (buffer);
  return buf_pos;
}

static int get_param (string_t * source, string_t * out) {
  char buffer[1024];

  int used = match_string (source, "%s+");
  if (used <= 0) return -1;

  char * ptr = source->ptr;
  char * max = source->ptr + source->len;

  size_t num = 0;
  for (; ptr < max; ptr++) {
    if (isspace (*ptr)) { break; }
    if (*ptr == '\\') ptr++;
    buffer[num] = *ptr;
    num++;
    if (num >= sizeof buffer) break;
  }
  out->ptr = strndup (buffer, num);
  out->len = num;

  num = ptr - source->ptr;
  source->len -= num;
  source->ptr = ptr;
  //printf ("param is %d '%.*s'\n", out->len, out->len, out->ptr);
  //printf ("source is %d '%.*s'\n", source->len, source->len, source->ptr);

  return used + num;
}

BASIC_EXPORT int basic_args_parse_file (const char * path, basic_args_callback_t callback) {
  FILE * fp = fopen (path, "r");
  if (fp == NULL) {
    fprintf (stderr, "can't open config file '%s': %s\n", path, strerror (errno));
    return -1;
  }
  char * argv[20];
  int num_args = 0;
  int ret = 0;
  while (1) {
    num_args = 0;
    char * line = NULL;
    int used = get_line (fp, &line);
    if (used < 0) break;
    //printf ("line is %d '%.*s'\n", used, used, line);
    if (*line == '#') {
      hfree (line);
      continue;
    }
    string_t source, param;
    source.len = used;
    source.ptr = line;
    used = match_string (&source, "([%w%p]+)", &param);
    if (used <= 0) continue;
    if (asprintf (&argv[0], "--%.*s", (int)param.len, param.ptr) <= 0) {
      printf ("error! no memory %x", 0x43654654);
      continue;
    }
    num_args++;
    while (1) {
      used = get_param (&source, &param);
      if (used <= 0) break;
      argv[num_args++] = param.ptr;
      if (num_args >= 19) break;
    }

    free (line);

    if (num_args == 0) continue;
    argv[num_args] = NULL;

    if (0) {
      printf ("argc %d\n", num_args);
      for (int i=0; i <= num_args; i++) {
        printf ("arg[%d] = '%s'\n", i, argv[i]);
      }
    }

    basic_args_t args;
    args.argc = num_args;
    args.argv = (const char**)argv;
    args.callback = callback;
    for (args.index = 0; args.index < args.argc; args.index++) {
      ret = argv_process1 (&args);
      if (ret) break;
    }

    for (int i=0; i<num_args; i++) {
      free (argv[i]);
      argv[i] = NULL;
    }

    if (ret) break;
    if (feof (fp)) break;
  }

  fclose (fp);
  //free (argv);

  return ret;
}

BASIC_EXPORT int basic_args_parse_line (string_t * source, basic_args_callback_t callback) {
  char * argv[20];
  int num_args = 0;
  int ret = 0;
  while (1) {
    num_args = 0;
    string_t param;
    int used = match_string (source, "([^%s]+)", &param);
    if (used <= 0) break;
    if (asprintf (&argv[0], "--%.*s", (int)param.len, param.ptr) <= 0) {
      printf ("error! no memory %x", 0x12423768);
      continue;
    }
    num_args++;
    while (1) {
      used = get_param (source, &param);
      if (used <= 0) break;
      //printf ("param ptr %d %d '%.*s'\n", used, param.len, param.len, param.ptr);
      argv[num_args++] = param.ptr;
      if (num_args >= 19) break;
    }

    argv[num_args] = NULL;

    if (0) {
      printf ("argc %d\n", num_args);
      for (int i=0; i <= num_args; i++) {
        printf ("arg[%d] = '%s'\n", i, argv[i]);
      }
    }

    basic_args_t args;
    args.argc = num_args;
    args.argv = (const char**)argv;
    args.callback = callback;
    for (args.index = 0; args.index < args.argc; args.index++) {
      ret = argv_process1 (&args);
      if (ret) break;
    }

    for (int i=0; i<num_args; i++) {
      free (argv[i]);
      argv[i] = NULL;
    }

    break;
  }

  return ret;
}

