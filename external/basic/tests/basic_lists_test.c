#include "basic_lists.h"
#define AUDIT_IMPLEMENTATION
#include "audit.h"

typedef struct {
  int value;
  basic_dlist_t list;
} struct_t;

audit ("Test that basic_lists works as expected") {
  basic_dlist_t head = {NULL, NULL};
  basic_dlist_t * elem;
  struct_t * item;
  int start = 0;
  int end = 5;
  int sum = 0;

  for (int i = start; i < end; i++) {
    item = calloc (1, sizeof (struct_t));
    item->value = i;
    basic_dlist_append (&head, &item->list);
  }

  elem = head.next;
  while (elem) {
    item = basic_dlist_ptr (elem, offsetof (struct_t, list));
    elem = elem->next;

    sum += item->value;
  }
  check_eq (sum, 10, "%i", "get sum");

  elem = head.next;
  item = basic_dlist_ptr (elem, offsetof (struct_t, list));
  check_eq (item->value, 0, "%i", "get head");

  elem = head.prev;
  item = basic_dlist_ptr (elem, offsetof (struct_t, list));
  check_eq (item->value, 4, "%i", "get tail");

  elem = head.next;
  while (elem) {
    item = basic_dlist_ptr (elem, offsetof (struct_t, list));
    elem = elem->next;

    free (item);
  }
}
