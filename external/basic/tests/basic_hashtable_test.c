#include "basic_hashtable.h"
#define AUDIT_IMPLEMENTATION
#include "audit.h"

audit ("Test that basic_hashtable works as expected") {
  basic_ht_hash_t h1 = 0, h2 = 0;
  basic_ht_pair_t * pair = NULL;
  basic_ht_t * ht = basic_ht_create (1024, 599);

  const char * name = "hello world";
  int name_len = strlen (name);

  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  basic_ht_set_pair (ht, h1, h2, 12, 78);

  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  pair = basic_ht_get_pair (ht, h1, h2);

  check_neq (pair, NULL, "%p", "get pair");
  check_eq (pair->d.ptr.value1, 12, "%i", "get pair v1");
  check_eq (pair->d.ptr.value2, 78, "%i", "get pair v2");

  int num = 0;

  basic_ht_iterator_t iter;
  memset (&iter, 0, sizeof iter);
  while ((pair = basic_ht_iterate_pair (ht, &iter)) != NULL) {
    num++;
  }

  check_eq (num, 1, "%i", "get num iterated");

  basic_ht_free (ht);
}
