#include "basic_pattern.h"
#define AUDIT_IMPLEMENTATION
#include "audit.h"

audit ("Test that basic_pattern parses strings as expected") {
  int used;
  string_t source, orig, param1, param2;
  orig.ptr = "Hello World!";
  orig.len = strlen (orig.ptr);

  source = orig;
  used = match_string (&source, "Hello");
  check_eq (used, 5, "%i", "get string");
  used = match_string (&source, "%s+World");
  check_eq (used, 6, "%i", "get continuation string");
  used = match_string (&source, "!");
  check_eq (used, 1, "%i", "get continuation string 2");

  source = orig;
  used = matchi_string (&source, "hello");
  check_eq (used, 5, "%i", "case sensitivity test");

  source = orig;
  used = match_string (&source, "(%w+)%s+(%w+)", &param1, &param2);
  check_eq (used, 11, "%i", "used");
  check_eq (param1.len, 5, "%i", "word1 len");
  check_eq (param2.len, 5, "%i", "word2 len");

  source = orig;
  source.len = 1;
  used = match_string (&source, "Hello");
  check_eq (used, -1, "%i", "get string");
  used = match_string (&source, "%s+World");
  check_eq (used, -1, "%i", "get continuation string");
  used = match_string (&source, "!");
  check_eq (used, -1, "%i", "get continuation string 2");

  source = orig;
  source.len = 3;
  used = match_string (&source, "%w+");
  check_eq (used, 3, "%i", "get string");
}
