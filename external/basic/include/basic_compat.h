

#ifdef BASIC_USE_MUSL
#ifndef BASIC_MUSL_COMPAT_H
#define BASIC_MUSL_COMPAT_H

int statx(int dirfd, const char *restrict pathname, int flags,
                 unsigned int mask, struct statx *restrict statxbuf);

#endif
#endif
