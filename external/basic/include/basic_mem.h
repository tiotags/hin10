
#ifndef BASIC_MALLOC_H
#define BASIC_MALLOC_H

#ifdef BASIC_USE_THREAD
#include <pthread.h>
#define basic_struct_lock(s) pthread_mutex_lock (&(s)->mutex)
#define basic_struct_unlock(s) pthread_mutex_unlock (&(s)->mutex)
#define basic_mutex_t pthread_mutex_t mutex;
#define basic_threadsafe_var(ptr, new_val_formula) do {\
  volatile __typeof__ (*ptr) old_val = *(ptr);\
  volatile __typeof__ (*ptr) new_val = (new_val_formula);\
  __typeof__ (*ptr) prev = __sync_val_compare_and_swap ((ptr), old_val, new_val);\
  if (prev == old_val) break;\
} while (1);
#else
#define basic_struct_lock(s)
#define basic_struct_unlock(s)
#define basic_mutex_t
#define basic_threadsafe_var(ptr, new_val_formula) *(ptr) = (new_val_formula);
#endif

// halloc quick
#define halloc_q(sz) malloc ((sz))
// halloc zero
#define halloc_z(sz) calloc (1, (sz))
// halloc realloc
#define halloc_re(old, sz) realloc ((old), (sz))
#define hfree(ptr) free ((ptr))

#endif

