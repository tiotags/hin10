
#ifndef BASIC_ARGS_H
#define BASIC_ARGS_H

#include <basic_pattern.h> // for parse line

struct basic_args_struct;

typedef int (*basic_args_callback_t) (struct basic_args_struct * args, const char * name);

enum {
BARG_DEBUG=0x1,
};

typedef struct basic_args_struct {
  int argc;
  const char ** argv;
  int index;
  uint32_t debug;
  basic_args_callback_t callback;
} basic_args_t;

int basic_args_process (int argc, const char * argv[], basic_args_callback_t callback);
int basic_args_parse_file (const char * path, basic_args_callback_t callback);
int basic_args_parse_line (string_t * source, basic_args_callback_t callback);
int basic_args_cmp (const char * base, ...);
const char * basic_args_get (basic_args_t * args);

#endif

