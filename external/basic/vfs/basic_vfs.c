
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "basic_vfs.h"
#include "internal.h"

void basic_vfs_node_clean (basic_vfs_node_t * node) {
  // TODO
}

BASIC_EXPORT int basic_vfs_node_free (basic_vfs_node_t * node) {
  switch (node->type) {
  case 0:
  case BASIC_ENT_FILE:
    if (node->inode == NULL) break;
    basic_vfs_unref_node (node);
    hfree (node->inode);
  break;
  case BASIC_ENT_DIR:
    if (node->inode == NULL) break;
    basic_vfs_dir_t * dir = node->inode;
    for (int i=0; i < dir->max; i++) {
      if (dir->entries[i])
        basic_vfs_node_free (dir->entries[i]);
    }
    if (dir->entries) hfree (dir->entries);
    if (dir->path) hfree ((char*)dir->path);
    hfree (dir);
  break;
  default:
    printf ("vfs clean unknown type ? %d\n", node->type);
  break;
  }
  hfree (node);
  return 0;
}

BASIC_EXPORT int basic_vfs_init (basic_vfs_t * vfs) {
  basic_struct_lock (vfs);
  basic_ht_init (&vfs->ht, 1024, 0);

  basic_vfs_node_t * root = halloc_z (sizeof (basic_vfs_node_t));
  root->type = BASIC_ENT_DIR;
  root->vfs = vfs;
  vfs->root = root;

  basic_vfs_dir_t * dir = halloc_z (sizeof (basic_vfs_dir_t));
  dir->i.vfs = vfs;
  root->inode = dir;
  basic_vfs_populate_dir (dir, "/", 1);

  char * cwd_path = realpath (".", NULL);
  string_t path;
  path.ptr = cwd_path;
  path.len = strlen (path.ptr);
  vfs->cwd = basic_vfs_ref_path_raw (vfs, NULL, &path);
  hfree (cwd_path);

  basic_struct_unlock (vfs);
  return 0;
}

BASIC_EXPORT int basic_vfs_clean (basic_vfs_t * vfs) {
  basic_struct_lock (vfs);
  basic_ht_clean (&vfs->ht);
  basic_vfs_node_free (vfs->root);
  if (vfs->inotify_fd) close (vfs->inotify_fd);
  basic_struct_unlock (vfs);
  return 0;
}

