
#ifndef BASIC_VFS_INT_H
#define BASIC_VFS_INT_H

#include "basic_mem.h"
#include "basic_vfs.h"
#include "basic_types.h"

basic_vfs_node_t * basic_vfs_ref_path_raw (basic_vfs_t * vfs, basic_vfs_node_t * dir_node, string_t * path);

int basic_vfs_populate_dir (basic_vfs_dir_t * dir, const char * path, int path_len);
int basic_vfs_node_free (basic_vfs_node_t * node);
void basic_vfs_node_clean (basic_vfs_node_t * node);
int basic_vfs_add_inotify (basic_vfs_t * vfs, basic_vfs_dir_t * dir);

int basic_vfs_delete (basic_vfs_dir_t * dir, const char * name, int name_len);
basic_vfs_node_t * basic_vfs_add (basic_vfs_dir_t * dir, int type, const char * name, int len);

basic_vfs_dir_t * basic_vfs_ref_dir (basic_vfs_node_t *);

#endif


