
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dirent.h>

#include "basic_vfs.h"
#include "internal.h"

static basic_vfs_node_t * basic_vfs_search_dir (basic_vfs_dir_t * dir, const char * name, int name_len) {
  if (dir == NULL) return NULL;
  for (int i=0; i < dir->num; i++) {
    basic_vfs_node_t * dent = dir->entries[i];
    if (dent == NULL) continue;
    if (dent->name_len != name_len) continue;
    if (memcmp (dent->name, name, name_len) != 0) continue;
    return dent;
  }
  return NULL;
}

int basic_vfs_delete (basic_vfs_dir_t * dir, const char * name, int name_len) {
  for (int i=0; i < dir->num; i++) {
    basic_vfs_node_t * node = dir->entries[i];
    if (node == NULL) continue;
    if (node->name_len != name_len) continue;
    if (memcmp (node->name, name, name_len) != 0) continue;
    basic_vfs_node_free (node);
    dir->entries[i] = NULL;
    return 1;
  }
  return 0;
}

basic_vfs_node_t * basic_vfs_add (basic_vfs_dir_t * dir, int type, const char * name, int len) {
  basic_vfs_node_t * new = basic_vfs_search_dir (dir, name, len);
  if (new) return new;

  int id = dir->num++;
  dir->used++;
  if (dir->used < dir->num) {
    dir->num = 0;
    while (1) {
      if (dir->entries && dir->entries[id] == NULL) break;
      id++;
      if (id >= dir->num) {
        dir->num++;
        break;
      }
    }
  }
  if (dir->num >= dir->max) {
    dir->max += 20;
    dir->entries = halloc_re (dir->entries, dir->max * sizeof (void*));
    memset (&dir->entries[dir->max-20], 0, 20 * sizeof (void*));
  }

  new = halloc_z (sizeof (*new) + len + 1);
  new->name_len = len;
  new->parent = dir;
  new->type = type;
  new->name_len = len;
  new->vfs = dir->i.vfs;
  memcpy (new->name, name, len + 1);
  dir->entries[id] = new;
  return new;
}

int basic_vfs_populate_dir (basic_vfs_dir_t * dir, const char * path, int path_len) {
  struct dirent *dent;
  dir->path = strndup (path, path_len+1);
  dir->path_len = path_len;
  DIR * d = opendir (dir->path);
  basic_vfs_t * vfs = dir->i.vfs;
  if (vfs->debug & BASIC_VFS_DEBUG)
    printf ("vfs populating '%s'\n", dir->path);
  if (d == NULL) {
    printf ("opendir can't open '%s': %s\n", dir->path, strerror (errno));
    return -1;
  }

  basic_vfs_add_inotify (vfs, dir);

  while ((dent = readdir (d)) != NULL) {
    int len = strlen (dent->d_name);
    if (dent->d_name[0] == '.' &&
      (len == 1 || (dent->d_name[1] == '.' && len == 2))) {
      continue;
    }
    int type = 0;
    switch (dent->d_type) {
    case DT_DIR: type = BASIC_ENT_DIR; break;
    case DT_REG: type = BASIC_ENT_FILE; break;
    case DT_LNK: type = 0; break;
    case DT_FIFO:	// pass-through
    case DT_SOCK:	// pass-through
    case DT_CHR:	// pass-through
    case DT_BLK:	// pass-through
      // should warn that server has weird htdocs ?
      type = 0;
    break;
    default:
      type = BASIC_ENT_UNKNOWN;
      printf ("unknown file type for '%s'\n", dent->d_name);
    break;
    }
    if (type == BASIC_ENT_UNKNOWN) continue;
    basic_vfs_add (dir, type, dent->d_name, strlen (dent->d_name));
  }
  closedir (d);
  return 0;
}


BASIC_EXPORT basic_vfs_dir_t * basic_vfs_ref_dir (basic_vfs_node_t * node) {
  if (node == NULL || node->type != BASIC_ENT_DIR) return NULL;
  if (node->inode) return node->inode;

  basic_vfs_dir_t * parent = node->parent;

  int new_len = 0;
  new_len += node->name_len + 1;
  new_len += parent->path_len;

  char * new_path = halloc_q (new_len+1);
  int pos = new_len;
  new_path[pos--] = '\0';
  new_path[pos--] = '/';
  pos++;
  pos -= node->name_len;
  memcpy (&new_path[pos], node->name, node->name_len);
  pos -= parent->path_len;
  memcpy (&new_path[pos], parent->path, parent->path_len);

  basic_vfs_dir_t * new_dir = halloc_z (sizeof (basic_vfs_dir_t));
  new_dir->parent = node->parent;
  new_dir->i.vfs = node->vfs;
  node->inode = new_dir;

  if (basic_vfs_populate_dir (new_dir, new_path, new_len) < 0) {
    node->flags |= BASIC_VFS_FORBIDDEN;
  }
  hfree (new_path);

  return new_dir;
}

