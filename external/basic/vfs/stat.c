
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "basic_compat.h"

#include "basic_vfs.h"
#include "internal.h"

static basic_vfs_file_t * basic_vfs_stat (basic_vfs_node_t * node) {
  if (node == NULL || node->type != BASIC_ENT_FILE) return NULL;

  basic_vfs_file_t * file = node->inode;

  if (file && file->fd >= 0) {
    file->i.refcount++;
    return file;
  }

  char * path = NULL;
  basic_vfs_dir_t * dir = node->parent;
  int ret = asprintf (&path, "%s/%s", dir->path, node->name);
  if (ret < 0) { goto finish; }

  int fd = openat (AT_FDCWD, path, O_RDONLY | O_CLOEXEC | O_NOCTTY);

  if (fd < 0) {
    printf ("can't open path '%s' err '%s'\n", path, strerror (errno));
    switch (errno) {
    case EMFILE:
    case ENFILE:
      node->flags |= BASIC_VFS_FD_LIMIT;
    break;
    }
    node->flags |= BASIC_VFS_FORBIDDEN;
    goto finish;
  }

  struct statx stat;
  if (statx (fd, "", AT_EMPTY_PATH, STATX_MTIME | STATX_SIZE | STATX_TYPE, &stat) < 0) {
    fprintf (stderr, "statx %s: %s\n", path, strerror (errno));
    close (fd);
    goto finish;
  }

  node->type = BASIC_ENT_FILE;

  uint64_t etag = 0;
  etag += stat.stx_mtime.tv_sec * 0xffff;
  etag += stat.stx_mtime.tv_nsec * 0xff;
  etag += stat.stx_size;

  if (file == NULL)
    file = halloc_z (sizeof (basic_vfs_file_t));

  file->i.type = 0;
  file->i.refcount++;
  file->i.vfs = node->vfs;
  file->fd = fd;
  file->size = stat.stx_size;
  file->etag = etag;
  file->modified = stat.stx_mtime.tv_sec;
  file->parent = node;

  node->inode = file;

  basic_vfs_t * vfs = node->vfs;
  if (vfs->fd_acquire)
    vfs->fd_acquire (node);

finish:
  hfree (path);
  return file;
}

BASIC_EXPORT basic_vfs_file_t * basic_vfs_ref_file (basic_vfs_node_t * node) {
  if (node == NULL || node->type != BASIC_ENT_FILE) return NULL;

  basic_struct_lock (node);
  basic_vfs_stat (node);
  basic_struct_unlock (node);

  return node->inode;
}

BASIC_EXPORT int basic_vfs_unref_file (basic_vfs_node_t * node) {
  basic_struct_lock (node);

  if (node->flags & BASIC_VFS_FORBIDDEN) {
    goto finish;
  }

  basic_vfs_file_t * inode = node->inode;
  if (inode == NULL) {
    printf ("tried to unref a null node\n");
    goto finish;
  }

  inode->i.refcount--;
  if (inode->i.refcount > 0) { goto finish; }

  if (inode->fd < 0) {
    printf ("double file unref\n");
    goto finish;
  }

  basic_vfs_t * vfs = node->vfs;
  if (vfs->fd_release)
    vfs->fd_release (node);

  // close fd
  if (close (inode->fd) < 0) {
    perror ("close");
  }
  inode->fd = -1;

finish:
  basic_struct_unlock (node);
  basic_vfs_unref_node (node);
  return 0;
}

