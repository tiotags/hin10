
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include "basic_vfs.h"
#include "internal.h"

basic_vfs_node_t * basic_vfs_ref_path_raw (basic_vfs_t * vfs, basic_vfs_node_t * dir_node, string_t * path) {
  basic_vfs_node_t * next = NULL;
  string_t source, param1;
  source = *path;
  basic_vfs_node_t * current;

  if (vfs->debug & BASIC_VFS_EXTRA)
    printf ("vfs path ref '%.*s'\n", (int)path->len, path->ptr);

  if (dir_node == NULL) {
    current = vfs->cwd;
  } else {
    current = dir_node;
  }

  // if initial / then set current to root
  if (path->len <= 0) return NULL;
  if (match_string (&source, "/") > 0) {
    current = vfs->root;
  }
  // try to iterate directories
  next = current;
  while (1) {
    basic_vfs_dir_t * dir = basic_vfs_ref_dir (next);
    if (dir == NULL) return NULL;
    int used = match_string (&source, "([^/%z]+)", &param1);
    if (vfs->debug & BASIC_VFS_EXTRA)
      printf (" match %d %ld '%.*s' in %s\n", used, param1.len, (int)param1.len, param1.ptr, dir->path);
    if (used <= 0) return NULL;
    next = basic_vfs_search_dir (dir, param1.ptr, param1.len);
    if (next == NULL) {
      return NULL;
    }
    if (next->type != BASIC_ENT_DIR
    || (source.len == 1 && *source.ptr == '/')
    || match_string (&source, "/") <= 0
    || source.len == 0) {
      *path = source;
      basic_vfs_ref_node (next);
      return next;
    }
    current = next;
  }
  return NULL;
}

BASIC_EXPORT basic_vfs_node_t * basic_vfs_ref_path (basic_vfs_t * vfs, basic_vfs_node_t * dir_node, string_t * path) {
  basic_struct_lock (vfs);
  basic_vfs_node_t * ret = basic_vfs_ref_path_raw (vfs, dir_node, path);
  basic_struct_unlock (vfs);
  return ret;
}

BASIC_EXPORT int basic_vfs_ref_node (basic_vfs_node_t * node) {
  if (node == NULL) return -1;
  basic_struct_lock (node);
  node->refcount++;
  basic_struct_unlock (node);
  return 0;
}

BASIC_EXPORT int basic_vfs_unref_node (basic_vfs_node_t * node) {
  if (node == NULL) return -1;
  basic_struct_lock (node);
  node->refcount--;
  basic_struct_unlock (node);
  return 0;
}

