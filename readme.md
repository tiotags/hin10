
![the hinsightd mascot](external/packaging/beary_logo.png)

Hinsightd MT
============

[Hinsightd](https://tiotags.gitlab.io/hinsightd/mt.html) is an event-based multi-threaded HTTP/1.1 server designed with the goal of fitting as many useful features in as little code as possible. It's [decently fast](https://tiotags.gitlab.io/blog/benchmark5.html) with almost nginx level of performance, it still needs some optimization and testing though.

[[_TOC_]]

About
-----

This is the development branch for the multi-threaded functionality. This branch doesn't yet have all the features of the main branch but still has many of the most common features:
* HTTP/1.1 file serving with pipelining
* HTTPS
* compressed requests
* range requests
* 304 unmodified requests
* even some basic DDoS protection

### Processing model

(developer notes :pencil:) This is an event-based multi-threaded web server, the main thread spawns a number of threads and all threads (including the main) wait for accept events via io\_uring, or if that doesn't exist via epoll. Each client is processed inside a single thread with very little global state being shared between threads, mostly things like the file information so we can cache it between requests.

### DDoS protections

(developer notes :pencil:)

* The server caches and tracks every fd allocated allowing it use very few file descriptors and even continue serving clients even when over the OS imposed file descriptor limit.

* It has some basic slow loris protection, but more testing is needed.

* Very resilient and fast processing of incomming connections makes it easy to add any other processing needed.

### Roadmap

* readd reverse proxy from the legacy branch
* readd fastcgi from the legacy branch
* readd cache from the leagacy branch and fix it so it works in more places
* add per request lua processing
* automated IP block list
* per url bandwidth controls
* dropping misbehaving clients faster when overloaded
* better configuration options for current and future features

Getting Started
---------------

To run you need a linux OS with the requirements below and the C compiler of your preference.

### requirements

* a recent linux kernel (>=5.6 - march 2020)
* glibc/musl libc
* liburing
* libz
* optional: openssl/libressl
* cmake for compilation

### compile & run

At the moment there's no external packaging for the multi-threaded branch so you have to compile and install it on your own or run the docker container ([steps below](#deployment-inside-docker)).

clone repo and change working directory

	git clone https://gitlab.com/tiotags/hin10.git
	cd hin10

run cmake and make then start the server with the default settings

	mkdir -p build
	cmake -DCMAKE_BUILD_TYPE=Release -G Ninja -B build/
	ninja -C build/
	build/hinsightd
	# if you want to install
	#ninja -C build install

run using custom paths and settings
`build/hinsightd --listen all 8080 --vhost localhost /var/www/localhost`

or by using a config file
`build/hinsightd --config workdir/config.ini`

read the help for more options
`build/hinsightd --help`

### running tests

Requirements: curl, nc (netcat), openssl, ab (apache bench), perl (for deflate decompression and url decoding), coreutils (md5sum, sleep, cat, dirname, chmod etc), bash-like shell (kill, wait, ulimit), awk, grep etc

Tests can be found in `external/tests` the default way to run evey test is
`sh external/tests/run.sh`

you can also run individual tests like
`sh external/tests/run.sh external/tests/02_*.sh`

### deployment inside docker

Steps needed to get a container with the latest git version up and running inside docker/podman

	docker build -f external/packaging/Dockerfile -t hin10 external/packaging
	docker run --rm -it -p 8080:8080 hin10

podman requires a slightly modified run command, because the io\_uring system calls are blocked

	podman run --rm -it -p 8080:8080 --security-opt seccomp=unconfined hin10

Default paths and variables inside the container

* /app/htdocs/ - document root, where you place your files
* /app/workdir/config.ini - default config
* /app/workdir/ssl/cert.pem /app/workdir/ssl/key.pem - ssl certificates, if provide these the server will listen for ssl connections on port 8081
* /app/workdir/logs/ - logs directory, logging is disabled by default
* http port 8080 and https port 8081

### Notes

Information if you want to run this in a live environment: the server should be fairly resilient to remote exploitation bugs but some denial of service bugs are still likely present. We're getting to them, thank you for your patience!

links
-----

* You can find more benchmarks, guides, news and others [on the project website](https://tiotags.gitlab.io/hinsightd/mt.html)

* or checkout the [changelog](https://gitlab.com/tiotags/hin10/-/blob/master/docs/changelog.md) in the repo

* also you can check the [legacy version](https://tiotags.gitlab.io/hinsightd/) for more information about the development

* [roadmap and goals](https://gitlab.com/tiotags/hin9/-/blob/master/docs/roadmap.md)

### proudly powered by

* io_uring - the new async linux kernel API

contributing
------------

We'd love to hear if you use the server for anything, also merge requests and bug reports are always welcomed.

A list of [contributors](docs/contributors.md) can be found in the docs directory.

### license

This project is licensed under the MIT license - see [LICENSE.txt](docs/LICENSE.txt) file for details.

