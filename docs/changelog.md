
* slightly improved systemd integration
* rproxy fixed passing hop-by-hop headers to the proxied server
* added packaged systemd .service file and openrc init.d files
* fixed some errors in the CMakeFile, not linking zlib to the hin library, not linking systemd when needed, not linking pthread for the library, etc
* fixed server quitting when not connected to a tty device

commit 071069a3a7f19f80cf3f3d0c604e3c4f4d87d99f
* rproxy now uses the client requested hostname not the internal server hostname, will make it configurable in the future
* logging: annoy now prints an ip address so you can use things like fail2ban easier
* internal: added a few more http methods
* testing: display segfault messages when the server segfaults, previously it daemonized preventing bash from handling it properly
* fixed a use-after-free bug related to cancelling of uring requests

commit c0d20f4984b439d91a345a5901b9b07152a6d3b8
* re-enabled http POST support to rproxy
* made the compilation of the daemon related functions (daemonize, pidfile, graceful restart, drop user) optional
* made the compilation of the reverse proxy optional
* debug print proper method name instead of method number when http debug is enabled
* fixed and refactored the post code from hin9
* fixed possible segfault introduced when pipe got bandwidth tracking, wasn't checking if pipe stats are being used
* fixed some compilation options that weren't doing anything (like disabling threads)

commit 3a9ec4cb34fa9cc33a4e02e9e8f88d13e9e48b5d
* proxy added X-Forwarded-Proto header
* fixed a few compilation issues

commit 69a63a71f97434bb4f25e338f42b4d3da832af3e
* proxy added X-Forwarded-For header
* proxy added X-Forwarded-Host header
* allow PUT, DELETE, CONNECT, TRACE methods for backends that support them
* internal reordered and renumbered HIN\_METHOD\_* values
* internal removed type field from buffer and moved it to flags
* internal removed type field from client, was not used
* internal added flag to hin\_client\_addr so you can get only the ip part
* fixed new ssl not freeing certain closed connections

commit 76e8486b2d89777ea90b0f6e35fbf86703ec273d
* improved reverse proxy config options, now you can put your reverse proxy on a subdirectory of the root
* added a way to target directories inside a vhost, used in rproxy but will be used in many places in the future
* remove debug information for parsing commands in the built-in command line
* http clients now use the global debug mask in addition to their vhost debug mask
* improved console agument --help: by default only show a basic overview of commands, full help can be toggled by adding a parameter like this `--help all`
* added console argument --debug: now you can set a specific debug mask without recompilation, it even works at runtime from the command line prompt
* added console argument --color: now you can toggle if the server uses ANSI color or not
* added console argument --path: use it to target a directory instead of the full vhost
* added console argument --keepalive: toggle if the reverse proxy uses keepalive connections, atm it's bugged
* other refactoring

commit e61fa4e8a3156836c0f08c50db4e75e34d9b0818
* added a simplified reverse proxy and a way to configure it
* ported the http client code to multithreading
* lots of fixes to http client due to a big rewrite
* tests fixed ignore return code of hammer subtest, this made the test pass despite it failing
* fixed use-after-free timers were not being properly cleaned when timeout happens

commit b3e0908c2ef8308d4141ad284276fbd39ce71ef6
* ssl added a timeout in case the remote peer doesn't respond to a close event
* ported drop user functionality from hin9 and fixed some error messages inside it
* refactored timers to use a single function
* teaked buffer size hin\_bufsz slightly
* tests cleaned up some '/' from paths
* tests error color set to bright red/green instead of dark red/green
* tests imported patch to make tests build path user specified
* tests more descriptive/better messages when creating test files
* fixed new ssl not reinitializing the buffer size, leading to an smaller and smaller buffers
* fixed new ssl use-after-free on last buffer when an error happens
* fixed new ssl weird error 7998734 handling to make it less likely
* fixed new ssl not unlinking buffers, that could potentially lead to use-after-free
* fixed new ssl various bugs
* fixed error messages not being thread safe and added color to them
* fixed missing time limit inside a compare and swap loop
* fixed some global variables not using the new compare and swap system

commit 73e182ae0581d84251db0ff0ff5c15d9860bd9f5
* refactored the ssl code fixing multiple bugs and making maintenance easier
* added ssl graceful shutdown, preventing a certain class of exploits
* added console argument --threads: replacement for --num_cores, looks nicer
* added more checks to openssl, before it didn't even check root certificates
* increased BUFSZ from 4096 to 8192-sizeof(hin\_buffer\_t)
* increased client idle timeout from 3 seconds to 5 seconds
* tests added multiple new ssl tests
* tests added hammer ssl compressed test
* tests added hammer ssl over fd limit test
* tests added hammer ssl medium sized file test
* tests added a timer for how long tests take
* tests fixed bad url in 06\_static\_raw.sh
* fixed use-after-free because pipe timer kept running after the client was cleaned
* fixed various bugs in ssl handling due to refactoring and probably added new bugs to replace the old, hopefully fewer new bugs

commit 6a290466e7ede94bd6f97b3a7b0d95704b70188e
* added HTTP OPTIONS method
* added test for HTTP OPTIONS method
* fixed use-after-free on ssl connection terminating before headers are received

commit e81017c2cdb661bf4827895a2d083dc194c8091d
* added a global max memory usage limit
* added a hin\_buffer\_alloc function to make it easier to manage buffers
* replaced every instance of malloc, calloc, realloc and free with internal names for future use
* unit tests added for basic\_hashtable
* unit tests added for basic\_lists

commit 1ec8e9b7bc6702ef09a4c3f56a8d5a4df5c6e262
* DDoS protections: don't allow very slow clients
* DDoS protections: implemented a global per connection maximum download speed
* tweaks to the readme, introduced TOC, rewording etc
* unit tests added for basic\_pattern
* added signal handler for graceful shutdown
* API moved the automatic pipe hasing to a stats struct so they don't take up memory unless needed
* API pipe removed some pointers and moved structure members
* fixed timers start after the current time so they don't last forever
* fixed daemon process lingering after tests are done
* fixed restart code not closing the input

commit 0ef4b0a990064ccc490b0db3383fc7855b8ae143
* added annoy framework to make it easy to reject clients based on bad behaviour
* readded protections against a certain type of slow loris attack
* added a Dockerfile to generate a container using the latest source
* improved readme
* tests added slowloris subtest for repeated small reads
* tests improved output of a few tests
* fixed bug in thread exit cleanup
* fixed signal fd not using the value set in config
* fixed signal fd not incrementing number of fd's
* fixed musl compilation (again)

v0.10.3
commit 9762d670d50da374dec878578b2179d40867ce95
* added graceful restart code back and fixed a few problems with the previous version
* daemonize code now waits for the server to be online before exiting
* added a few more thread safe operations so it's easier to share the global hin\_g state

commit cc8a22b7463ac4cec816e0148c4b4ff9833cae0c
* remove idle http connections when overloaded
* cache idle http connections so it's faster to find them
* increased default idle timeout so it's easier to work with telnet in development mode
* added a rate limit to some debug messages so they only show every second
* fixed use-after-free when the http client cancels a connection

commit 0fa419b75ddfb1a02309833fdd721ada6501a00b
* added a timeout for idle httpd connections, so we don't keep connections forever
* now idle httpd connections are closed when you exit
* added console argument: --no_cmdline so you can skip the console input without creating a full daemon
* added code to measure sub second precision timers and timeouts
* added logo to repos so it actually shows in the readme
* tests now stop processing more tests if a crash is detected
* fixed not setting the default vhost when you start a new request
* fixed vhost search order prevented some vhosts from being found
* fixed not issuing error if you can't initialize the zlib library
* tweaked some zlib parameters
* API started using hin\_request\_cancel for hin\_buffer\_stop\_clean
* API added a new function to set variables in a thread safe-ish way
* API moved some flags from the hin\_g to hin\_app because they made no sense there
* API moved setting default vhost from the header read callback to the httpd client start request function
* API basic hashtable set pair now returns a pair instead of a success condition
* API added an union for the data part of the basic\_hashtable pair so you can use the data in more places
* API code that uses hashtable always use the first data value instead of the 2nd

commit 675073c8bbaabf05b640f8de9c3ca589228423bf
* added some initial systemd new style daemon support
* rewrote part of the readme
* fixed waiting for busy socket
* fixed not cleaning fd for sockets that are marked as busy
* fixed errors in compilation of the shared library
* fixed daemonized server still trying to start the command line interpreter
* fixed daemonize code not closing stdin and stdout
* fixed testing not using the port and vhost specified via environment variables because it didn't define a vhost as an program argument so it used the default
* tests added test for long paths inside large directories
* tests added subtest for aborted connections, doesn't have a fail condition but if the server fails it should be detected in the later tests
* tests now abort if the pid is not written to the pidfile
* tests now create a directory filled with a large number of small files for the long paths test
* added more verbose debug output, print when loading a default config file
* added O\_NOCTTY to some open calls

commit b0a51b816c3ff93484b33bd976f0817342bb19c0
* fixed segfault when run from the console via arguments without specifying a mime db
* added console argument: --force\_epoll disable the use of io\_uring and only use epoll, it's slower and not very well tested

v0.10.2
commit 18ece9cebfd8ab4806f2248f41e09d81298f350d
* server now uses the default config file in the workdir/config.ini if no other options set
* added console argument: --check checks current config and reports errors
* added console argument: --allow\_empty\_cert don't abort server if you're missing the ssl certificate, useful for simple configs
* fixed basic\_args not matching names with punctuations in them
* fixed argument warnings appearing when --quiet is set

commit f0c143d9b5b60bcf6a182f09c0550ed8b5717d87
* fixed misordered data that caused incomplete responses
* fixed pipe not writing data if offsets if disabled on file targets (not used atm)
* added descriptive names to the the argument's parameters in the program's --help
* modified console argument: --port arguments order switched, before it was bind address > port number now it's port number > bind address, it's easier to understand if you read the --help
* don't condense buffers if you have more queued data than bufsz
* added a request cancel function, todo: test if this works
* use the port set in the config for the tests
* switched some pipe debug messages from HNDBG\_HTTP to HNDBG\_PIPE
* now we keep the offset into the pipe/stream even for unoffseted writes (could be useful for debugging)

commit d0ec626012a2c9bb1edbb4b5c1349cc9da582632
* added a command line parser so you can issue commands while the server is running and also exit the server cleanly
* refactored the io uring event loop, simple is better
* added console argument: --daemonize this detaches the process from the console and runs it in the background
* added console argument: --pidfile save the PID to a file so you can track the process after a daemonize
* docs reworded part of the --help command
* tests removed random sleep by switching to daemonize
* fixed a few compiler warnings regarding asprintf and null variables

commit 2de5a00207e8b8cbfbfbcf54b6963b84754d274c
* conform to http spec and reject requests containing headers with the null character
* fixed not responding with an error to http/2 connections
* fixed `make check` by adding all tests to it
* tests added subtest to check if and invalid method returns status 501
* tests added subtest to check if and invalid version returns status 505
* tests better temporary file paths
* tests removed dependency on qpdf and replaced with perl (perl was already required)
* increased hin uring queue depth
* docs added required programs to the testing section

commit 7c37409d10f0f61ce8c710599d9fd0d67a072275
* server now adds trailing slash for directories without it
* added console argument: --trailing trailing slash redirect flag
* added test for trailing slash redirect
* fix not sending redirects (server did not previously send any redirects)
* removed warning message for unkown file types in basic\_vfs for block devices, pipes, etc
* added handling of incomplete reads to pipe

commit e198a5a03150b59567cd00210e99142dca3c1082
* fixed major bug that caused low performance when the file being served is larger than the headers sent
* cache the size of the write que so you don't have to iterate over a whole linked list every request

commit d0017da47dc2f09f30a0c2766c9ab96745f9b41c
* try to balance the number of requests over all the threads
* added toggle if you want to log 4xx status (client side errors like 404 or 403)
* made using the cork algorithm easier to configure
* API changes reordered the hin debug flags
* decreased the time between maintenance updates
* 403 and 404 status errors are now keepalive enabled increasing performance
* now http error logging should be kept on a single line even when using multiple threads
* tweaked http error messages for 404 and 403 status
* fixed sending 403 instead of 405 when POST is disabled
* fixed double free on certificate cleanup due to not unlinking certificates from the list

commit 0303936a5816fe9d9a12663106cff74523c39bf3
* improved handling of mime types by parsing the OS mime database
* added a parser for /etc/mime.types
* added console argument: --mimes read a /etc/mime.type style database

commit 47f1c6440dea115b4d8f32de3419aa327f97086d
* reworked server configuration via command line arguments (and new system too)
* added console argument: cert, port, ports, htdocs, config
* added a simple config file parser that uses the same commands as the command line options
* improved --help output so it shows argument parameters and other tweaks

commit 001c67f9ba6f0c7c9f7681bd6a9adf6aa772b255
* better out-of-fd overload protection
* better tracking of the fds used
* reaccept no longer iterates over every other thread's accept buffer
* added callbacks to vfs when getting/releasing a fd
* added proper 503 error when you can't obtain a fd for a file from the vfs
* fixed race condition on server stop
* fixed race condition on free of certs and vhosts
* fixed vfs printing certain debug messages when it shouldn't

commit 480802e1c88b1590dfe5da8798db898c62aa7bfa
* added tcp cork config
* pointed readme to the proper page

v0.10.1
commit 9246987bb2ffa67850554d942d45d851a507a4bb
* added httpd access logging in the common log format
* added console argument: --access_log create the httpd access log at the path specified, if this is not set then no access log is created
* API changed the way httpd client query string is parsed, now it includes the beginning ? so it's easier to use in logs, anything that needs to parse the query string will have to be updated
* fixed double dereferencing of the file node on 405 error: POST on static resource
* tests added for jquery over ssl
* tests added subtest for POST on static resource
* tests added subtest for checking if HEAD requests sent bytes
* tests for 304 status now check for response length (should only allow 0 bytes responses)
* tests renamed large text test to jquery

commit 8c1e03dd05bf387d642e390e931f966ec86eb015
* refactored ssl certificate handling
* restored sni negociation
* removed old defines from conf.h, updated version name
* tweaked tests so they don't test deflate in other servers
* temporary fix for poor performance introduced in commit ec93e5e31e5e0b66497cb4dbee173ccd4bab3c33

commit ce701cdac9cb514367a49a073147e4ffff7c6215
* restored and rewrote the compression code so it's located in a separate file
* restored tests for hammer-ing deflate and gzip
* fixed deflate test not checking for a deflate response
* renamed hin\_lua\_init to hin\_config\_init

commit ec93e5e31e5e0b66497cb4dbee173ccd4bab3c33
* fixed bug where pipe did not properly mark when the pipe is flushed, generating premature flushes and ruining filters: deflate, chunked, etc
* fixed vhosts not getting the default debug mask
* fixed vhosts not setting a default vhost for http1.0 requests
* fixed overload errors generating read requests
* moved where default vhost initialization is set so the code is not run multiple times

commit cbfb74c0cc6f69bd41415f782b372028d30d6870
* added more tests for 304: etag and last modified
* restored previously disabled 304 last modified code
* made the previous 'file special codes' test more verbose
* now each thread tracks the number of connected clients
* split src/http/utils/string.c in 2 files: string.c and chunked.c

v0.10.0
commit 9c715a03d95b5bad00b4457d05ad7706f07c6daa
* added a readme and other basic documentation
* renamed lua.c to config.c
* removed some unused declarations from the CMakeLists.txt file

commit 29ddf434c48889f8ead2ceaf4114c01f78a89a48
* added tests and fixed the server so it can pass said tests
* improved some of the test from the hin9 version, better fd limit handling, etc
* added default htdocs/tests dir for tests

commit ff10bad1432ac07ad9761083e54008bd183e0960
* added range requests (no multi range)
* added content type header

commit 0d77a4ea8854ed27fa198b9b419381fb89b90dd3
* added ability to change the file descriptor cap and set it to max allowed by OS
* removed very low overload limit temporarily
* fixed segfault when overloaded

commit 046076abb69d35416d93865c19fdae99b4b1579b
* added signal handling: SIGPIPE and SIGINT

commit 2a45e9c10dcf9c9295f572f1f1b684b0407cfe59
* fixed 403 errors

commit 7bc5add31fc271e634cdff7c72d513cfb09302fd
* now number of threads is set to the number of available cores
* added console argument: --num_cores number of threads
* added console argument: --log redirect stdout/stderr to log file
* now listens by default to 8080 and ssl/8081 and a default vhost in htdocs/

commit 52f7fa699a0c01e2e7ac3b7449dbbfa069c10404
* console argument parsing improvements, now check if correct number of params are passed
* added console argument: --listen to listen to a socket
* added console argument: --vhost to create a vhost and a document root
* removed HNDBG\_VFS from the default debug mask

commit e5f1a14a1588d2407f5ac78bf137d8cb741bb7e3
* rewrote the basic\_vfs api so it fits better with multi-threading
  renamed some vfs functions to better represent what they should do
  simplified vfs API and code to improve thread-safety
* another debug flag for basic\_vfs: BASIC\_VFS\_FDS

commit f320f95081441a8424f9c71405df717e1cba67cd
* actually implemented the file backend, not just a null backend, no mutexes yet though, so it will crash
* basic\_vfs now tracks current working dir, I don't know why it didn't in the past
* added a better debugging to basic\_vfs
* lock free cleaning of clients, should improve performance quite a bit

commit 02b982ebf3998f362c42b68eedd07b0f7647ac82
* added multithreading

commit 8f152494f956235f646f8ef815abe6bdca548113
* rewrote http server, no serve file support or most functions
* numerous style changes
* renamed some headers master.h to hmaster.h, etc
* separated hin.h into multiple headers
* renamed READ_SZ to HIN\_BUFSZ
* added a fds count and a max fds
* switched to unified builds by module

