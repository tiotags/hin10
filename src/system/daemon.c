
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef HIN_USE_DAEMON

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <syslog.h>

#include "hin.h"
#include "hmaster.h"
#include "hin_app.h"

static int pipe_fd = 0;

int hin_pidfile (const char * path) {
  int fd = openat (AT_FDCWD, path, O_WRONLY | O_CREAT | O_CLOEXEC | O_TRUNC | O_NOCTTY, 0644);
  if (fd < 0) {
    if (errno == EEXIST) {
      hin_debug ("should clean up old pidfile %s\n", path);
    }
    hin_error ("can't open '%s': %s\n", path, strerror (errno));
    exit (1);
  }
  char buf[256];
  int ret = snprintf (buf, sizeof buf, "%d\n", getpid ());
  if (ret < 0) return -1;
  int err = write (fd, buf, ret);
  if (err < ret) { perror ("write"); return -1; }
  err = close (fd);
  if (err < 0) { perror ("close"); return -1; }
  return 0;
}

int hin_pidfile_clean () {
  if (hin_app.pid_path) {
    if (unlinkat (AT_FDCWD, hin_app.pid_path, 0) < 0)
      perror ("unlinkat");
  }
  return 0;
}

int hin_daemonize_final () {
  if (pipe_fd) {
    if (write (pipe_fd, "OK\n", 3) != 3) hin_perror ("write");
    close (pipe_fd);
    pipe_fd = 0;
  }
  return 0;
}

int hin_daemonize () {
  pid_t pid;

  int fds[2];
  char buf[1];
  if (pipe (fds) != 0) {
    hin_perror ("daemonize pipe");
  }

  pid = fork ();
  if (pid < 0) {
    perror ("fork");
    exit (EXIT_FAILURE);
  }
  if (pid > 0) {
    close (fds[1]);
    if (read (fds[0], buf, 1) <= 0) hin_perror ("read");
    exit (EXIT_SUCCESS);
  }

  if (setsid () < 0) {
    perror ("setsid");
    exit (EXIT_FAILURE);
  }

  //TODO: Implement a working signal handler
  // should default all signals
  signal (SIGCHLD, SIG_IGN);
  signal (SIGHUP, SIG_IGN);

  pid = fork ();
  if (pid < 0) {
    perror ("fork");
    exit (EXIT_FAILURE);
  }
  if (pid > 0) {
    close (fds[1]);
    if (read (fds[0], buf, 1) <= 0) hin_perror ("read");
    exit (EXIT_SUCCESS);
  }

  close (fds[0]);
  pipe_fd = fds[1];

  umask (0);

  // Change the working directory to the root directory
  // or another appropriated directory
  if (chdir ("/") == -1) {
    hin_perror ("chdir daemonize");
  }

  for (int x = sysconf (_SC_OPEN_MAX); x >= 3; x--) {
    //close (x);
    // this is (should) done in the limits file
  }

  // close stdin/stdout
  int null_in = open ("/dev/null", O_RDONLY | O_NOCTTY);
  if (dup2 (null_in, STDIN_FILENO) == -1) {
    hin_perror ("dup2");
  }
  hin_cmdline_clean ();

  if ((hin_app.flags & HIN_APP_REDIRECTED_OUTPUT) == 0) {
    hin_redirect_log ("/dev/null");
  }

  // Open the log file
  openlog ("hin", LOG_PID, LOG_DAEMON);
  return 0;
}

#else

#include "hin.h"
int hin_daemonize () {
  hin_error ("no daemonize/pidfile support compiled");
  return -1;
}
int hin_pidfile (const char * path) {
  hin_error ("no daemonize/pidfile support compiled");
  return -1;
}

#endif

