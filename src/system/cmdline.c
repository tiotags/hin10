
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <basic_pattern.h>
#include <basic_args.h>

#include "hin.h"
#include "hmaster.h"
#include "http/http.h"
#include "conf.h"

#include "hin_app.h"

static hin_buffer_t * cmdline_buffer = NULL;

static int cmdline_execute (string_t * source) {
  int hin_process_argv (basic_args_t * args, const char * name);

  if (matchi_string_equal (source, "q") > 0) {
    printf ("do quit\n");
    hin_app_stop ();
  } else {
    basic_args_parse_line (source, hin_process_argv);
    if (source->len > 0) {
      hin_error ("unkown command '%.*s'\n", source->len, source->ptr);
    }
  }
  return 0;
}

static int hin_cmdline_read_callback (hin_buffer_t * buf, int ret) {
  if (ret <= 0) {
    if (ret < 0) {
      hin_error ("cmdline: %s", strerror (-ret));
    }
    if (hin_g.debug & HNDBG_CONFIG) hin_debug ("cmdline EOF %s\n", isatty (0) ? "closing" : "not a tty");
    if (isatty (0)) {
      hin_app_stop ();
    }
    return 0;
  }

  string_t source, line;
  source.ptr = buf->ptr;
  source.len = ret;

  while (1) {
    if (source.len <= 0) break;
    int ret = hin_find_line (&source, &line);
    if (ret == 0) break;
    if (ret < 0) hin_error ("cmdline bad characters in read");
    if (line.len == 0) break;
    cmdline_execute (&line);
  }

  if (hin_request_read (buf) < 0) {
    hin_weird_error (897455);
    return -1;
  }
  return 0;
}

int hin_cmdline_clean () {
  hin_struct_lock (&hin_g);
  if (cmdline_buffer) {
    cmdline_buffer->flags &= ~HIN_ACTIVE;
    hin_buffer_stop_clean (cmdline_buffer);
    cmdline_buffer = NULL;
  }
  hin_struct_unlock (&hin_g);
  return 0;
}

int hin_cmdline_init () {
  if (feof (stdin) || (hin_app.flags & HIN_CMDLINE_DISABLE)) {
    hin_debug ("cmdline stdin was closed\n");
    return 0;
  }
  if (!isatty (0)) {
    // TODO temporary until the proper system is implemented
    hin_debug ("cmdline not connected to a terminal\n");
  }

  hin_buffer_t * buf = hin_buffer_alloc (HIN_BUFSZ);
  #ifdef HIN_LINUX_BUG_5_11_3
  //buf->flags |= HIN_EPOLL;
  #endif
  buf->fd = STDIN_FILENO;
  buf->callback = hin_cmdline_read_callback;
  buf->debug = hin_g.debug;
  if (hin_request_read (buf) < 0) {
    hin_error ("cmdline init failed");
    hin_buffer_clean (buf);
    return -1;
  }
  cmdline_buffer = buf;
  return 0;
}



