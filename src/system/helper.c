
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#include "hin.h"
#include "hmaster.h"

#include "hin_app.h"

int hin_open_file_and_create_path (int dirfd, const char * path, int flags, mode_t mode) {
  int fd = openat (dirfd, path, flags, mode);
  if (fd >= 0) return fd;
  if (errno != ENOENT) return fd;
  if ((hin_g.flags & HIN_CREATE_DIRECTORY) == 0) return fd;

  mode_t mask = 0;
  if (mode & 0600) mask |= 0100;
  if (mode & 060) mask |= 010;
  if (mode & 06) mask |= 01;

  const char * ptr = path;
  while (1) {
    if (*ptr == '/') {
      ptr++;
      continue;
    }
    ptr = strchr (ptr, '/');
    if (ptr == NULL) return openat (dirfd, path, flags, mode);
    char * dir_path = strndup (path, ptr-path);
    int err = mkdirat (dirfd, dir_path, mode | mask);
    if (err == 0) {
      if (hin_g.debug & HNDBG_CONFIG) {
        hin_debug ("created for '%s' directory '%s'\n", path, dir_path);
      }
      free (dir_path);
      continue;
    }
    free (dir_path);

    switch (errno) {
    case ENOENT: break;
    case EEXIST: break;
    default:
      return -1;
    break;
    }
  }
}

int hin_redirect_log (const char * path) {
  int fd = hin_open_file_and_create_path (AT_FDCWD, path, O_WRONLY | O_APPEND | O_CLOEXEC | O_CREAT | O_TRUNC | O_NOCTTY, 0660);
  if (fd < 0) {
    hin_error ("can't open log '%s': %s\n", path, strerror (errno));
    return -1;
  }

  fflush (stdout);
  fflush (stderr);
  setvbuf (stdout, NULL, _IOLBF, 1024);
  setvbuf (stderr, NULL, _IOLBF, 1024);

  if (dup2 (fd, STDOUT_FILENO) < 0) hin_perror ("dup2 stdout");
  if (dup2 (fd, STDERR_FILENO) < 0) hin_perror ("dup2 stderr");
  close (fd);

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("create log on %d '%s'\n", fd, path);

  hin_app.flags |= HIN_APP_REDIRECTED_OUTPUT;

  return 0;
}
