
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include <basic_args.h>
#include <basic_pattern.h>
#include <basic_vfs.h>

#include "hin.h"
#include "hmaster.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_app_extra.h"

typedef struct {
  int id;
  pthread_t pthread;
} hin_thread_t;

hin_app_t hin_app;
static hin_thread_t * threads = NULL;

void hin_app_clean () {
  hin_signal_clean ();
  hin_config_clean ();
  hin_vfs_clean ();
  hin_mime_types_clean ();
  hin_cmdline_clean ();
  hin_stop ();

  hfree ((void*)hin_app.username);
  hfree ((void*)hin_app.groupname);

  hin_clean ();

  if (hin_g.debug & HNDBG_BASIC)
    hin_debug ("hin close ...\n");

  #ifdef BASIC_USE_MALLOC_DEBUG
  hin_debug ("num fds open %d\n", print_fds ());
  print_unfree ();
  #endif
}

void hin_app_stop () {
  sd_notify (0, "STOPPING=1");
  hin_stop ();
}

static void hin_init_all (hin_thread_t * thread) {
  memset (&hin_t, 0, sizeof hin_t);
  hin_t.id = thread->id;
  //printf ("create thread %d\n", hin_t.id);

  hin_init ();
}

static void hin_run_all (hin_thread_t * thread) {
  while (hin_event_wait ()) {
    //hin_event_process ();
  }
}

#ifdef HIN_USE_THREAD
static void * thread_function (void * ptr) {
  hin_thread_t * thread = ptr;
  hin_init_all (thread);

  hin_server_start_thread ();

  hin_run_all (thread);

  hin_clean ();

  return NULL;
}

int hin_run_threads () {
  for (int i=1; i < hin_g.num_thread; i++) {
    hin_thread_t * thread = &threads[i];
    thread->id = i;
    int err = pthread_create (&thread->pthread, NULL, thread_function, thread);
    if (err) {
      hin_error ("pthread_create %s", strerror (err));
      //return -1;
    }
  }
  return 0;
}

int hin_stop_threads () {
  for (int i=1; i < hin_g.num_thread; i++) {
    hin_thread_t * thread = &threads[i];
    void * ptr = NULL;
    pthread_join (thread->pthread, &ptr);
  }
  return 0;
}
#else
int hin_run_threads () { return -1; }
int hin_stop_threads () { return -1; }
#endif

int main (int argc, const char * argv[], const char * envp[]) {
  memset (&hin_g, 0, sizeof hin_g);
  memset (&hin_app, 0, sizeof hin_app);
  hin_app.conf_path = HIN_CONF_PATH;
  hin_app.exe_path = realpath ((char*)argv[0], NULL);
  hin_app.argc = argc;
  hin_app.argv = (char**)argv;
  hin_app.envp = (char**)envp;

  hin_g.num_thread = hin_num_cores ();
  hin_g.debug = HIN_DEBUG_MASK;
  hin_g.flags |= HIN_ANSI_COLOR;
  //hin_g.max_client = HIN_HTTPD_DEFAULT_MAX_CONNECTIONS;

  hin_config_init ();

  int ret = basic_args_process (argc, argv, hin_process_argv);
  if (ret) {
    if (ret > 0) return 0;
    return -1;
  }

  threads = halloc_z (sizeof (hin_thread_t) * (hin_g.num_thread));

  hin_cmdline_init ();

  if (hin_g.debug & HNDBG_BASIC) {
    hin_debug ("hin start ...\n");
    hin_debug (" creating main+%d threads\n", hin_g.num_thread - 1);
  }

  if (HIN_RESTRICT_ROOT && (geteuid () == 0) && hin_app.username == NULL) {
    if (HIN_RESTRICT_ROOT == 1) {
      hin_debug ("WARNING! process started as root\n");
    } else if (HIN_RESTRICT_ROOT == 2) {
      hin_error ("not allowed to run as root");
      exit (1);
    }
  }

  hin_linux_set_limits ();
  hin_signal_init ();

  if (hin_config_process () < 0)
    return -1;


  if ((hin_app.flags & HIN_DAEMONIZE) && ((hin_app.flags & HIN_PRETEND) == 0)) {
    if (hin_daemonize () < 0) { return -1; }
  }
  // pid file always goes after daemonize
  if (hin_app.pid_path) {
    if (hin_pidfile (hin_app.pid_path) < 0) { return -1; }
  }

  hin_init_all (&threads[0]);

  if (hin_app.flags & HIN_PRETEND) {
    goto final;
  }

  if ((hin_g.flags & HIN_FLAG_RUN) == 0) {
    hin_error ("no work to do, exiting");
    return -1;
  } else if (hin_g.debug & HNDBG_BASIC) {
    hin_debug ("hin serve ...\n");
  }

  hin_run_threads ();

  hin_config_startup_done ();

  if (hin_app.username) {
    if (hin_drop_user (hin_app.username, hin_app.groupname) < 0)
      return -1;
  }

  hin_run_all (&threads[0]);

  hin_stop_threads ();

final:
  hin_app_clean ();
  free (threads);

  if ((hin_g.mem_usage != 0) && (hin_g.debug & HNDBG_BASIC))
    hin_error ("memory leak amount is %ld", hin_g.mem_usage);

  return 0;
}


