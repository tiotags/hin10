
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "hmaster.h"

#include <basic_hashtable.h>
#include <basic_pattern.h>

#define HIN_DEFAULT_MIME_HASH 546745756

typedef struct hin_mime_types_struct {
  int num_mimes, num_exts;
  basic_ht_t exts;
  pthread_mutex_t mutex;
} hin_mime_types_t;

static hin_mime_types_t * mime_types = NULL;

static int hin_mime_types_init () {
  hin_struct_lock (&hin_g);
  if (mime_types) goto finish;

  mime_types = halloc_z (sizeof (hin_mime_types_t));
  basic_ht_init (&mime_types->exts, 2048, HIN_DEFAULT_MIME_HASH);

finish:
  hin_struct_unlock (&hin_g);
  return 0;
}

void hin_mime_types_clean () {
  if (mime_types == NULL) return ;
  hin_mime_types_t * mime = mime_types;
  hin_struct_lock (mime);
  basic_ht_iterator_t iter;
  basic_ht_pair_t * pair;
  memset (&iter, 0, sizeof iter);
  while ((pair = basic_ht_iterate_pair (&mime->exts, &iter)) != NULL) {
    if (pair->d.ptr.value2) free ((void*)pair->d.ptr.value1);
  }
  basic_ht_clean (&mime->exts);
  mime_types = NULL;
  hin_struct_unlock (mime);
  free (mime);
}

static int get_line (FILE * fp, char ** str) {
  char buffer[1024];
  unsigned int buf_pos = 0;
  do {
    int c = fgetc (fp);
    if (c == '\r') continue;
    if (feof (fp)) { break; }
    if (c == '\n') {
      if (buf_pos == 0) continue;
      break;
    }
    if (buf_pos >= sizeof buffer - 1) continue;
    buffer[buf_pos] = c;
    buf_pos++;
  } while (1);
  buffer[buf_pos] = '\0';
  if (buf_pos == 0) {
    if (feof (fp)) { return -1; }
    return buf_pos;
  }
  *str = strdup (buffer);
  return buf_pos;
}

int hin_mime_types_read (const char * path) {
  if (mime_types == NULL) {
    if (hin_mime_types_init () < 0) return -1;
  }

  hin_struct_lock (mime_types);
  int ret = 0;
  FILE * fp = fopen (path, "r");
  if (fp == NULL) {
    fprintf (stderr, "can't mime types file '%s': %s\n", path, strerror (errno));
    ret = -1;
    goto finish;
  }
  if (hin_g.debug & (HNDBG_CONFIG|HNDBG_MIME))
    hin_debug ("loading mime type database from '%s'\n", path);

  while (1) {
    char * line = NULL;
    int used = get_line (fp, &line);
    if (used < 0) break;
    if (*line == '#') {
      free (line);
      continue;
    }
    //printf ("line is %d '%.*s'\n", used, used, line);

    string_t source, mime, param;
    source.ptr = line;
    source.len = used;
    used = match_string (&source, "([^%s]+)", &mime);
    if (used <= 0) {
      continue;
    }
    mime_types->num_mimes++;
    char * mime_str = NULL;
    int first = 1;

    while (1) {
      used = match_string (&source, "%s+([^%s]+)", &param);
      if (used <= 0) break;
      if (hin_g.debug & HNDBG_MIME)
        hin_debug ("  found extension '%.*s' for mime '%.*s'\n", (int)param.len, param.ptr, (int)mime.len, mime.ptr);
      mime_types->num_exts++;
      basic_ht_hash_t h1, h2;
      basic_ht_hash (param.ptr, param.len, mime_types->exts.seed, &h1, &h2);
      if (mime_str == NULL) mime_str = strndup (mime.ptr, mime.len);
      basic_ht_set_pair (&mime_types->exts, h1, h2, (uintptr_t)mime_str, first);
      first = 0;
    }
    if (first) free (mime_str);

    free (line);
    //if (ret) break;
    if (feof (fp)) break;
  }

finish:
  if (fp) fclose (fp);
  hin_struct_unlock (mime_types);

  return ret;
}

const char * hin_mime_types_find (const char * ext, int len) {
  if (mime_types == NULL) {
    return NULL;
  }
  basic_ht_hash_t h1, h2;
  basic_ht_hash (ext, len, mime_types->exts.seed, &h1, &h2);
  basic_ht_pair_t * pair = basic_ht_get_pair (&mime_types->exts, h1, h2);
  if (pair == NULL) return NULL;
  return (char*)pair->d.value1;
}


