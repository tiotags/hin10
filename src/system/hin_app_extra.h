
#ifndef HIN_APP_EXTRA_H
#define HIN_APP_EXTRA_H

#include <basic_args.h>

int hin_process_argv (basic_args_t * args, const char * name);


#include "http/vhost.h"

httpd_server_t * httpd_create (const char * addr, const char * port, const char * sock_type, void * ssl_ctx);
int httpd_handle_file_request (httpd_client_t * http, off_t pos, off_t count);
int hin_vfs_vhost_set_cwd (httpd_vhost_t * vhost, const char * path);


int hin_config_create_rproxy (httpd_vhost_t * vhost, httpd_vhost_map_t * map, const char * url);


#ifdef HIN_USE_SYSTEMD
#include <systemd/sd-daemon.h>
#else
#define sd_notify(x,msg) 
#define sd_notifyf(x,msg,...) 
#define sd_pid_notifyf_with_fds(pid,x,fds,num,msg,...) 
#endif

int hin_daemonize_final ();
void hin_app_restart_final ();

#endif

