
#ifndef HIN_APP_H
#define HIN_APP_H

enum {
HIN_APP_REDIRECTED_OUTPUT = 0x1,
HIN_CMDLINE_DISABLE = 0x2,
HIN_PRETEND = 0x4,
HIN_DAEMONIZE = 0x8,
};

typedef struct {
  char * conf_path;
  char * exe_path;
  uint32_t flags;
  int restart_fd;
  char * pid_path;
  char * access_log_path;
  char * username, * groupname;
  int argc;
  char ** argv;
  char ** envp;
} hin_app_t;

extern hin_app_t hin_app;

void hin_app_clean ();
void hin_app_stop ();
int hin_app_restart ();

int hin_config_init ();
int hin_config_process ();
int hin_config_startup_done ();
void hin_config_clean ();
void hin_vfs_init ();
int hin_vfs_clean ();

int hin_signal_init ();
int hin_signal_clean ();

int hin_cmdline_init ();
int hin_cmdline_clean ();

int hin_daemonize ();
int hin_pidfile (const char * path);
int hin_pidfile_clean ();
int hin_drop_user (const char * username, const char * groupname);

//limits
int hin_num_cores ();
int hin_linux_set_limits ();

// mime
int hin_mime_types_read (const char * path);
const char * hin_mime_types_find (const char * ext, int len);
void hin_mime_types_clean ();

// helper
int hin_redirect_log (const char * path);

#endif

