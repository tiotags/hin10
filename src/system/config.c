
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <sys/socket.h>

#include "hin.h"
#include "hextra.h"
#include "hmaster.h"
#include "http/server.h"
#include "http/file.h"
#include "conf.h"

#include "basic_vfs.h"

#include "hin_app.h"
#include "hin_app_extra.h"

static FILE * access_log = NULL;

int hin_config_create_rproxy (httpd_vhost_t * vhost, httpd_vhost_map_t * map, const char * url) {
  httpd_vhost_scope_t * scope = &vhost->scope;
  if (map) {
    scope = &map->scope;
  }

  if (scope->rproxy_url) hfree (scope->rproxy_url);
  int len = strlen (url);
  //if (url[len-1] == '/') len--;
  scope->rproxy_url = strndup (url, len);
  scope->rproxy_url_len = len;
  return 0;
}

static int httpd_server_callback (httpd_client_t * http) {
  //httpd_respond_text (http, 200, "Hello\n");

  if (httpd_vhost_map_callback (http, HIN_VHOST_MAP_PREFILE) == 0) {
    string_t source;
    source.ptr = http->path;
    source.len = strlen (source.ptr);
    if (match_string (&source, "/") != 1) {
      httpd_respond_text (http, 400, NULL);
      return 0;
    }
    if (hin_vhost_run_scope (http, http->vhost, NULL, &source) == 0) {
      httpd_handle_file_request (http, 0, -1);
    }
  }

  if (httpd_vhost_map_callback (http, HIN_VHOST_MAP_PROCESS)) {
  }

  return 0;
}

static int httpd_client_access_log (httpd_client_t * http) {
  // strftime format %d/%b/%Y:%H:%M:%S %z
  char date_b[256];
  date_b[0] = '\0';
  time_t rawtime;
  rawtime = time (NULL);
  struct tm *tmp = localtime (&rawtime);
  if (tmp == NULL) {
    hin_perror ("localtime");
  } else if (strftime (date_b, sizeof date_b, "%d/%b/%Y:%H:%M:%S %z", tmp) == 0) {
    hin_perror ("strftime");
  }

  char hbuf[NI_MAXHOST];
  int err = getnameinfo (
    &http->c.ai_addr,
    http->c.ai_addrlen,
    hbuf,
    sizeof hbuf,
    NULL,
    0,
    NI_NUMERICHOST | NI_NUMERICSERV
  );
  if (err) {
    hin_error ("getnameinfo: %s", gai_strerror (err));
    return -1;
  }

  fprintf (access_log, "%s - - [%s] \"%s %s%s HTTP/%s\" %d %lld\n",
    hbuf,
    date_b,
    hin_http_method_name (http->method),
    http->path,
    http->query ? http->query : "",
    hin_http_version_name (hin_http_version (http->peer_flags)),
    http->status,
    (long long)http->sent_bytes
  );
  return 0;
}

static int httpd_client_finish_callback (httpd_client_t * http) {
  basic_vfs_node_t * file = http->file;

  if (httpd_vhost_map_callback (http, HIN_VHOST_MAP_FINISH)) {
  }

  if (access_log) {
    httpd_client_access_log (http);
  }

  if (file)
    basic_vfs_unref_file (http->file);

  return 0;
}

static int httpd_server_listen_callback (hin_server_t * server) {
  #ifdef HIN_USE_SYSTEMD
  int fds[2];
  fds[0] = server->c.sockfd;
  sd_pid_notifyf_with_fds (0, 0, fds, 1, "FDSTORE=1");
  #endif
  //printf ("listen callback was called with fd %d\n", server->c.sockfd);
  return 0;
}

httpd_server_t * httpd_create (const char * addr, const char * port, const char * sock_type, void * ssl_ctx) {
  httpd_server_t * server = halloc_z (sizeof *server);
  server->process_callback = httpd_server_callback;
  server->finish_callback = httpd_client_finish_callback;
  server->s.listen_callback = httpd_server_listen_callback;

  if (hin_httpd_start (server, addr, port, sock_type, ssl_ctx) == 0) {
    free (server);
    return NULL;
  }

  return server;
}

int hin_config_init () {
  hin_vfs_init ();

  return 0;
}

int hin_config_process () {
  httpd_vhost_t * vhost = httpd_vhost_default ();

  if (((hin_g.flags & HIN_FLAG_RUN) == 0) || (vhost == NULL)) {
    const char * path = "workdir/config.ini";
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("using default config file '%s'\n", path);
    if (basic_args_parse_file (path, hin_process_argv) < 0) {
      hin_debug ("could not load default config '%s'\n", path);
    }
  }

  if ((hin_g.flags & HIN_FLAG_RUN) == 0) {
    hin_error ("did not specify a socket, please load a config");
    return -1;
  }

  vhost = httpd_vhost_default ();
  if (vhost == NULL) {
    hin_error ("did not specify a vhost, please load a config");
    return -1;
  }

  if (hin_app.access_log_path) {
    access_log = fopen (hin_app.access_log_path, "a");
    // TODO make sure fopen uses O_NOCTTY
  }

  return 0;
}

int hin_config_startup_done () {
  #ifdef HIN_USE_DAEMON
  hin_daemonize_final ();
  hin_app_restart_final ();
  #endif
  sd_notifyf (0,
    "READY=1\n"
    "STATUS=hin serve ...\n"
    "MAINPID=%d", getpid ()
  );
  return 0;
}

void hin_config_clean () {
  if (access_log) fclose (access_log);
}

