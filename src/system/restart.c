
#ifdef HIN_USE_DAEMON

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "hin.h"
#include "hmaster.h"

#include "hin_app.h"
#include "hin_app_extra.h"

static void hin_app_restart_do_exec (int pipe_fd) {
  printf ("restarting to '%s'\n", hin_app.exe_path);

  hin_stop ();

  const char ** argv = halloc_z ((hin_app.argc + 5) * sizeof (char*));
  int pos=0;
  int skip_daemonize = 0;
  argv[pos++] = hin_app.exe_path;
  for (int i=1; i<hin_app.argc; i++) {
    char * param = hin_app.argv[i];
    if (strcmp (param, "--restartfd") == 0) {
      i++;
      continue;
    } else if (strcmp (param, "--restart") == 0) {
      continue;
    } else if (strcmp (param, "--daemonize") == 0) {
      skip_daemonize = 1;
    } else if (strcmp (param, "--no_cmdline") == 0) {
      skip_daemonize = 1;
    }
    argv[pos++] = hin_app.argv[i];
  }

  if (skip_daemonize == 0) {
    argv[pos++] = "--no_cmdline";
  }

  char buf[20];
  snprintf (buf, sizeof buf, "%d", pipe_fd);
  argv[pos++] = "--restartfd";
  argv[pos++] = buf;

  for (int i=0; i<hin_app.argc; i++) {
    printf ("argv[%d]=%s\n", i, argv[i]);
    if (argv[i] == NULL) break;
  }

  execvp (hin_app.exe_path, (char * const*)argv);
  hin_error ("execvp '%s': %s", hin_app.exe_path, strerror (errno));
  exit (-1);
}

static int hin_restart_pipe_read_callback (hin_buffer_t * buf, int ret) {
  if (ret <= 0) {
    if (ret < 0) {
      hin_error ("restart %s", strerror (-ret));
    }
    hin_error ("restart crashed");
    return 1;
  }
  printf ("restart succeeded\n");
  hin_app_stop ();
  return 1;
}

static int hin_app_restart_do_wait (int fd) {
  hin_buffer_t * buf = hin_buffer_alloc (HIN_BUFSZ);
  #ifdef HIN_LINUX_BUG_5_11_3
  //buf->flags |= HIN_EPOLL;
  #endif
  buf->fd = fd;
  buf->callback = hin_restart_pipe_read_callback;
  buf->debug = hin_g.debug;

  if (hin_request_read (buf) < 0) {
    hin_weird_error (54222111);
    hin_buffer_clean (buf);
    return -1;
  }

  printf ("restarting waiting finish\n");
  return 0;
}

int hin_app_restart () {
  struct timespec ts;
  clock_gettime (CLOCK_MONOTONIC, &ts);
  sd_notifyf (0,
    "RELOADING=1\n"
    "STATUS=hin restart ...\n"
    "MONOTONIC_USEC=%" PRIdMAX "%06d", (intmax_t)ts.tv_sec, (int)(ts.tv_nsec / 1000)
  );
  printf("hin restart ...\n");

  hin_pidfile_clean ();

  int info_pipe[2];
  if (pipe (info_pipe) < 0) {
    perror ("pipe");
    return -1;
  }

  pid_t pid = fork ();
  if (pid < 0) {
    perror ("fork");
    return -1;
  }
  if (pid == 0) {
    close (info_pipe[0]);
    hin_app_restart_do_exec (info_pipe[1]);
    return 0;
  }
  close (info_pipe[1]);
  hin_app_restart_do_wait (info_pipe[0]);

  return 0;
}

void hin_app_restart_final () {
  if (hin_app.restart_fd) {
    const char * msg = "restart done\n";
    int ret = write (hin_app.restart_fd, msg, strlen (msg));
    if (ret < 0) hin_perror ("write");
    close (hin_app.restart_fd);
    printf ("restart done\n");
  }
}

#else

#include "hin.h"
int hin_app_restart () {
  hin_error ("no restart support compiled");
  return -1;
}

#endif

