
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "hmaster.h"
#include "http/server.h"
#include "http/file.h"
#include "http/vhost.h"

#include "basic_vfs.h"

#include "hin_app.h"

static basic_vfs_t vfs = {0};

static int obtain_file_node (httpd_client_t * http) {
  basic_vfs_node_t * node = NULL;
  string_t path;
  path.ptr = http->path;
  path.len = strlen (path.ptr);

  //printf ("host name is %s vhost %p\n", http->hostname, http->vhost);
  httpd_vhost_t * vhost = http->vhost;
  basic_vfs_node_t * cwd_dir = vhost->cwd_dir;

  int used = match_string (&path, "/");

  if (path.len > 0) {
    node = basic_vfs_ref_path (&vfs, cwd_dir, &path);
  } else {
    node = cwd_dir;
    if (used == 1) {
      path.len++;
      path.ptr--;
    }
  }
  if (node == NULL) {
    return -1;
  }

  if (node->type == BASIC_ENT_DIR) {
    if ((match_string (&path, "/") < 0)
    && ((hin_g.flags & HIN_NO_TRAILING_SLASH) == 0)) {
      char * new = NULL;
      if (asprintf (&new, "%s/%s", http->path, http->query ? http->query : "") <= 0) {
        return -1;
      }
      httpd_respond_redirect (http, 301, new);
      free (new);
      return 0;
    }
    string_t path;
    path.ptr = "index.html";
    path.len = strlen (path.ptr);
    basic_vfs_node_t * new = basic_vfs_ref_path (&vfs, node, &path);
    if (new) {
      basic_vfs_unref_node (node);
      node = new;
    } else {
      if (http->file) {
        basic_vfs_unref_file (http->file);
      }
      http->file = node;
      return -1;
    }
  }

  if (http->file) {
    basic_vfs_unref_file (http->file);
  }
  http->file = node;
  return 0;
}

static char * find_ext (char * name, int len) {
  for (char * ptr = name + len; ptr > name; ptr--) {
    if (*ptr == '.') { return ptr+1; }
  }
  return NULL;
}

HIN_EXPORT int httpd_handle_file_request (httpd_client_t * http, off_t pos, off_t count) {
  if (http->state & HIN_REQ_DATA) return -1;
  http->state |= HIN_REQ_DATA;
  http->peer_flags &= ~(HIN_HTTP_CHUNKED);

  if (http->method == HIN_METHOD_POST) {
    httpd_error (http, 405, "post on a file resource");
    return 0;
  }

  if (http->file == NULL) {
    if (obtain_file_node (http) < 0) {
      httpd_error (http, 404, "can't find '%s'", http->path);
      return 0;
    }
    if (http->status == 301) return 0;
  }

  if (http->file == NULL) {
    httpd_error (http, 404, "no file set");
    return 0;
  }

  basic_vfs_node_t * node = http->file;
  basic_vfs_file_t * f = basic_vfs_ref_file (node);
  if (node->flags & BASIC_VFS_FORBIDDEN) {
    if (node->flags & BASIC_VFS_FD_LIMIT) {
      httpd_error (http, 503, "out-of-fds vfs");
      return 0;
    }
    httpd_error (http, 403, "can't read '%s'", http->path);
    return 0;
  }
  if (f == NULL) {
    basic_vfs_unref_node (node);
    http->file = NULL;
    httpd_error (http, 404, "can't open '%s'", http->path);
    return 0;
  }

  // find and set mime type
  char * ext = find_ext (node->name, node->name_len);
  if (ext) {
    const char * mime = hin_mime_types_find (ext, strlen (ext));
    if (mime) {
      if (http->content_type) free (http->content_type);
      http->content_type = strdup (mime);
    }
    if (http->debug & HNDBG_MIME) {
      hin_debug ("httpd %d file '%s' ext '%s' has mime '%s'\n", http->c.sockfd, node->name, ext, mime);
    }
  }

  hin_file_t file;
  memset (&file, 0, sizeof (file));
  file.type = 0;
  file.fd = f->fd;
  file.modified = f->modified;
  file.size = f->size;
  file.etag = f->etag;

  httpd_send_file (http, &file, NULL);
  return 0;
}

static hin_buffer_t * inotify_buffer = NULL;

static int hin_vfs_event_callback (hin_buffer_t * buf, int ret) {
  if (ret < 0) {
    hin_error ("inotify: '%s'", strerror (-ret));
    return -1;
  }
  basic_vfs_event (&vfs, buf->ptr, ret);

  if (hin_request_read (buf) < 0) {
    hin_weird_error (4443212);
  }

  return 0;
}

static int hin_vfs_event_init (int inotify_fd) {
  hin_buffer_t * buf = hin_buffer_alloc (HIN_BUFSZ);
  #ifdef HIN_LINUX_URING_DONT_HAVE_NOTIFYFD
  buf->flags |= HIN_EPOLL;
  #endif
  buf->fd = inotify_fd;
  buf->callback = hin_vfs_event_callback;
  buf->debug = hin_g.debug;

  if (hin_request_read (buf) < 0) {
    hin_weird_error (4443211);
  }

  inotify_buffer = buf;
  return 1;
}

int hin_vfs_vhost_set_cwd (httpd_vhost_t * vhost, const char * path_str) {
  string_t path;
  path.ptr = (char *)path_str;
  path.len = strlen (path.ptr);
  basic_vfs_node_t * cwd_dir = basic_vfs_ref_path (&vfs, NULL, &path);
  if (cwd_dir == NULL) {
    hin_error ("can't find htdocs '%.*s'", path.len, path.ptr);
    return -1;
  }
  vhost->cwd_dir = cwd_dir;
  return 0;
}

static int hin_vfs_fd_acquire (basic_vfs_node_t * node) {
  basic_vfs_t * vfs = node->vfs;
  basic_vfs_file_t * inode = node->inode;
  if (vfs->debug & BASIC_VFS_FDS)
    hin_debug ("fd++ vfs %d '%s' %d/%d\n", inode->fd, node->name, hin_g.num_fds, hin_g.max_fds);
  return 0;
}

static int hin_vfs_fd_release (basic_vfs_node_t * node) {
  basic_vfs_t * vfs = node->vfs;
  basic_vfs_file_t * inode = node->inode;
  if (vfs->debug & BASIC_VFS_FDS)
    hin_debug ("fd-- vfs %d '%s' %d/%d\n", inode->fd, node->name, hin_g.num_fds, hin_g.max_fds);
  return 0;
}

int hin_vfs_clean () {
  if (inotify_buffer) {
    inotify_buffer->flags &= ~HIN_ACTIVE;
    hin_buffer_clean (inotify_buffer);
  }
  inotify_buffer = NULL;

  hin_threadsafe_dec (&hin_g.num_fds);

  if (hin_g.debug & HNDBG_FDS)
    hin_debug ("fd-- inotify %d %d/%d\n", vfs.inotify_fd, hin_g.num_fds, hin_g.max_fds);

  basic_vfs_clean (&vfs);

  return 0;
}

void hin_vfs_init () {
  basic_struct_lock (&hin_g);
  if (hin_g.debug & HNDBG_VFS)
    vfs.debug |= BASIC_VFS_DEBUG;
  if (hin_g.debug & HNDBG_FDS)
    vfs.debug |= BASIC_VFS_FDS;
  if (hin_g.debug & HNDBG_RW)
    vfs.debug |= BASIC_VFS_EXTRA;
  hin_threadsafe_inc (&hin_g.num_fds);
  basic_struct_unlock (&hin_g);

  basic_vfs_init (&vfs);
  vfs.fd_acquire = hin_vfs_fd_acquire;
  vfs.fd_release = hin_vfs_fd_release;

  if (hin_g.debug & HNDBG_FDS)
    hin_debug ("fd++ inotify %d %d/%d\n", vfs.inotify_fd, hin_g.num_fds, hin_g.max_fds);
  hin_vfs_event_init (vfs.inotify_fd);
}

