
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/signalfd.h>

#include <basic_hashtable.h>

#include "hin.h"
#include "hmaster.h"
#include "conf.h"

#include "hin_app.h"

#define RESTART_SIGNAL SIGUSR1

static void hin_sig_restart_handler (int signo, siginfo_t * info, void * ucontext) {
  printf ("do restart\n");
  hin_app_restart ();
}

static void hin_sig_pipe_handler (int signo, siginfo_t * info, void * ucontext) {
  printf ("broken pipe signal received\n");
}

static void hin_sig_int_handler (int signo, siginfo_t * info, void * ucontext) {
  printf("^C pressed. Shutting down.\n");
  hin_app_stop ();
  exit (0);
}

static void hin_sig_hup_handler (int signo, siginfo_t * info, void * ucontext) {
  printf ("do graceful exit\n");
  hin_app_stop ();
}

#if (HIN_USE_SIGNAL_FD == 0)

int hin_signal_clean () {
  signal (SIGHUP, SIG_DFL);
  signal (SIGINT, SIG_DFL);
  signal (RESTART_SIGNAL, SIG_DFL);
  signal (SIGPIPE, SIG_DFL);

  return 0;
}

int hin_signal_init () {
  sigset_t mask;
  sigemptyset (&mask);
  sigaddset (&mask, RESTART_SIGNAL);

  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) < 0)
    hin_perror ("sigprocmask");

  // It's better to use sigaction() over signal().  You won't run into the
  // issue where BSD signal() acts one way and Linux or SysV acts another.
  struct sigaction sa;
  memset (&sa, 0, sizeof sa);
  sigemptyset (&sa.sa_mask);
  sa.sa_flags = SA_SIGINFO;

  sa.sa_sigaction = hin_sig_hup_handler;
  sigaction (SIGHUP, &sa, NULL);

  sa.sa_sigaction = hin_sig_int_handler;
  sigaction (SIGINT, &sa, NULL);

  sa.sa_sigaction = hin_sig_pipe_handler;
  sigaction (SIGPIPE, &sa, NULL);

  sa.sa_sigaction = hin_sig_restart_handler;
  sigaction (RESTART_SIGNAL, &sa, NULL);

  return 0;
}

#else

static sigset_t mask;
static hin_buffer_t * signal_buffer = NULL;

int hin_signal_clean () {
  if (sigprocmask (SIG_UNBLOCK, &mask, NULL) == -1)
    hin_perror ("sigprocmask");

  if (signal_buffer == NULL) return 0;

  hin_threadsafe_dec (&hin_g.num_fds);
  if (hin_g.debug & HNDBG_FDS)
    hin_debug ("fd-- signal %d %d/%d\n", signal_buffer->fd, hin_g.num_fds, hin_g.max_fds);
  signal_buffer->flags &= ~HIN_ACTIVE;
  close (signal_buffer->fd);
  hin_buffer_stop_clean (signal_buffer);
  signal_buffer = NULL;

  return 0;
}

static int hin_signal_callback1 (hin_buffer_t * buf, int ret) {
  siginfo_t * info = (void*)buf->ptr;

  if (ret < 0) {
    hin_error ("signal read '%s'", strerror (-ret));
    if (hin_request_read (buf) < 0) {
      hin_weird_error (46547867);
      return -1;
    }
    return 0;
  }

  switch (info->si_signo) {
  case SIGHUP:
    hin_sig_hup_handler (info->si_signo, info, NULL);
  break;
  case SIGINT:
    hin_sig_int_handler (info->si_signo, info, NULL);
  break;
  case RESTART_SIGNAL:
    hin_sig_restart_handler (info->si_signo, info, NULL);
  break;
  case SIGPIPE:
    hin_sig_pipe_handler (info->si_signo, info, NULL);
  break;
  default:
    hin_error ("got unexpected signal");
    return -1;
  break;
  }

  if (hin_request_read (buf) < 0) {
    hin_weird_error (46547868);
    return -1;
  }

  return 0;
}

int hin_signal_init () {
  sigemptyset (&mask);
  sigaddset (&mask, SIGHUP);
  sigaddset (&mask, SIGINT);
  sigaddset (&mask, RESTART_SIGNAL);
  sigaddset (&mask, SIGPIPE);

  if (sigprocmask (SIG_BLOCK, &mask, NULL) < 0)
    hin_perror ("sigprocmask");

  int sig_fd = signalfd (-1, &mask, 0);
  if (sig_fd < 0)
    hin_perror ("signalfd");

  hin_buffer_t * buf = hin_buffer_alloc (sizeof (siginfo_t));
  #ifdef HIN_LINUX_URING_DONT_HAVE_SIGNALFD
  buf->flags |= HIN_EPOLL;
  #endif
  buf->fd = sig_fd;
  buf->callback = hin_signal_callback1;
  buf->debug = hin_g.debug;
  signal_buffer = buf;

  hin_threadsafe_inc (&hin_g.num_fds);
  if (hin_g.debug & HNDBG_FDS)
    hin_debug ("fd++ signal %d %d/%d\n", sig_fd, hin_g.num_fds, hin_g.max_fds);

  if (hin_request_read (buf) < 0) {
    hin_weird_error (46547869);
    return -1;
  }

  return 0;
}

#endif


