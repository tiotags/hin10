
#include <stdio.h>
#include <stdlib.h>

#ifdef HIN_USE_DAEMON

#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#include "hin.h"
#include "hmaster.h"

int hin_drop_user (const char * username, const char * groupname) {
  int ret = -1, err = 0;
  int userid = 0, groupid = 0;
  const char * msg = NULL;
  struct passwd *pwd = halloc_z (sizeof (struct passwd));
  struct group *grp = halloc_z (sizeof (struct group));
  size_t pwd_len = sysconf (_SC_GETPW_R_SIZE_MAX);
  char *pwd_buf = halloc_z (pwd_len);
  size_t grp_len = sysconf (_SC_GETGR_R_SIZE_MAX);
  char *grp_buf = halloc_z (grp_len);

  if (pwd == NULL || grp == NULL || pwd_buf == NULL || grp_buf == NULL) {
    msg = "no mem";
    goto finish;
  }

  err = getpwnam_r (username, pwd, pwd_buf, pwd_len, &pwd);
  if (pwd == NULL || err) {
    if (err)
      hin_perror ("getpwnam_r");
    msg = "username not found";
    goto finish;
  }
  //printf("uid: %d\n", pwd->pw_uid);
  //printf("gid: %d\n", pwd->pw_gid);
  err = getgrnam_r (groupname, grp, grp_buf, grp_len, &grp);
  if (grp == NULL || err) {
    if (err)
      hin_perror ("getgrnam_r");
    msg = "groupname not found";
    goto finish;
  }
  //printf("gid: %d\n", grp->gr_gid);
  userid = pwd->pw_uid;
  groupid = grp->gr_gid;

  if (chdir ("/") != 0) {
    hin_perror ("drop user chdir");
  }

  if (initgroups (username, groupid) == -1) {
    msg = "not root, can't initgroups";
    goto finish;
  }

  if (setresgid (groupid, groupid, groupid) != 0) {
    msg = "not root, can't set group";
    goto finish;
  }

  if (setresuid (userid, userid, userid) != 0) {
    msg = "not root, can't set user";
    goto finish;
  }

  if (setuid (0) == 0 || setgid (0) == 0) {
    msg = "smoke test failed, didn't drop priv";
    goto finish;
  }

  ret = 0;
  if (hin_g.debug & HNDBG_CONFIG) {
    hin_debug ("drop user switched user %s(%d):%s(%d)\n", username, userid, groupname, groupid);
  }

finish:
  if (ret != 0) {
    hin_error ("drop user failed to switch user %s(%d):%s(%d): %s", username, userid, groupname, groupid, msg);
    if (geteuid () != 0)
      hin_debug ("are you root ?\n");
  }
  if (pwd) hfree (pwd);
  if (grp) hfree (grp);
  if (pwd_buf) hfree (pwd_buf);
  if (grp_buf) hfree (grp_buf);
  return ret;
}

#else

#include "hin.h"
int hin_drop_user (const char * username, const char * groupname) {
  hin_error ("no drop user support compiled");
  return -1;
}

#endif

