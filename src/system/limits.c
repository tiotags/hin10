
#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <string.h>

#include <sched.h>
#include <sys/resource.h>
#include <sys/sysinfo.h>

#include "hin.h"
#include "http/http.h"
#include "hmaster.h"
#include "conf.h"

int hin_num_cores () {
  cpu_set_t cs;
  CPU_ZERO(&cs);
  sched_getaffinity(0, sizeof(cs), &cs);
  return CPU_COUNT_S(sizeof(cs), &cs);
}

int hin_linux_set_limits () {
  struct rlimit new;
  rlim_t max;

  memset (&new, 0, sizeof (new));
  if (getrlimit (RLIMIT_MEMLOCK, &new) < 0) {
    hin_perror ("getrlimit");
  }

  max = new.rlim_max;
  max = max > HIN_RLIMIT_MEMLOCK ? HIN_RLIMIT_MEMLOCK : max;
  if (hin_g.debug & HNDBG_INFO)
    hin_debug ("rlimit MEMLOCK %lld/%lld/%lld\n", (long long)new.rlim_cur, (long long)max, (long long)new.rlim_max);

  new.rlim_cur = max;
  if (setrlimit (RLIMIT_MEMLOCK, &new) < 0) {
    hin_perror ("setrlimit");
  }

  if (new.rlim_cur < HIN_RLIMIT_MEMLOCK && (hin_g.debug & HNDBG_CONFIG)) {
    hin_debug ("WARNING! low RLIMIT_MEMLOCK, possible crashes, message possibly outdated\n");
    hin_debug (" current: %lld\n", (long long)new.rlim_cur);
    hin_debug (" suggested: %lld\n", (long long)HIN_RLIMIT_MEMLOCK);
  }

  memset (&new, 0, sizeof (new));
  if (getrlimit (RLIMIT_NOFILE, &new) < 0) {
    hin_perror ("getrlimit");
  }

  max = new.rlim_max;
  if (HIN_RLIMIT_NOFILE > max) {
    hin_debug ("rlimit requested nofile higher than max %lld < %lld\n", (long long)max, (long long)HIN_RLIMIT_NOFILE);
  } else {
    max = HIN_RLIMIT_NOFILE;
  }
  if (hin_g.debug & HNDBG_INFO)
    hin_debug ("rlimit NOFILE %lld/%lld/%lld\n", (long long)new.rlim_cur, (long long)max, (long long)new.rlim_max);

  new.rlim_cur = max;
  if (setrlimit (RLIMIT_NOFILE, &new) < 0) {
    hin_perror ("setrlimit");
  }
  hin_g.max_fds = new.rlim_cur;

  extern httpd_annoy_t default_annoy;
  httpd_annoy_t * annoy = &default_annoy;

  struct sysinfo info;
  if (annoy->max_mem_usage == 0
  && annoy->max_mem_usage_ratio
  && sysinfo (&info) == 0) {
    annoy->max_mem_usage = info.totalram * annoy->max_mem_usage_ratio;
    if (hin_g.debug & HNDBG_INFO)
      hin_debug ("total ram is %ld max_mem_usage %ld\n", info.totalram, annoy->max_mem_usage);
  }

  return 0;
}
