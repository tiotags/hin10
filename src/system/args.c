
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <basic_args.h>

#include "hin.h"
#include "hextra.h"
#include "hmaster.h"
#include "conf.h"

#include "hin_app.h"
#include "hin_app_extra.h"

typedef struct {
  const char * nshort;
  const char * nlong;
  const char * help;

  int cmd;
  int min_param;
  int max_param;
  char ** param_name;
} hin_arg_t;

enum {
CVER=1, CHELP, CVERBOSE, CQUIET,
CEPOLL, CCORES, CDLOG,
CCONFIG, CLISTEN, CLISTENS, CVHOST,
CACCESS_LOG, CCERT, CHTDOCS, CMIMES,
CTRAILING, CDAEMONIZE, CPIDFILE, CCHECK,
CEMPTYCERT, CCMDLINE_DISABLE,
CRESTART, CRESTARTFD, CUSER, CRPROXY,
CDEBUG, CCOLOR, CMAP_PATH, CKEEPALIVE,
};

#define ADV 0x1000

static char * number_param[] = (char *[]){"number"};
static char * boolean_param[] = (char *[]){"on/off"};
static char * path_param[] = (char *[]){"path"};
static char * url_param[] = (char *[]){"url"};
static char * port_param[] = (char *[]){"port number", "bind address"};
static char * list_param[] = (char *[]){"list (eg: a1,a2,a3)"};

static hin_arg_t cmdlist[] = {
// server root
{"-v", "--version", "display version information and exit", CVER, 0, 0, NULL},
{"-h", "--help", "display this help information and exit, if given a parameter prints extra information", CHELP, 0, 1, (char *[]){"print advanced commands"}},
{"-c", "--config", "load a config file", CCONFIG, 1, 1, path_param},
{NULL, "--check", "don't execute anything just validate the config and exit", CCHECK|ADV, 0, 0, NULL},

// sockets
{NULL, "--cert", "create a certificate needed for ssl sockets and vhosts, should be set before --ports and --vhost", CCERT, 2, 2, (char *[]){"certificate", "private key"}},
{"-p", "--port", "httpd listen to unencrypted http port", CLISTEN, 1, 2, port_param},
{"-s", "--ports", "httpd listen to ssl encrypted https port", CLISTENS, 1, 2, port_param},
{NULL, "--listen", "same as --port", CLISTEN|ADV, 1, 2, port_param},

// vhosts and pathing
{"-n", "--vhost", "create a vhost for the httpd server", CVHOST, 1, 2, (char *[]){"host name", "htdocs"}},
{NULL, "--htdocs", "set a vhosts document root/htdocs", CHTDOCS, 1, 1, path_param},
{NULL, "--rproxy", "set up a reverse proxy", CRPROXY, 1, 1, url_param},
{NULL, "--path", "used to target a specific path inside a vhost", CMAP_PATH, 1, 1, url_param},

// global config
{"-a", "--access_log", "set the httpd access log path, if not set then the server doesn't log requests", CACCESS_LOG, 1, 1, path_param},
{NULL, "--mimes", "load a mime type database from this path, eg /etc/mime.types", CMIMES, 1, 1, path_param},
{NULL, "--num_cores", NULL, CCORES|ADV, 1, 1, number_param},
{NULL, "--threads", "set number of threads used, the default value is 0 and it means detect number of cores and use that value", CCORES, 1, 1, number_param},

// misc config
{NULL, "--trailing", "add a trailing slash to directories (default on)", CTRAILING|ADV, 1, 1, boolean_param},
{NULL, "--allow_empty_cert", "don't abort if you can't load a certificate, just don't use ssl (default off)", CEMPTYCERT|ADV, 1, 1, boolean_param},
{NULL, "--color", "should we use ANSI escape sequences to color error messages", CCOLOR|ADV, 1, 1, boolean_param},
{NULL, "--keepalive", "should the reverse proxy use keepalive connections", CKEEPALIVE|ADV, 1, 1, boolean_param},

// daemon operations
{NULL, "--force_epoll", "don't use io_uring at all and just try to use epoll and synchronous operations", CEPOLL|ADV, 0, 0, NULL},
{NULL, "--daemonize", "daemonize the server", CDAEMONIZE|ADV, 0, 0, NULL},
{NULL, "--no_cmdline", "don't listen to stdin for commands", CCMDLINE_DISABLE|ADV, 0, 0, NULL},
{NULL, "--pidfile", "write the process PID to a file", CPIDFILE|ADV, 1, 1, path_param},
{NULL, "--restart", "graceful restart", CRESTART, 0, 0, NULL},
{NULL, "--restartfd", "graceful restart fd passing", CRESTARTFD|ADV, 1, 1, NULL},
{NULL, "--user", "drop root priviledges to specified user:group", CUSER, 1, 2, (char *[]){"user name", "group name"}},

//logging
{NULL, "--log", "write the stdout and stderr to a log file", CDLOG, 1, 1, path_param},
{"-d", "--debug", "enable debug information, eg: -d pipe,http,-uring", CDEBUG | ADV, 1, 1, list_param},
{"-V", "--verbose", "verbose output, print all debug information", CVERBOSE, 0, 0, NULL},
{"-q", "--quiet", "print only errors", CQUIET, 0, 0, NULL},
{NULL, NULL, NULL, 0, 0, 0, NULL},
};

static int hin_convert_boolean (const char * str) {
  if (strcmp (str, "on") == 0) return 1;
  if (strcmp (str, "off") == 0) return 0;
  return atoi (str);
}

static const char * hin_name_boolean (int flag) {
  if (flag) return "on";
  return "off";
}

static void print_help (const char * group) {
  printf ("Usage: hinsightd [OPTION]...\n");
  for (int i=0; (size_t)i < sizeof (cmdlist) / sizeof (cmdlist[0]) - 1; i++) {
    hin_arg_t * cmd = &cmdlist[i];
    if (group == NULL && (cmd->cmd & ADV)) continue;
    if (cmd->nshort) {
      printf (" %s,", cmd->nshort);
    } else {
      printf ("    ");
    }
    printf (" %s", cmd->nlong);
    for (int j=1; j <= cmd->min_param; j++) {
      if (cmd->param_name) {
        printf (" <%s*>", cmd->param_name[j-1]);
      } else {
        printf (" <param%d*>", j);
      }
    }
    for (int j=cmd->min_param+1; j <= cmd->max_param; j++) {
      if (cmd->param_name) {
        printf (" <%s (opt)>", cmd->param_name[j-1]);
      } else {
        printf (" <optional %d>", j);
      }
    }
    if (cmd->help) {
      printf ("\n\t%s", cmd->help);
    }
    printf ("\n");
  }
}

static hin_ssl_ctx_t * ssl_cert = NULL;
static httpd_vhost_t * vhost = NULL;
static httpd_vhost_map_t * map = NULL;

int hin_process_argv (basic_args_t * args, const char * name) {
  hin_arg_t * cmd = NULL;
  int index = 0;

  for (int i=0; (size_t)i < sizeof (cmdlist) / sizeof (cmdlist[0]); i++) {
    cmd = &cmdlist[i];
    if ((cmd->nshort && strcmp (name, cmd->nshort) == 0) ||
        (cmd->nlong && strcmp (name, cmd->nlong) == 0)) {
      index = i;
      break;
    }
  }

  int nparam = 0;
  const char * params[10] = {0};
  int max = cmd->max_param;
  if (max >= 10) { hin_weird_error (43345); }
  for (nparam=0; nparam < max; nparam++) {
    const char * param = basic_args_get (args);
    if (param == NULL) {
      if (nparam >= cmd->min_param) break;
      hin_error ("missing parameter for %s (arg no. %d) provided: %d params required: %d", cmd->nlong, index, nparam, cmd->min_param);
      print_help (NULL);
      return -1;
    }
    params[nparam] = param;
  }

  httpd_vhost_scope_t * scope = NULL;
  if (map) {
    scope = &map->scope;
  } else if (vhost) {
    scope = &vhost->scope;
  }

  int ssl = 0;
  switch (cmd->cmd & (~ADV)) {
  case CVER:
    printf ("%s/%s\nfeatures", HIN_SERVER_NAME, HIN_SERVER_VERSION);
    #ifdef HIN_USE_THREAD
    printf (" threaded");
    #endif
    #ifdef HIN_USE_OPENSSL
    printf (" openssl");
    #endif
    #ifdef HIN_USE_DAEMON
    printf (" daemon");
    #endif
    #ifdef HIN_USE_SYSTEMD
    printf (" systemd");
    #endif
    #ifdef HIN_USE_RPROXY
    printf (" rproxy");
    #endif
    #ifdef HIN_USE_FCGI
    printf (" fcgi");
    #endif
    #ifdef HIN_USE_CGI
    printf (" cgi");
    #endif
    printf ("\n");
    return 1;
  break;
  case CHELP:
    print_help (params[0]);
    return 1;
  break;

  case CCERT:
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("ssl cert '%s' '%s'\n", params[0], params[1]);
    ssl_cert = hin_ssl_cert_create (params[0], params[1], 0);
  break;

  case CLISTENS:
    ssl = 1;
    // fall-through
  case CLISTEN: {
    const char * bind, * port;
    if (nparam > 1) {
      bind = params[1];
      port = params[0];
    } else {
      bind = "all";
      port = params[0];
    }

    void * ctx = NULL;
    if (ssl) {
      if ((ssl_cert == NULL) && (hin_g.flags & HIN_SSL_ALLOW_EMPTY_CERT)) {
        if (hin_g.debug & HNDBG_CONFIG)
          hin_debug ("did not listen to %s:%s missing certificate\n", bind, port);
        return 0;
      } else if (ssl_cert == NULL) {
        hin_error ("requested ssl for %s:%s but didn't provide a certificate", bind, port);
        return -1;
      }
      hin_ssl_cert_ref (ssl_cert);
      ctx = ssl_cert->ctx;
    }

    if (strcmp ("all", bind) == 0) bind = NULL;
    httpd_server_t * sock = httpd_create (bind, port, NULL, ctx);
    if (sock == NULL) {
      hin_error ("can't create socket '%s':%s\n", bind, port);
      return -1;
    }
  break; }

  case CVHOST: {
    const char * name = params[0];
    const char * htdocs = NULL;
    if (nparam > 1) htdocs = params[1];

    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("vhost on '%s' '%s'\n", name, htdocs ? htdocs : "");
    vhost = httpd_vhost_create (name, NULL, NULL);
    if (htdocs && hin_vfs_vhost_set_cwd (vhost, htdocs) < 0) {
      hin_error ("can't find '%s'\n", htdocs);
      return -1;
    }
  break; }

  case CMAP_PATH: {
    const char * path = params[0];
    httpd_vhost_map_t * new = hin_vhost_map_path (vhost, HIN_VHOST_MAP_PREFILE, path);
    if (new == NULL) {
      hin_error ("can't add map");
      return -1;
    }
    map = new;
  break; }

  case CHTDOCS: {
    const char * htdocs = params[0];
    if (vhost == NULL) {
      hin_error ("trying to set htdocs but no vhost created");
      return -1;
    }
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("htdocs for '%s' set '%s'\n", vhost->hostname, htdocs);
    if (hin_vfs_vhost_set_cwd (vhost, htdocs) < 0) {
      hin_error ("can't find '%s'\n", htdocs);
      return -1;
    }
  break; }

  case CRPROXY: {
    #ifdef HIN_USE_RPROXY
    const char * url = params[0];
    if (vhost == NULL) {
      hin_error ("can't setup a reverse proxy without a vhost");
      return -1;
    }

    hin_config_create_rproxy (vhost, map, url);
    #else
    hin_error ("no rproxy support compiled");
    return -1;
    #endif
  break; }

  case CCONFIG: {
    if (hin_g.debug & HNDBG_CONFIG)
      printf ("load config file at '%s'\n", params[0]);
    if (basic_args_parse_file (params[0], hin_process_argv) < 0)
      return -1;
  break; }

  case CMIMES: {
    if (hin_mime_types_read (params[0]) < 0)
      return -1;
  break; }

  case CCORES: {
    int num = atoi (params[0]);
    if (num < 1) num = hin_num_cores ();
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set number of cores to %d\n", num);
    hin_g.num_thread = num;
  break; }

  case CTRAILING: {
    int flag = hin_convert_boolean (params[0]);
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set add trailing slash flag %s\n", hin_name_boolean (flag));
    uint32_t mask = HIN_SSL_ALLOW_EMPTY_CERT;
    hin_g.flags = (flag ? 0 : mask) | (hin_g.flags & (~mask));
  break; }

  case CEMPTYCERT: {
    int flag = hin_convert_boolean (params[0]);
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set allow ssl empty certificate %s\n", hin_name_boolean (flag));
    uint32_t mask = HIN_SSL_ALLOW_EMPTY_CERT;
    hin_g.flags = (flag ? mask : 0) | (hin_g.flags & (~mask));
  break; }

  case CCOLOR: {
    int flag = hin_convert_boolean (params[0]);
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set console color %s\n", hin_name_boolean (flag));
    uint32_t mask = HIN_ANSI_COLOR;
    hin_g.flags = (flag ? mask : 0) | (hin_g.flags & (~mask));
  break; }

  case CKEEPALIVE: {
    int flag = hin_convert_boolean (params[0]);
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set keepalive %s\n", hin_name_boolean (flag));
    uint32_t mask = HIN_HTTP_REUSE_CONNECTION;
    hin_g.flags = (flag ? mask : 0) | (hin_g.flags & (~mask));
  break; }

  case CACCESS_LOG: {
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("set access log path to '%s'\n", params[0]);
    hin_app.access_log_path = (char*)params[0];
  break; }

  case CDLOG: {
    if (hin_redirect_log (params[0]) < 0) {
      return -1;
    }
  break; }

  case CDAEMONIZE:
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("daemonizing ...\n");
    hin_app.flags |= HIN_DAEMONIZE | HIN_CMDLINE_DISABLE;
  break;
  case CPIDFILE: {
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("pid written to '%s'\n", params[0]);
    hin_app.pid_path = (char*)params[0];
  break; }

  case CCMDLINE_DISABLE:
    hin_app.flags |= HIN_CMDLINE_DISABLE;
  break;

  case CCHECK:
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("pretend run ...\n");
    hin_app.flags |= HIN_PRETEND;
  break;

  case CEPOLL:
    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("forcing epoll only ...\n");
    hin_g.flags |= HIN_FORCE_EPOLL;
    hin_g.num_thread = 1; // TODO is this a bug ?
  break;

  case CRESTART:
    hin_app_restart ();
  break;
  case CRESTARTFD:
    hin_app.restart_fd = atoi (params[0]);
    printf ("restart fd was %d\n", hin_app.restart_fd);
  break;

  case CUSER: {
    const char * username = params[0];
    const char * groupname = NULL;
    if (nparam > 1) groupname = params[1];
    if (groupname == NULL) groupname = username;

    hin_app.username = strdup (username);
    hin_app.groupname = strdup (groupname);
  break; }

  case CDEBUG: {
    string_t source, param;
    source.ptr = (char*)params[0];
    source.len = strlen (source.ptr);
    uint32_t plus_mask = 0, minus_mask = 0, minus = 0;

    while (1) {
      param.len = 0;
      minus = 0;

      if (match_string (&source, "+") > 0) minus = 0;
      if (match_string (&source, "-") > 0) minus = 1;

      int used = match_string (&source, "([%w_]+)", &param);
      uint32_t mask = hin_get_debug_flag (&param);
      if (used <= 0 || mask == 0) {
        hin_error ("debug flag unkown '%.*s' (list %s)", (int)param.len, param.ptr, params[0]);
        return -1;
      }

      if (minus) {
        minus_mask |= mask;
      } else {
        plus_mask |= mask;
      }

      if (hin_g.debug & HNDBG_CONFIG) hin_debug ("debug flag %s\t'%.*s' %x\n", minus ? "disable" : "enable", (int)param.len, param.ptr, mask);

      if (match_string (&source, "%s*,%s*") <= 0) break;
    }

    hin_g.debug |= plus_mask;
    hin_g.debug &= ~minus_mask;
  break; }

  case CVERBOSE:
    //httpd_vhost_set_debug (0xffffffff);
    hin_g.debug = 0xffffffff;
  break;
  case CQUIET:
    //httpd_vhost_set_debug (0x0);
    hin_g.debug = 0x0;
  break;
  default:
    hin_error ("unkown option '%s'", name);
    print_help (NULL);
    return -1;
  break;
  }
  return 0;
}


