
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

#include "hin.h"
#include "hextra.h"
#include "hlisten.h"
#include "hmaster.h"

#include "hin_internal.h"

HIN_EXPORT void hin_client_close (hin_client_t * client) {
  hin_server_t * server = (hin_server_t*)client->parent;

  hin_t.num_client--;

  hin_threadsafe_dec (&hin_g.num_client);
  hin_threadsafe_dec (&hin_g.num_fds);

  if (hin_g.debug & (HNDBG_FDS|HNDBG_SOCKET))
    hin_debug ("fd-- accept %d %d/%d\n", client->sockfd, hin_g.num_fds, hin_g.max_fds);

  hin_struct_lock (server);
  hin_threadsafe_dec (&server->num_client);
  basic_dlist_remove (&server->client_list, &client->list);
  int test = (server->client_list.next == NULL) && (server->accept_buf[hin_t.id] == NULL);
  hin_struct_unlock (server);

  if (client->flags & HIN_SSL)
    hin_client_ssl_cleanup (client);
  free (client);

  if (test) {
    hin_struct_lock (server);
    for (int i=0; i<server->num_thread; i++) {
      if (server->accept_buf[i]) {
        hin_struct_unlock (server);
        return;
      }
    }
    hin_struct_unlock (server);
    hin_server_close (server);
  }
}

static int hin_server_do_client_callback (hin_client_t * client) {
  hin_server_t * server = (hin_server_t*)client->parent;

  if (server->ssl_ctx) {
    hin_ssl_accept_init (client);
  }

  int ret = 0;
  if (server->accept_callback) {
    ret = server->accept_callback (client);
  }

  hin_threadsafe_inc (&server->num_client);

  hin_t.num_client++;

  hin_threadsafe_inc (&hin_g.num_client);
  hin_threadsafe_inc (&hin_g.num_fds);

  return ret;
}

static hin_client_t * hin_server_create_client (hin_server_t * server) {
  hin_client_t * new = halloc_z (sizeof (hin_client_t) + server->user_data_size);
  new->magic = HIN_CLIENT_MAGIC;
  new->parent = server;
  return new;
}

static void hin_server_accept_buffer_free (hin_buffer_t * buf) {
  hin_client_t * client = (hin_client_t*)buf->parent;
  hin_server_t * server = (hin_server_t*)client->parent;
  if (client->sockfd > 0) close (client->sockfd);
  free (client);		// free empty client
  buf->parent = NULL;
  server->accept_buf[hin_t.id] = NULL;
  hin_buffer_stop_clean (buf);
  //printf ("freeing server %d buffer for thread %d\n", server->c.sockfd, hin_t.id);
  hin_server_close (server);
}

static int hin_server_accept_callback (hin_buffer_t * buffer, int ret) {
  hin_client_t * client = (hin_client_t*)buffer->parent;
  hin_server_t * server = (hin_server_t*)client->parent;

  if (buffer->flags & HIN_INACTIVE) {
    // TODO free virtual client
    hin_server_accept_buffer_free (buffer);
    if (ret > 0) close (ret);
    return 0;
  }

  if (ret < 0) {
    switch (-ret) {
    case EAGAIN:
    #if EAGAIN != EWOULDBLOCK
    case EWOULDBLOCK:
    #endif
    case EINTR:
    case ECONNABORTED:
    case ENETDOWN:
    case EPROTO:
    case EHOSTDOWN:
    case ENONET:
    case EHOSTUNREACH:
    case ENETUNREACH:
      // retry errors
      if (hin_request_accept (buffer, server->accept_flags) < 0) {
        hin_weird_error (5364567);
        return -1;
      }
      return 0;
    break;
    case EMFILE:
    case ENFILE:
    case ENOBUFS:
    case ENOMEM:
    case EPERM:
      // slow down errors
      if (hin_g.debug & (HNDBG_INFO|HNDBG_CONFIG|HNDBG_RW_ERROR))
        hin_error ("accept %d out of resources: %s", server->c.sockfd, strerror (-ret));
      return 0;
    break;
    default:
      // other errors are fatal
      hin_error ("failed accept %d '%s'", server->c.sockfd, strerror (-ret));
      hin_server_accept_buffer_free (buffer);
      // TODO do you need to clean the rest too ?
      return 0;
    break;
    }
  }

  if (hin_g.debug & (HNDBG_FDS|HNDBG_SOCKET)) {
    char buf1[1080];
    hin_client_addr (buf1, sizeof buf1, &client->ai_addr, client->ai_addrlen, 0);
    hin_debug ("fd++ accept %d '%s' server %d time %lld %d/%d\n", ret, buf1, server->c.sockfd, (long long)time (NULL), hin_g.num_fds, hin_g.max_fds);
  }

  client->sockfd = ret;
  if (hin_server_do_client_callback (client) < 0) {
    if (hin_request_accept (buffer, server->accept_flags) < 0) {
      hin_weird_error (3252543);
      return -1;
    }
    // not really an error but it's unexpected atm
    hin_weird_error (432526654);
    return 0;
  }

  if (buffer->flags & HIN_INACTIVE) {
    hin_server_accept_buffer_free (buffer);
    return 0;
  }

  hin_struct_lock (server);
  basic_dlist_append (&server->client_list, &client->list);
  hin_struct_unlock (server);

  hin_client_t * new = hin_server_create_client (client->parent);
  buffer->parent = new;

  if (hin_overload_slowdown ()) {
    return 0;
  }
  if (hin_request_accept (buffer, server->accept_flags) < 0) {
    hin_weird_error (435332);
    return -1;
  }
  return 0;
}

int hin_server_reaccept () {
  if (hin_overload_slowdown ()) {
    return 0;
  }

  if (hin_g.flags & HIN_FLAG_QUIT) {
    httpd_flush_idle_connections ();
  }

  hin_struct_lock (&hin_g);
  basic_dlist_t * elem = hin_g.server_list.next;
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    hin_buffer_t * buf = server->accept_buf[hin_t.id];
    if (buf == NULL) continue;
    if (buf->flags & HIN_INACTIVE) {
      hin_server_accept_buffer_free (buf);
      continue;
    }
    if (buf->flags & HIN_ACTIVE) continue;
    if (hin_request_accept (buf, server->accept_flags) < 0) {
      hin_weird_error (436332);
      //return -1; // TODO probably shouldn't exit
    }
  }
  hin_struct_unlock (&hin_g);
  return 0;
}

HIN_EXPORT int hin_server_start_thread () {
  basic_dlist_t * elem = hin_g.server_list.next;
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    hin_server_start_accept (server);
  }
  return 0;
}

int hin_server_start_accept (hin_server_t * server) {
  if (server->c.sockfd < 0) {
    if (server->flags & HIN_SERVER_RETRY) { return 0; }
    hin_error ("didn't listen first");
    return -1;
  }

  hin_buffer_t * buffer = hin_buffer_alloc (0);
  buffer->fd = server->c.sockfd;
  buffer->parent = hin_server_create_client (server);
  buffer->callback = hin_server_accept_callback;
  buffer->debug = server->debug;
  buffer->count = hin_t.id;

  hin_struct_lock (server);
  if (server->num_thread < hin_g.num_thread) {
    server->accept_buf = halloc_re (server->accept_buf, hin_g.num_thread * sizeof (void*));
    memset (
      &server->accept_buf[server->num_thread],
      0,
      (hin_g.num_thread - server->num_thread) * sizeof (void*)
    );
    server->num_thread = hin_g.num_thread;
  }
  server->accept_buf[hin_t.id] = buffer;

  if ((server->c.flags & HIN_SERVER_INIT) == 0) {
    hin_struct_lock (&hin_g);
    basic_dlist_append (&hin_g.server_list, &server->c.list);
    hin_struct_unlock (&hin_g);

    server->c.flags |= HIN_SERVER_INIT;
  }
  hin_struct_unlock (server);

  if (hin_request_accept (buffer, server->accept_flags) < 0) {
    hin_weird_error (35776893);
    return -1;
  }

  return 0;
}

HIN_EXPORT int hin_server_close (hin_server_t * server) {
  int has_listen = 0;
  hin_struct_lock (server);
  for (int i=0; i<server->num_thread; i++) {
    hin_buffer_t * buf = server->accept_buf[i];
    if (buf == NULL) continue;
    has_listen++;
    hin_threadsafe_var (&buf->flags, buf->flags | HIN_INACTIVE);
  }
  hin_struct_unlock (server);

  if (server->num_client > 0 || has_listen) return 0;

  hin_threadsafe_dec (&hin_g.num_listen);
  hin_threadsafe_dec (&hin_g.num_fds);
  // is already inside a hin_g lock ?
  basic_dlist_remove (&hin_g.server_list, &server->c.list);

  if (hin_g.debug & HNDBG_FDS)
    hin_debug ("fd-- listen %d %d/%d\n", server->c.sockfd, hin_g.num_fds, hin_g.max_fds);

  close (server->c.sockfd);
  free (server->accept_buf);
  free (server);

  hin_check_alive ();

  return 0;
}



