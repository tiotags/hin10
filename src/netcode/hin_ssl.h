
#ifndef HIN_SSL_H
#define HIN_SSL_H

#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>

#include <basic_lists.h>

enum {
HIN_SSL_READ 		= 0x1, 	HIN_SSL_WRITE 		= 0x2,
HIN_SSL_HANDSHAKE 	= 0x4, 	HIN_SSL_CLOSE 		= 0x8,
HIN_SSL_REQ_READ 	= 0x10, HIN_SSL_REQ_WRITE 	= 0x20,
HIN_SSL_CLOSED 		= 0x40, HIN_SSL_ERROR 		= 0x80,
};

typedef struct hin_ssl_struct {
  SSL *ssl;
  BIO *rbio;	// SSL reads from, we write to
  BIO *wbio;	// SSL writes to, we read from

  int fd;
  struct hin_timer_struct * timer;
  uint32_t flags;
  uint32_t debug;
  basic_dlist_t read_list, write_list, close_list, bufs;
} hin_ssl_t;

#define hin_buffer_list_ssl_ptr(elem) (basic_dlist_ptr (elem, offsetof (hin_buffer_t, ssl_list)))

void hin_ssl_timeout (struct hin_ssl_struct * ssl, int msec);

#endif

