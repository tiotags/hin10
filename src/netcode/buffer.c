
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "hextra.h"
#include "hmaster.h"

#include "hin_internal.h"

HIN_EXPORT void hin_buffer_clean (hin_buffer_t * buffer) {
  if (buffer->debug & HNDBG_MEMORY) hin_debug ("cleanup  \tbuf %p\n", buffer);

  if (buffer->flags & HIN_ACTIVE) {
    hin_error ("buf %d clean but active", buffer->fd);
    hin_buffer_stop_clean (buffer);
    return ;
  }

  if (buffer->flags & HIN_SSL) {
    hin_ssl_unlink_buffer (buffer);
  }

  if (buffer->flags & HIN_DYN_BUFFER) {
    hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
    if (lines->base) free (lines->base);
    hin_threadsafe_var (&hin_g.mem_usage, hin_g.mem_usage - sizeof (hin_lines_t));
  }

  hin_threadsafe_var (&hin_g.mem_usage, hin_g.mem_usage - (sizeof (hin_buffer_t) + buffer->sz));

  free (buffer);
}

static int hin_buffer_cancel_orig_cb (hin_buffer_t * buf, int ret) {
  //printf ("stop buffer orig %d: %s\n", ret, strerror (-ret));
  return 1;
}

static int hin_buffer_cancel_stub_cb (hin_buffer_t * buf, int ret) {
  //printf ("stop buffer stub %d: %s\n", ret, strerror (-ret));
  return 1;
}

HIN_EXPORT void hin_buffer_stop_clean (hin_buffer_t * buf) {
  if ((buf->flags & HIN_SSL)) {
    hin_ssl_unlink_buffer (buf);
    buf->flags &= ~(HIN_ACTIVE|HIN_SSL);
  }
  if (buf->flags & HIN_ACTIVE) {
    buf->callback = hin_buffer_cancel_orig_cb;

    hin_buffer_t * new = hin_buffer_alloc (0);
    new->callback = hin_buffer_cancel_stub_cb;
    new->parent = buf;
    hin_request_cancel (new, 0);
    return ;
  }
  hin_buffer_clean (buf);
}

HIN_EXPORT hin_buffer_t * hin_buffer_alloc (int sz) {
  hin_buffer_t * buf = halloc_q (sizeof (hin_buffer_t) + sz);
  memset (buf, 0, sizeof (hin_buffer_t));
  buf->count = buf->sz = sz;
  buf->ptr = buf->buffer;
  hin_threadsafe_var (&hin_g.mem_usage, hin_g.mem_usage + (sizeof (hin_buffer_t) + sz));
  return buf;
}

HIN_EXPORT hin_buffer_t * hin_buffer_create_from_data (void * parent, const char * ptr, int sz) {
  hin_buffer_t * buf = hin_buffer_alloc (sz);
  buf->parent = parent;
  if (ptr)
    memcpy (buf->ptr, ptr, sz);
  return buf;
}

HIN_EXPORT int hin_buffer_continue_write (hin_buffer_t * buf, int ret) {
  if (ret <= 0) {

  } else if (ret != buf->count) {
    buf->ptr += ret;
    buf->count -= ret;
    if (buf->flags & HIN_OFFSETS)
      buf->pos += ret;

    if (hin_request_write (buf) < 0)
      hin_weird_error (3253534);

  } else if (buf->list.next) {
    hin_buffer_t * next = hin_buffer_list_ptr (buf->list.next);
    next->callback = buf->callback;
    next->parent = buf->parent;

    if (hin_request_write (next) < 0)
      hin_weird_error (3253534);

    hin_buffer_clean (buf);
  } else {
    return 0;
  }
  return ret;
}

HIN_EXPORT int hin_lines_prepare (hin_buffer_t * buffer, int num) {
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  int pos = lines->count;
  int min_sz = pos + num;
  if (min_sz > buffer->sz) {
    hin_threadsafe_var (&hin_g.mem_usage, hin_g.mem_usage + (min_sz - buffer->sz));
    buffer->sz = min_sz;
    lines->base = halloc_re (lines->base, buffer->sz);
  }
  buffer->ptr = lines->base + pos;
  buffer->count = num;
  return 0;
}

HIN_EXPORT int hin_lines_eat (hin_buffer_t * buffer, int num) {
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  int pos = lines->count;
  char * base = lines->base;

  if (num < 0) return -1;

  memmove (base, base + num, pos - num);
  pos -= num;

  buffer->ptr = base + pos;
  lines->count -= num;
  return 0;
}

static int hin_lines_read_callback (hin_buffer_t * buffer, int ret);

HIN_EXPORT int hin_lines_request (hin_buffer_t * buffer, int min) {
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  int left = buffer->sz - lines->count;
  int new;

  if (buffer->flags & HIN_ACTIVE) {
    return 0;
  }
  if (min > 0 && min > left) { new = min - left; }
  else if (left < (buffer->sz / 2)) { new = HIN_BUFSZ; }
  else { new = left; }

  if (left < 0) {
    hin_error ("lines request negative %d - %d = %d", buffer->sz, lines->count, left);
  }
  if (new < 0) {
    hin_error ("lines new is %d: min %d left %d", new, min, left);
  }
  hin_lines_prepare (buffer, new);
  buffer->callback = hin_lines_read_callback;
  if (buffer->fd >= 0) {
  if (hin_request_read (buffer) < 0) {
    hin_weird_error (3464354);
    return -1;
  }
  }
  return 0;
}

int hin_lines_default_eat (hin_buffer_t * buffer, int num) {
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  if (num > 0) {
    hin_lines_eat (buffer, num);
  } else if (num == 0) {
    hin_lines_request (buffer, 0);
  } else {
    if (lines->close_callback) {
      return lines->close_callback (buffer, num);
    } else {
      hin_weird_error (6778900);
    }
    return -1;
  }
  return 0;
}

static int hin_lines_read_callback (hin_buffer_t * buffer, int ret) {
  if (buffer->flags & HIN_INACTIVE) {
    return 1;
  }

  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  if (ret <= 0) {
    if (lines->close_callback)
      return lines->close_callback (buffer, ret);
    return -1;
  }

  buffer->count = 0;
  lines->count += ret;

  int num = lines->read_callback (buffer, ret);

  int ret1 = lines->eat_callback (buffer, num);
  return ret1;
}

HIN_EXPORT int hin_lines_reread (hin_buffer_t * buf) {
  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;

  buf->count = 0;

  int num = lines->read_callback (buf, lines->count);

  int ret = lines->eat_callback (buf, num);
  if (ret) hin_buffer_clean (buf);
  return ret;
}

HIN_EXPORT int hin_lines_write (hin_buffer_t * buf, char * data, int len) {
  hin_lines_prepare (buf, len);
  memcpy (buf->ptr, data, len);
  int ret = buf->callback (buf, len);
  if (ret) { hin_buffer_clean (buf); }
  return ret;
}

HIN_EXPORT hin_buffer_t * hin_lines_create_raw (int sz) {
  hin_buffer_t * buf = hin_buffer_alloc (sizeof (hin_lines_t));
  buf->flags |= HIN_DYN_BUFFER;
  buf->count = buf->sz = sz;
  buf->pos = 0;
  buf->ptr = halloc_q (sz);
  buf->callback = hin_lines_read_callback;

  hin_threadsafe_var (&hin_g.mem_usage, hin_g.mem_usage + sz);

  hin_lines_t * lines = (hin_lines_t*)&buf->buffer;
  memset (lines, 0, sizeof (hin_lines_t));
  lines->eat_callback = hin_lines_default_eat;
  lines->base = buf->ptr;
  return buf;
}

