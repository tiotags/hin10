
#ifndef HIN_INTERNAL_H
#define HIN_INTERNAL_H

#include "hin.h"
#include "hlisten.h"

int hin_server_start_accept (hin_server_t * server);

int hin_ssl_accept_init (hin_client_t * client);
int hin_ssl_request_write (hin_buffer_t * buffer);
int hin_ssl_request_read (hin_buffer_t * buffer);
void hin_client_ssl_cleanup (hin_client_t * client);

int hin_epoll_request_write (hin_buffer_t * buf);
int hin_epoll_request_read (hin_buffer_t * buf);
int hin_epoll_request_accept (hin_buffer_t * buf);
int hin_epoll_request_connect (hin_buffer_t * buf);

int hin_request_callback (hin_buffer_t * buf, int ret);
int hin_request_callback_run (hin_buffer_t * buf, int ret);

int hin_ssl_unlink_buffer (hin_buffer_t * buf);

int hin_server_reaccept ();
int hin_epoll_check ();
void hin_timer_process ();

int hin_event_init ();
int hin_event_clean ();
void hin_epoll_clean ();
void hin_timer_cleanup ();
void hin_ssl_cleanup ();

int hin_make_socket_non_blocking (int sockfd);

void httpd_flush_idle_connections ();

#endif

