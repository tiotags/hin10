
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <fcntl.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#include "hin.h"
#include "hlisten.h"
#include "hin_internal.h"

int hin_overload_cutoff () {
  /*if (hin_g.max_client && hin_g.num_client > hin_g.max_client) {
    //printf ("overloaded %d/%d\n", hin_g.num_client, hin_g.max_client);
    return 1;
  }*/
  if (hin_g.num_fds + HIN_HTTPD_FD_LIMIT_OVERLOAD > hin_g.max_fds) {
    if (hin_g.debug & (HNDBG_FDS|HNDBG_RW_ERROR))
      hin_debug ("fd overload cutoff %d/%d\n", hin_g.num_fds, hin_g.max_fds);
    return 1;
  }
  return 0;
}

int hin_overload_slowdown () {
  if (hin_request_is_overloaded ()) {
    return 1;
  }

  extern httpd_annoy_t default_annoy;
  httpd_annoy_t * annoy = &default_annoy;
  if (hin_g.mem_usage && hin_g.mem_usage > annoy->max_mem_usage) {
    if (hin_g.debug & (HNDBG_RW_ERROR)) {
      hin_debug ("over memory usage %ld/%ld\n", hin_g.mem_usage, annoy->max_mem_usage);
    }
    return 1;
  }

  if (hin_t.num_client > 100) {
    int expected = hin_g.num_client * 6 / 4 / hin_g.num_thread;
    if (hin_t.num_client > expected) {
      // TODO probably something wrong in rebalance operation
      //hin_debug ("unbalanced %d %d/%d/%d\n", hin_t.id, hin_t.num_client, expected, hin_g.num_client);
      return 1;
    }
  }
  if (hin_g.num_fds + HIN_HTTPD_FD_LIMIT_SLOWDOWN > hin_g.max_fds) {
    if (hin_g.debug & (HNDBG_FDS|HNDBG_RW_ERROR) && (hin_t.flags & HIN_FLAG_OVERLOAD_TIMER) == 0) {
      hin_debug ("fd overload %d/%d\n", hin_g.num_fds, hin_g.max_fds);
      hin_t.flags |= HIN_FLAG_OVERLOAD_TIMER;
    }
    return 1;
  }
  return 0;
}

HIN_EXPORT int hin_client_addr (char * str, int len, struct sockaddr * ai_addr, socklen_t ai_addrlen, uint32_t flags) {
  char hbuf[NI_MAXHOST], sbuf[NI_MAXSERV];
  int err = getnameinfo (ai_addr, ai_addrlen,
			hbuf, sizeof hbuf,
			sbuf, sizeof sbuf,
			NI_NUMERICHOST | NI_NUMERICSERV);
  if (err) {
    hin_error ("getnameinfo: %s", gai_strerror (err));
    return -1;
  }
  if (flags & HIN_ADDR_ONLY_IP) {
    snprintf (str, len, "%s", hbuf);
  } else {
    snprintf (str, len, "%s:%s", hbuf, sbuf);
  }
  return 0;
}

int hin_make_socket_non_blocking (int sockfd) {
  int flags, s;

  flags = fcntl (sockfd, F_GETFL, 0);
  if (flags == -1) {
    hin_perror ("fcntl");
    return -1;
  }

  flags |= O_NONBLOCK;
  s = fcntl (sockfd, F_SETFL, flags);
  if (s == -1) {
    hin_perror ("fcntl");
    return -1;
  }

  return 0;
}

typedef struct {
  uint32_t mask;
  const char * str;
} hin_flag_name_t;

hin_flag_name_t dbg_name[] = {
{HNDBG_BASIC,		"basic"},
{HNDBG_CONFIG,		"config"},
{HNDBG_RW_ERROR,	"rw_error"},
{HNDBG_400ERR,		"400err"},
{HNDBG_VFS,		"vfs"},
{HNDBG_SSL,		"ssl"},
{HNDBG_SYSCALL,		"syscall"},
{HNDBG_SOCKET,		"socket"},
{HNDBG_HTTP,		"http"},
{HNDBG_CGI,		"cgi"},
{HNDBG_PROXY,		"proxy"},
{HNDBG_HTTP_FILTER,	"http_filter"},
{HNDBG_POST,		"post"},
{HNDBG_CHILD,		"child"},
{HNDBG_CACHE,		"cache"},
{HNDBG_TIMEOUT,		"timeout"},
{HNDBG_RW,		"rw"},
{HNDBG_MEMORY,		"memory"},
{HNDBG_PIPE,		"pipe"},
{HNDBG_INFO,		"info"},
{HNDBG_PROGRESS,	"progress"},
{HNDBG_ANNOY,		"annoy"},
{HNDBG_FDS,		"fds"},
{HNDBG_MIME,		"mime"},
{HNDBG_URING,		"uring"},
{HNDBG_ALL,		"all"},
{0,			NULL},
};

HIN_EXPORT uint32_t hin_get_debug_flag (string_t * source) {
  char * str = strndup (source->ptr, source->len);
  uint32_t mask = 0;
  size_t n = sizeof(dbg_name)/sizeof(dbg_name[0]);
  for (size_t i=0; i<n; i++) {
    hin_flag_name_t * dbg = &dbg_name[i];
    if (strcmp (str, dbg->str)) continue;
    mask |= dbg->mask;
    break;
  }
  free (str);
  return mask;
}

#define writerr(str, ...) pos += snprintf (&buf[pos], sizeof (buf) - 1 - pos, str, ##__VA_ARGS__)
#define writerr1(str, ...) pos += vsnprintf (&buf[pos], sizeof (buf) - 1 - pos, str, ##__VA_ARGS__)

HIN_EXPORT void hin_error (const char * fmt, ...) {
  va_list ap;
  va_start (ap, fmt);

  char buf[4096];
  int pos = 0;

  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[1;31m");
  writerr ("error! ");
  writerr1 (fmt, ap);
  writerr ("\n");
  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[0m");

  buf[sizeof(buf)-1] = '\0';
  fprintf (stderr, "%s", buf);

  va_end (ap);
}

HIN_EXPORT void hin_perror (const char * fmt, ...) {
  va_list ap;
  va_start (ap, fmt);

  char buf[4096];
  int pos = 0;

  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[1;31m");
  writerr ("error! ");
  writerr1 (fmt, ap);
  writerr (": %s\n", strerror (errno));
  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[0m");

  buf[sizeof(buf)-1] = '\0';
  fprintf (stderr, "%s", buf);

  va_end (ap);
}

HIN_EXPORT void hin_weird_error (int errcode) {
  char buf[1024];
  int pos = 0;

  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[1;31m");
  writerr ("error! generic error # %d please consult source code\n", errcode);
  if (hin_g.flags & HIN_ANSI_COLOR)
    writerr ("\033[0m");

  buf[sizeof(buf)-1] = '\0';
  fprintf (stderr, "%s", buf);
}



