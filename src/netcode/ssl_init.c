
#include "hin.h"

#ifdef HIN_USE_OPENSSL

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <netinet/in.h>
#include <ctype.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <fcntl.h>

#include "hmaster.h"

#include "hin_internal.h"
#include "hin_ssl.h"

// Global SSL context
static HIN_LOCAL_VAR SSL_CTX * default_ctx = NULL;

static SSL_CTX * hin_ssl_default_init () {
  SSL_CTX * ctx = NULL;

  SSL_library_init ();
  OpenSSL_add_all_algorithms ();
  SSL_load_error_strings ();
  ERR_load_crypto_strings ();

  const SSL_METHOD *method = TLS_method ();

  ctx = SSL_CTX_new (method);

  SSL_CTX_set_session_cache_mode (ctx, SSL_SESS_CACHE_SERVER | SSL_SESS_CACHE_NO_INTERNAL | SSL_SESS_CACHE_NO_AUTO_CLEAR);

  if (ctx == NULL) {
    hin_error ("ssl context creation failed");
    return NULL;
  }

  SSL_CTX_set_ecdh_auto (ctx, 1);

  // Recommended to avoid SSLv2 & SSLv3
  SSL_CTX_set_options (ctx, SSL_OP_ALL|SSL_OP_NO_SSLv2|SSL_OP_NO_SSLv3|SSL_OP_CIPHER_SERVER_PREFERENCE);

  return ctx;
}

HIN_EXPORT int hin_ssl_accept_init (hin_client_t * client) {
  hin_ssl_t * ssl = client->ssl;
  if (ssl == NULL) {
    ssl = halloc_z (sizeof (hin_ssl_t));
    client->ssl = ssl;
  }
  hin_server_t * server = (hin_server_t*)client->parent;

  ssl->rbio = BIO_new (BIO_s_mem ());
  ssl->wbio = BIO_new (BIO_s_mem ());

  ssl->ssl = SSL_new (server->ssl_ctx);

  SSL_set_accept_state (ssl->ssl); // sets ssl to work in server mode.
  SSL_set_bio (ssl->ssl, ssl->rbio, ssl->wbio);
  SSL_set_ex_data (ssl->ssl, 0, client);

  client->flags |= HIN_SSL;
  if (hin_g.debug & HNDBG_SSL) hin_debug ("ssl init accept sockfd %d\n", client->sockfd);
  return 1;
}

HIN_EXPORT int hin_ssl_connect_init (hin_client_t * client) {
  hin_ssl_t * ssl = client->ssl;
  if (ssl == NULL) {
    ssl = halloc_z (sizeof (hin_ssl_t));
    client->ssl = ssl;
  }

  ssl->rbio = BIO_new (BIO_s_mem ());
  ssl->wbio = BIO_new (BIO_s_mem ());

  if (default_ctx == NULL) {
    default_ctx = hin_ssl_default_init ();
  }

  // TODO test if this is correct
  SSL_CTX_set_verify (default_ctx, SSL_VERIFY_PEER, NULL);

  // Use the default trusted certificate store
  if (!SSL_CTX_set_default_verify_paths (default_ctx)) {
    hin_error ("failed to set the default trusted certificate store");
    //goto end;
    return -1;
  }

  ssl->ssl = SSL_new (default_ctx);

  SSL_set_connect_state (ssl->ssl); // sets ssl to work in connect mode.
  SSL_set_bio (ssl->ssl, ssl->rbio, ssl->wbio);
  SSL_set_ex_data (ssl->ssl, 0, client);

  // TODO test this
  // Tell the server during the handshake which hostname we are attempting
  // to connect to in case the server supports multiple hosts.
  //if (!SSL_set_tlsext_host_name(ssl->ssl, hostname)) {
  //  printf("Failed to set the SNI hostname\n");
  //  goto end;
  //}

  // Ensure we check during certificate verification that the server has
  // supplied a certificate for the hostname that we were expecting.
  // Virtually all clients should do this unless you really know what you are doing.
  //if (!SSL_set1_host(ssl->ssl, hostname)) {
  //  printf("Failed to set the certificate verification hostname");
  //  goto end;
  //}

  client->flags |= HIN_SSL;
  if (hin_g.debug & HNDBG_SSL) hin_debug ("ssl init connect sockfd %d\n", client->sockfd);
  return 1;
}

HIN_EXPORT void hin_client_ssl_cleanup (hin_client_t * client) {
  hin_ssl_t * ssl = client->ssl;

  hin_ssl_timeout (ssl, -1);

  basic_dlist_t * elem = ssl->bufs.next;
  while (elem) {
    hin_buffer_t * buf = hin_buffer_list_ssl_ptr (elem);
    elem = elem->next;

    hin_buffer_stop_clean (buf);
  }

  SSL_free (ssl->ssl);   // free the SSL object and its BIO's
  free (ssl);
  client->ssl = NULL;
}

HIN_EXPORT void hin_ssl_cleanup () {
  hin_struct_lock (&hin_g);
  if (default_ctx) {
    SSL_CTX_free (default_ctx);
    default_ctx = NULL;
  }
  basic_dlist_t * elem = hin_g.cert_list.next;
  while (elem) {
    hin_ssl_ctx_t * cert = basic_dlist_ptr (elem, offsetof (hin_ssl_ctx_t, list));
    elem = elem->next;

    hin_ssl_cert_unref (cert);
  }
  hin_struct_unlock (&hin_g);
}

void hin_ssl_print_error () {
  int err1 = ERR_get_error ();
  hin_error ("ssl reason '%s'", ERR_reason_error_string (err1));
  hin_error ("ssl '%s'", ERR_error_string (err1, NULL));
}

static int hin_ssl_sni_callback (SSL *ssl, int *ad, void *arg) {
  if (ssl == NULL)
    return SSL_TLSEXT_ERR_NOACK;
  uint32_t debug = hin_g.debug;

  const char* servername = SSL_get_servername (ssl, TLSEXT_NAMETYPE_host_name);
  if (servername == NULL || servername[0] == '\0') {
    if (debug & (HNDBG_SSL|HNDBG_INFO))
      hin_debug ("ssl SNI null\n");
    return SSL_TLSEXT_ERR_NOACK;
  }

  if (debug & (HNDBG_SSL))
    hin_debug ("ssl SNI '%s'\n", servername);

  hin_client_t * client = SSL_get_ex_data (ssl, 0);
  hin_server_t * server = client->parent;

  if (server->sni_callback) {
    SSL_CTX * new = server->sni_callback (client, servername, strlen (servername));
    if (new) {
      SSL_CTX * r = SSL_set_SSL_CTX (ssl, new);
      if (r != new) {
        hin_error ("ssl can't set new ctx");
        return SSL_TLSEXT_ERR_ALERT_FATAL;
      }
    }
  }

  return SSL_TLSEXT_ERR_OK;
}

static SSL_CTX * hin_ssl_cert_create_ctx (const char * cert, const char * key) {
  SSL_CTX * ctx = hin_ssl_default_init ();

  // Load certificate and private key files, and check consistency
  int err;
  err = SSL_CTX_use_certificate_file (ctx, cert, SSL_FILETYPE_PEM);
  if (err != 1) {
    hin_error ("SSL_CTX_use_certificate_file '%s' failed", cert);
    hin_ssl_print_error ();
    return NULL;
  }
  if (hin_g.debug & HIN_SSL)
    hin_debug ("ssl cert '%s' ok.\n", cert);

  // Indicate the key file to be used
  err = SSL_CTX_use_PrivateKey_file (ctx, key, SSL_FILETYPE_PEM);
  if (err != 1) {
    hin_error ("SSL_CTX_use_PrivateKey_file '%s' failed", key);
    hin_ssl_print_error ();
    return NULL;
  }
  if (hin_g.debug & HIN_SSL)
    hin_debug ("ssl key '%s' ok.\n", key);

  // Make sure the key and certificate file match.
  if (SSL_CTX_check_private_key (ctx) != 1) {
    hin_error ("SSL_CTX_check_private_key failed");
    hin_ssl_print_error ();
    return NULL;
  }

  SSL_CTX_set_tlsext_servername_callback (ctx, hin_ssl_sni_callback);

  // TODO not doing what I want it
  SSL_CTX_set_verify (ctx, SSL_VERIFY_NONE, NULL);

  return ctx;
}

HIN_EXPORT struct hin_ssl_ctx_struct * hin_ssl_cert_create (const char * cert, const char * key, uint32_t flags) {
  SSL_CTX * ctx = hin_ssl_cert_create_ctx (cert, key);
  if (ctx == NULL) return NULL;

  hin_ssl_ctx_t * box = halloc_z (sizeof (*box));
  box->cert = strdup (cert);
  box->key = strdup (key);
  box->ctx = ctx;
  box->magic = HIN_CERT_MAGIC;
  box->refcount++;
  basic_dlist_append (&hin_g.cert_list, &box->list);
  return box;
}

HIN_EXPORT void hin_ssl_cert_ref (struct hin_ssl_ctx_struct * box) {
  box->refcount++;
}

HIN_EXPORT void hin_ssl_cert_unref (struct hin_ssl_ctx_struct * box) {
  box->refcount--;
  if (box->refcount > 0) { return ; }
  free ((void*)box->cert);
  free ((void*)box->key);
  // free ssl ctx
  SSL_CTX_free (box->ctx);
  basic_dlist_remove (&hin_g.cert_list, &box->list);
  free (box);
}

int hin_ssl_unlink_buffer (hin_buffer_t * buf) {
  // TODO unlink from the list
  // find proper buffer used and then remove parent pointer
  hin_ssl_t * ssl = buf->ssl;
  basic_dlist_t * head = NULL;
  if (buf->flags & HIN_READ) {
    head = &ssl->read_list;
  } else if (buf->flags & HIN_WRITE) {
    head = &ssl->write_list;
  } else if (buf->flags & HIN_CLOSE) {
    head = &ssl->close_list;
  } else {
    hin_weird_error (1984367);
  }

  basic_dlist_t * elem = head->next;
  while (elem) {
    hin_buffer_t * plain = hin_buffer_list_ssl_ptr (elem);
    elem = elem->next;

    if (plain != buf) continue;

    //printf ("ssl canceled buffer %p\n", buf);
    basic_dlist_remove (head, &buf->list);
    return 1;
  }
  //hin_error ("ssl couldn't find buf %p type %x", buf, buf->flags & HIN_IO);
  return 0;
}

int hin_ssl_timeout_cb (struct hin_timer_struct * timer) {
  hin_ssl_t * ssl = timer->ptr;
  if (ssl->debug & HNDBG_TIMEOUT)
    hin_debug ("ssl %d timedout\n", ssl->fd);
  ssl->timer = NULL;
  hin_ssl_do_close (ssl);
  return 0;
}

void hin_ssl_timeout (struct hin_ssl_struct * ssl, int msec) {
  if (ssl->debug & HNDBG_TIMEOUT)
    hin_debug ("ssl %d timeout %d\n", ssl->fd, msec);

  hin_timer_set (&ssl->timer, hin_ssl_timeout_cb, ssl, msec);
}

#else

HIN_EXPORT int hin_ssl_accept_init (hin_client_t * client) {
  hin_error ("ssl disabled");
  return -1;
}

HIN_EXPORT int hin_ssl_connect_init (hin_client_t * client) {
  hin_error ("ssl disabled");
  return -1;
}

HIN_EXPORT void hin_client_ssl_cleanup (hin_client_t * client) {}

HIN_EXPORT struct hin_ssl_ctx_struct * hin_ssl_cert_create (const char * cert, const char * key, uint32_t flags) {
  hin_error ("ssl disabled");
  return NULL;
}

HIN_EXPORT void hin_ssl_cleanup () {}

HIN_EXPORT void hin_ssl_cert_ref (struct hin_ssl_ctx_struct * box) {}
HIN_EXPORT void hin_ssl_cert_unref (struct hin_ssl_ctx_struct * box) {}

int hin_ssl_request_write (hin_buffer_t * buffer) { return -1; }
int hin_ssl_request_read (hin_buffer_t * buffer) { return -1; }
int hin_ssl_request_close (hin_buffer_t * buffer) { return -1; }

int hin_ssl_unlink_buffer (hin_buffer_t * buf) { return -1; }

#endif

