
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "hmaster.h"

#include "conf.h"

#ifdef HIN_USE_OPENSSL

#include "hin_internal.h"
#include "hin_ssl.h"

static int hin_ssl_do_close (hin_ssl_t * ssl);
static int hin_ssl_advance (hin_ssl_t * ssl);

static int hin_ssl_callback (hin_buffer_t * plain, int ret) {
  plain->flags &= ~HIN_ACTIVE;
  if (plain->callback (plain, ret)) {
    hin_buffer_clean (plain);
  }
  return 1;
}

static hin_buffer_t * hin_ssl_get_buf (hin_ssl_t * ssl, hin_buffer_t * orig) {
  hin_buffer_t * crypt = NULL;

  basic_dlist_t * elem = ssl->bufs.next;
  while (elem) {
    crypt = hin_buffer_list_ssl_ptr (elem);
    elem = elem->next;

    if (crypt->flags & HIN_ACTIVE) continue;

    crypt->ptr = crypt->buffer;
    crypt->count = crypt->sz;
    return crypt;
  }

  if (orig == NULL) {
    orig = hin_buffer_list_ssl_ptr (ssl->bufs.next);
  }

  int sz = HIN_BUFSZ + 100;
  crypt = hin_buffer_alloc (sz);
  crypt->flags = orig->flags & (HIN_SOCKET|HIN_FILE|HIN_OFFSETS|HIN_EPOLL|HIN_CORK);
  crypt->fd = orig->fd;
  crypt->ssl = ssl;
  crypt->debug = orig->debug;

  basic_dlist_append (&ssl->bufs, &crypt->ssl_list);
  ssl->debug = crypt->debug;
  ssl->fd = crypt->fd;

  if (crypt->debug & HNDBG_SSL)
    hin_debug ("ssl create new %p buffer\n", crypt);

  return crypt;
}

static int hin_ssl_handle_io (hin_ssl_t * ssl, int n) {
  int err = SSL_get_error (ssl->ssl, n);
  switch (err) {
  case SSL_ERROR_NONE:
    return 0;
  break;
  case SSL_ERROR_WANT_WRITE:
    if (ssl->debug & HNDBG_SSL)
      hin_debug ("ssl require write\n");
    ssl->flags |= HIN_SSL_REQ_WRITE;
    return 0;
  break;
  case SSL_ERROR_WANT_READ:
    if (ssl->debug & HNDBG_SSL)
      hin_debug ("ssl require read\n");
    ssl->flags |= HIN_SSL_REQ_READ;
    return 0;
  break;
  case SSL_ERROR_ZERO_RETURN: printf ("SSL_ERROR_ZERO_RETURN\n"); break;
  case SSL_ERROR_WANT_CONNECT: printf ("SSL_ERROR_WANT_CONNECT\n"); break;
  case SSL_ERROR_WANT_ACCEPT: printf ("SSL_ERROR_WANT_ACCEPT\n"); break;
  case SSL_ERROR_WANT_X509_LOOKUP: printf ("SSL_ERROR_WANT_X509_LOOKUP\n"); break;
  case SSL_ERROR_SYSCALL: printf ("SSL_ERROR_SYSCALL\n"); break;
  case SSL_ERROR_SSL: printf ("SSL_ERROR_SSL\n"); break;
  default: printf ("SSL_UNKNOWN_ERROR\n"); break;
  }

  ssl->flags |= HIN_SSL_ERROR;
  if (err == SSL_ERROR_SSL)
    hin_error ("ssl err: %s", ERR_error_string (ERR_get_error (), NULL));

  return 1;
}

static int hin_ssl_push_read_data (hin_ssl_t * ssl, hin_buffer_t * plain) {
  int m = SSL_read (ssl->ssl, plain->ptr, plain->count);
  if (m > 0) {
    if (plain->debug & HNDBG_SSL) hin_debug ("ssl read %d/%d\n", m, plain->count);
    basic_dlist_remove (&ssl->read_list, &plain->ssl_list);
    hin_ssl_callback (plain, m); // does clean inside the function
    return 1;
  }
  return 0;
}

static void hin_ssl_push_error (hin_ssl_t * ssl, hin_buffer_t * crypt, int ret) {
  ssl->flags |= HIN_SSL_ERROR;

  if (hin_ssl_advance (ssl)) {
  }

  basic_dlist_t * head, * elem;

  head = &ssl->read_list;
  elem = head->next;
  while (elem) {
    hin_buffer_t * plain = hin_buffer_list_ssl_ptr (elem);
    elem = elem->next;

    basic_dlist_remove (head, &plain->ssl_list);
    hin_ssl_callback (plain, ret);
  }

  head = &ssl->write_list;
  elem = head->next;
  while (elem) {
    hin_buffer_t * plain = hin_buffer_list_ssl_ptr (elem);
    elem = elem->next;

    basic_dlist_remove (head, &plain->ssl_list);
    hin_ssl_callback (plain, ret);
  }
}

static int hin_ssl_read_callback (hin_buffer_t * crypt, int ret) {
  hin_ssl_t * ssl = crypt->ssl;
  ssl->flags &= ~HIN_SSL_READ;
  hin_buffer_t * plain = crypt->parent;

  if (ret <= 0) {
    if (ret != 0) {
      hin_error ("ssl read cb %p: %s", crypt, strerror (-ret));
    }
    hin_ssl_push_error (ssl, crypt, ret);
    return 0;
  }

  int n = BIO_write (ssl->rbio, crypt->ptr, ret);
  if (crypt->debug & HNDBG_SSL) hin_debug ("ssl done read %d/%d/%d\n", n, ret, crypt->count);

  if (n < 0) {
    hin_error ("ssl bio read fatal fail %d<%d", n, ret);
    hin_ssl_push_error (ssl, crypt, ret);
    return 0;
  }

  // callback
  if (plain == NULL) plain = hin_buffer_list_ssl_ptr (ssl->read_list.next);
  if (plain) {
    if (hin_ssl_push_read_data (ssl, plain)) {
      basic_dlist_remove (&ssl->read_list, &plain->ssl_list);
    }
  }

  if (hin_ssl_advance (ssl)) {
  }

  return 0;
}

static int hin_ssl_write_callback (hin_buffer_t * crypt, int ret) {
  hin_ssl_t * ssl = crypt->ssl;
  ssl->flags &= ~HIN_SSL_WRITE;

  hin_buffer_t * plain = (hin_buffer_t*)crypt->parent;

  if (ret < crypt->count) {
    if (ret < 0) {
      hin_error ("ssl write cb %p: '%s'", crypt, strerror (-ret));
      hin_ssl_push_error (ssl, crypt, ret);
      return 0;
    }

    if (crypt->debug & HNDBG_SSL) hin_debug ("ssl incomplete write done callback %d/%d bytes\n", ret, crypt->count);
    crypt->ptr += ret;
    crypt->count -= ret;

    if (hin_request_write (crypt) < 0) {
      hin_weird_error (35769678);
    }

    return 0;
  }

  if (hin_ssl_advance (ssl)) {
  }

  // callback
  if (plain) {
    basic_dlist_remove (&ssl->write_list, &plain->ssl_list);
    hin_ssl_callback (plain, plain->count);
  }

  return 0;
}

static int hin_ssl_do_read (hin_ssl_t * ssl) {
  int ret = 0;
  // read ahead
  hin_buffer_t * plain;
  while (1) {
    plain = hin_buffer_list_ssl_ptr (ssl->read_list.next);
    if (plain == NULL) break;

    if (hin_ssl_push_read_data (ssl, plain) <= 0) {
      ssl->flags |= HIN_SSL_REQ_READ;
      break;
    }
    ret = 1;
  }

  if (plain == NULL && (ssl->flags & HIN_SSL_REQ_READ) == 0) goto finish;
  ssl->flags &= ~HIN_SSL_REQ_READ;

  if (ssl->flags & (HIN_SSL_READ)) goto finish;
  ssl->flags |= HIN_SSL_READ;

  hin_buffer_t * crypt = hin_ssl_get_buf (ssl, NULL);
  crypt->flags = (crypt->flags & ~HIN_IO) | HIN_READ;
  crypt->callback = hin_ssl_read_callback;
  crypt->parent = plain;

  if (crypt->debug & HNDBG_SSL)
    printf ("ssl read crypt buffer %p count %d\n", crypt, crypt->count);

  if (hin_request_read (crypt) < 0) {
    hin_weird_error (435769678);
    return -1;
  }

finish:
  return ret;
}

static int hin_ssl_do_write (hin_ssl_t * ssl) {
  if (ssl->flags & (HIN_SSL_WRITE|HIN_SSL_ERROR)) return 1;
  ssl->flags |= HIN_SSL_WRITE;

  hin_buffer_t * plain = hin_buffer_list_ssl_ptr (ssl->write_list.next);
  if (plain && (plain->flags & HIN_DONE) == 0 && (ssl->flags & HIN_SSL_CLOSE) == 0) {
    plain->flags |= HIN_DONE;

    int n = SSL_write (ssl->ssl, plain->ptr, plain->count);
    if (n <= 0) {
      printf ("can't write ? %d\n", n);
      hin_ssl_handle_io (ssl, n);
    } else {
      if (plain->debug & HNDBG_SSL)
        hin_debug ("ssl normal write %p %d\n", plain, n);
    }
  }

  if (BIO_pending (ssl->wbio) <= 0) {
    if (ssl->debug & HNDBG_SSL)
      hin_debug ("ssl nothing to write\n");
    ssl->flags &= ~HIN_SSL_WRITE;
    return 0;
  }

  hin_buffer_t * crypt = hin_ssl_get_buf (ssl, NULL);
  crypt->flags = (crypt->flags & ~HIN_IO) | HIN_WRITE;
  crypt->callback = hin_ssl_write_callback;
  crypt->parent = plain;

  int n = 0;
  n = BIO_read (ssl->wbio, crypt->ptr, crypt->sz);
  if (n < 0) {
    ssl->flags |= HIN_SSL_ERROR;
    hin_error ("ssl bio write fatal fail %d", n);
    return -1; // if BIO write fails, assume unrecoverable
  }
  crypt->count = n;

  if (crypt->debug & HNDBG_SSL)
    hin_debug ("ssl write crypt buffer %p %d bytes\n", crypt, n);

  if (hin_request_write (crypt) < 0) {
    hin_weird_error (43434436);
    return -1;
  }

  return 1;
}

static int hin_ssl_do_close (hin_ssl_t * ssl) {
  if (ssl->flags & HIN_SSL_CLOSED) return 0;
  ssl->flags |= HIN_SSL_CLOSED;

  hin_buffer_t * plain;
  while (1) {
    plain = hin_buffer_list_ssl_ptr (ssl->close_list.next);
    if (plain == NULL) break;

    if (plain->debug & HNDBG_SSL)
      hin_debug ("ssl close %p\n", plain);

    plain->flags &= ~(HIN_SSL|HIN_ACTIVE);
    basic_dlist_remove (&ssl->close_list, &plain->ssl_list);

    if (hin_request_close (plain) < 0) {
      hin_weird_error (435769678);
      return -1;
    }
  }

  return 0;
}

static int hin_ssl_advance (hin_ssl_t * ssl) {
restart:
  if (ssl->flags & (HIN_SSL_ERROR|HIN_SSL_CLOSED)) {
    hin_ssl_do_close (ssl);
  } else if ((ssl->flags & HIN_SSL_HANDSHAKE) == 0) {
    int n = SSL_do_handshake (ssl->ssl);
    if (n > 0) {
      if (ssl->debug & HNDBG_SSL)
        hin_debug ("ssl handshake complete\n");
      ssl->flags |= HIN_SSL_HANDSHAKE;
    } else {
      hin_ssl_handle_io (ssl, n);
    }
  } else if ((ssl->flags & HIN_SSL_CLOSE) && (ssl->flags & HIN_SSL_CLOSED) == 0) {
    int n = SSL_shutdown (ssl->ssl);
    int do_write = hin_ssl_do_write (ssl);
    if (do_write <= 0 && n > 0) {
      if (ssl->debug & HNDBG_SSL)
        hin_debug ("ssl close complete\n");
      hin_ssl_do_close (ssl);
      return 0;
    } else if (do_write <= 0) {
      if (ssl->debug & HNDBG_SSL)
        hin_debug ("ssl shutdown issued %d %d\n", do_write, n);
      extern httpd_annoy_t default_annoy;
      httpd_annoy_t * annoy = &default_annoy;
      hin_ssl_timeout (ssl, annoy->ssl_timeout);
    } else if (n < 0) {
      if (ssl->debug & HNDBG_SSL)
        hin_debug ("ssl close not yet complete error %d\n", n);
      hin_ssl_handle_io (ssl, n);
    }
  }

  if (ssl->flags & (HIN_SSL_CLOSED|HIN_SSL_ERROR)) {
    hin_ssl_do_close (ssl);
  }

  if (hin_ssl_do_read (ssl) > 0) {
    goto restart;
  }
  hin_ssl_do_write (ssl);

  return 0;
}

int hin_ssl_request_write (hin_buffer_t * buffer) {
  hin_ssl_t * ssl = buffer->ssl;
  if (buffer->ssl_list.prev || buffer->ssl_list.next) {
    hin_weird_error (435343478);
    return -1;
  }
  if (ssl->bufs.next == NULL) {
    hin_ssl_get_buf (ssl, buffer);
  }
  basic_dlist_append (&ssl->write_list, &buffer->ssl_list);
  hin_ssl_advance (ssl);
  return 0;
}

int hin_ssl_request_read (hin_buffer_t * buffer) {
  hin_ssl_t * ssl = buffer->ssl;
  if (buffer->ssl_list.prev || buffer->ssl_list.next) {
    hin_weird_error (436767676);
    return -1;
  }
  if (ssl->bufs.next == NULL) {
    hin_ssl_get_buf (ssl, buffer);
  }
  basic_dlist_append (&ssl->read_list, &buffer->ssl_list);
  hin_ssl_advance (ssl);
  return 0;
}

int hin_ssl_request_close (hin_buffer_t * buffer) {
  hin_ssl_t * ssl = buffer->ssl;
  if (buffer->ssl_list.prev || buffer->ssl_list.next) {
    hin_weird_error (436767876);
    return -1;
  }
  if (ssl->bufs.next == NULL) {
    hin_ssl_get_buf (ssl, buffer);
  }
  ssl->flags |= HIN_SSL_CLOSE;
  ssl->flags &= ~HIN_SSL_CLOSED;
  basic_dlist_append (&ssl->close_list, &buffer->ssl_list);
  hin_ssl_advance (ssl);
  return 0;
}

#endif
