
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "hclient.h"
#include "hextra.h"
#include "hmaster.h"
#include "conf.h"

#include "hin_internal.h"
#include "../http/server/httpd_internal.h"

HIN_EXPORT hin_master_global_t hin_g;
HIN_EXPORT HIN_LOCAL_VAR hin_master_local_t hin_t;

HIN_EXPORT int hin_init () {
  if (hin_event_init () < 0) {
    hin_threadsafe_var (&hin_g.flags, hin_g.flags|HIN_FORCE_EPOLL);
  }
  hin_timer_init (NULL);
  hin_threadsafe_var (&hin_g.flags, hin_g.flags & ~(HIN_FLAG_QUIT|HIN_FLAG_FINISH));
  if (hin_g.max_fds == 0)
    hin_g.max_fds = 1024;
  hin_g.cork_size = HIN_HTTPD_CORK_LIMIT;
  return 0;
}

HIN_EXPORT int hin_clean () {
  hin_event_clean ();
  hin_epoll_clean ();
  hin_ssl_cleanup ();
  hin_timer_cleanup ();

  httpd_vhost_clean_all ();

  return 0;
}

HIN_EXPORT void hin_stop () {
  if (hin_g.flags & HIN_FLAG_QUIT) {
    return;
  }
  hin_struct_lock (&hin_g);
  hin_threadsafe_var (&hin_g.flags, hin_g.flags | HIN_FLAG_QUIT);

  basic_dlist_t * elem = hin_g.server_list.next;
  if (elem == NULL) {
    hin_struct_unlock (&hin_g);
    hin_check_alive ();
    return ;
  }
  while (elem) {
    hin_server_t * server = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;

    if (hin_g.debug & HNDBG_CONFIG)
      hin_debug ("stopping server %d\n", server->c.sockfd);
    hin_server_close (server);
  }
  hin_struct_unlock (&hin_g);
}

HIN_EXPORT int hin_check_alive () {
  if ((hin_g.flags & HIN_FLAG_QUIT) == 0) {
    return 1;
  }

  hin_stop ();

  if (hin_g.server_list.next || hin_g.num_connection > 0) {
    if ((hin_g.debug & HNDBG_CONFIG) && (hin_t.flags & HIN_FLAG_CHECK_ALIVE_TIMER) == 0 && hin_t.id == 0) {
      hin_debug ("hin closing ... still alive:\n %d servers\n %d clients\n %d connections\n", hin_g.num_listen, hin_g.num_client, hin_g.num_connection);
      hin_t.flags |= HIN_FLAG_CHECK_ALIVE_TIMER;
    }

    return 1;
  }

  hin_threadsafe_var (&hin_g.flags, hin_g.flags | HIN_FLAG_FINISH);
  return 0;
}




