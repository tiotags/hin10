
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <unistd.h>

#include "hin.h"
#include "hextra.h"
#include "hmaster.h"
#include "conf.h"

#include "hin_internal.h"

#define HIN_TIMER_HASH 643464
#define DIVIDER 64

typedef struct {
  time_t time;
  struct __kernel_timespec ts;
  int pad;
  int (*callback) (int ms);
} hin_timeout_t;

static HIN_LOCAL_VAR hin_buffer_t * timeout_buffer = NULL;
static HIN_LOCAL_VAR basic_ht_t * timer_ht = NULL;
static HIN_LOCAL_VAR basic_ht_hash_t last_key = 0;

basic_ht_hash_t hin_timer_key_from_timespec (struct timespec * tm) {
  basic_ht_hash_t key = tm->tv_sec * DIVIDER + tm->tv_nsec * DIVIDER / 1000000000;
  //printf ("nsec %ld\nkey  %ld\n", tm->tv_nsec, key);
  return key;
}

int hin_timer_key_to_timespec (basic_ht_hash_t key, struct timespec * tm) {
  tm->tv_sec = key / DIVIDER;
  tm->tv_nsec = (key % DIVIDER) * 1000000000 / DIVIDER;
  //printf ("nsec %ld\nkey  %ld\n", tm->tv_nsec, key);
  return 0;
}

HIN_EXPORT int hin_timer_remove (hin_timer_t * timer) {
  basic_ht_hash_t key = hin_timer_key_from_timespec (&timer->time);
  basic_ht_pair_t * pair = basic_ht_get_pair (timer_ht, key, key);
  if (pair == NULL) { return 0; }
  basic_dlist_t * dlist = (basic_dlist_t *)&pair->d.str;
  basic_dlist_remove (dlist, &timer->list);
  return 0;
}

HIN_EXPORT int hin_timer_free (hin_timer_t * timer) {
  if (timer == NULL) return 0;
  hin_timer_remove (timer);
  free (timer);
  return 0;
}

static int hin_timer_init_global () {
  hin_struct_lock (&hin_g);
  if (timer_ht) goto finish;

  timer_ht = basic_ht_create (1024, HIN_TIMER_HASH);

  struct timespec new;
  if (clock_gettime (CLOCK_MONOTONIC, &new) < 0) {
    hin_perror ("monotonic");
    exit (1);
  }
  last_key = hin_timer_key_from_timespec (&new);

finish:
  hin_struct_unlock (&hin_g);
  return 0;
}

HIN_EXPORT int hin_timer_start (hin_timer_t * timer) {
  basic_ht_hash_t key = hin_timer_key_from_timespec (&timer->time);

  if (timer_ht == NULL) hin_timer_init_global ();

  if (key < last_key) {
    key = last_key;
    hin_timer_key_to_timespec (key, &timer->time);
  }

  // remove the old timer ?
  basic_ht_pair_t * pair = basic_ht_get_pair (timer_ht, key, key);
  if (pair == NULL) {
    pair = basic_ht_set_pair (timer_ht, key, key, 0, 0);
  }

  // TODO hacky ?
  basic_dlist_t * dlist = (basic_dlist_t *)&pair->d.str;
  basic_dlist_append (dlist, &timer->list);

  return 0;
}

int hin_timer_set (struct hin_timer_struct ** timer_ptr, hin_timer_callback_t * callback, void * ptr, int msec) {
  hin_timer_t * timer = *timer_ptr;
  if (msec <= 0) {
    if (timer) {
      hin_timer_free (timer);
      *timer_ptr = NULL;
    }
    return 0;
  }

  if (timer == NULL) {
    timer = halloc_z (sizeof (hin_timer_t));
    timer->callback = callback;
    timer->ptr = ptr;
    *timer_ptr = timer;
  } else {
    hin_timer_remove (timer);
  }

  clock_gettime (CLOCK_MONOTONIC, &timer->time);
  timer->time.tv_sec += msec / 1000;
  timer->time.tv_nsec += (msec % 1000) * 1000000;
  while (timer->time.tv_nsec > 1000000000) {
    timer->time.tv_sec++;
    timer->time.tv_nsec -= 1000000000;
  }

  hin_timer_start (timer);
  return 0;
}

static void hin_timer_check_timeouts () {
  if (timer_ht == NULL) return ;

  struct timespec new;
  if (clock_gettime (CLOCK_MONOTONIC, &new) < 0) {
    hin_perror ("monotonic");
    return ;
  }

  basic_ht_hash_t key1 = last_key;
  basic_ht_hash_t key2 = hin_timer_key_from_timespec (&new);
  last_key = key2 + 1;

  for (basic_ht_hash_t key = key1; key <= key2; key++) {
    basic_ht_pair_t * pair = basic_ht_get_pair (timer_ht, key, key);
    if (pair == NULL) continue;

    basic_dlist_t * dlist = (basic_dlist_t *)&pair->d.str;
    basic_dlist_t * elem = dlist->next;
    while (elem) {
      hin_timer_t * timer = basic_dlist_ptr (elem, offsetof (hin_timer_t, list));
      elem = elem->next;

      if (timer->callback (timer) == 0) {
        basic_dlist_remove (dlist, &timer->list);
        free (timer);
      }
    }
    basic_ht_delete_pair (timer_ht, key, key);
  }
}

void hin_timer_process () {
  hin_epoll_check ();
  hin_server_reaccept ();
  hin_timer_check_timeouts ();

  hin_timeout_t * tm = (void*)&timeout_buffer->buffer;
  time_t new = time (NULL);
  if (tm->time == new) return;

  hin_socket_do_retry ();
  hin_t.flags &= ~(HIN_FLAG_OVERLOAD_TIMER|HIN_FLAG_CHECK_ALIVE_TIMER);

  if (tm->callback)
    tm->callback (1000);

  tm->time = new;
}

static int hin_timer_callback (hin_buffer_t * buffer, int ret) {
  if (ret < 0 && ret != -ETIME) {
    hin_error ("timer error %s", strerror (-ret));
  }
  hin_timeout_t * tm = (void*)&buffer->buffer;
  if (hin_request_timeout (buffer, &tm->ts, 0, 0) < 0) {
    hin_weird_error (78347122);
    return -1;
  }

  hin_timer_process ();

  return 0;
}

HIN_EXPORT int hin_timer_init (int (*callback) (int ms)) {
  if (timeout_buffer) {
    hin_timeout_t * tm = (void*)&timeout_buffer->buffer;
    tm->callback = callback;
    return 0;
  }

  hin_buffer_t * buf = hin_buffer_alloc (sizeof (hin_timeout_t));
  buf->fd = 0;
  buf->callback = hin_timer_callback;
  timeout_buffer = buf;

  hin_timeout_t * tm = (void*)&buf->buffer;
  tm->ts.tv_sec = HIN_HTTPD_TIME_DT / 1000;
  tm->ts.tv_nsec = (HIN_HTTPD_TIME_DT % 1000) * 1000000;
  tm->callback = callback;

  if ((hin_g.flags & HIN_FORCE_EPOLL) == 0) {
    if (hin_request_timeout (buf, &tm->ts, 0, 0) < 0) {
      hin_weird_error (212223556);
      return -1;
    }
  }

  return 0;
}

void hin_timer_cleanup () {
  if (timeout_buffer) {
    timeout_buffer->flags &= ~HIN_ACTIVE;
    hin_buffer_stop_clean (timeout_buffer);
    timeout_buffer = NULL;
  }
  if (timer_ht) {
    basic_ht_free (timer_ht);
    timer_ht = NULL;
  }
}

