
#ifndef HIN_MASTER_H
#define HIN_MASTER_H

#include "hin.h"

enum {
HIN_FLAG_RUN = 0x1, HIN_FLAG_QUIT = 0x2,
HIN_CREATE_DIRECTORY = 0x4, HIN_VERBOSE_ERRORS = 0x8,
HIN_FLAG_FINISH = 0x10, HIN_FORCE_EPOLL = 0x20,
HIN_NO_TRAILING_SLASH = 0x40, HIN_SSL_ALLOW_EMPTY_CERT = 0x80,
HIN_HTTP_REUSE_CONNECTION = 0x100, HIN_ANSI_COLOR = 0x200,
};

enum {
HIN_FLAG_OVERLOAD_TIMER = 0x1,
HIN_FLAG_CHECK_ALIVE_TIMER = 0x2,
};

typedef struct {
  basic_dlist_t connection_idle;
  basic_mutex_t;
} hin_master_hget_idle_t;

typedef struct {
  uint32_t flags;
  uint32_t debug;
  int num_fds, max_fds;
  int num_listen;
  int num_client, max_client;
  int num_connection, num_idle;
  int num_thread;
  off_t cork_size;
  uintptr_t mem_usage;
  basic_dlist_t server_list;
  basic_dlist_t server_retry;
  basic_dlist_t connection_idle;
  basic_dlist_t vhost_list;
  basic_dlist_t cert_list;
  pthread_mutex_t mutex;
} hin_master_global_t;

typedef struct {
  int id;
  int num_client, num_idle;
  uint32_t flags;
  basic_dlist_t httpd_idle;
  basic_dlist_t connection_list;
} hin_master_local_t;

extern hin_master_global_t hin_g;
extern HIN_LOCAL_VAR hin_master_local_t hin_t;

int hin_init ();
int hin_server_start_thread ();
int hin_timer_init (int (*callback) (int ms));
int hin_clean ();
int hin_event_wait ();
void hin_event_loop ();

void hin_stop ();
int hin_check_alive ();

#endif

