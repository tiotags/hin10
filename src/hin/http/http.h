
#ifndef HIN_HTTP_H
#define HIN_HTTP_H

#include "hin.h"
#include "hclient.h"

#include <basic_pattern.h>

enum {
HIN_REQ_HEADERS = 0x1, HIN_REQ_DATA = 0x2, HIN_REQ_POST = 0x4, HIN_REQ_WAIT = 0x8,
HIN_REQ_PROXY = 0x10, HIN_REQ_CGI = 0x20, HIN_REQ_FCGI = 0x40, HIN_REQ_END = 0x80,
HIN_REQ_STOPPING = 0x100, HIN_REQ_ERROR = 0x200,
HIN_REQ_ERROR_HANDLED = 0x400, HIN_REQ_IDLE = 0x800,
};

enum {
HIN_METHOD_GET = 0x10,
HIN_METHOD_HEAD = 0x11,
HIN_METHOD_OPTIONS = 0x12,

HIN_METHOD_POST = 0x20,

HIN_METHOD_UNSUPPORTED = 0x40,
HIN_METHOD_PUT = 0x41,
HIN_METHOD_DELETE = 0x42,
HIN_METHOD_CONNECT = 0x43,
HIN_METHOD_TRACE = 0x44,

HIN_METHOD_COPY = 0x45,
HIN_METHOD_LOCK = 0x46,
HIN_METHOD_MKCOL = 0x47,
HIN_METHOD_MOVE = 0x48,
HIN_METHOD_PROPFIND = 0x49,
HIN_METHOD_PROPPATCH = 0x4a,
HIN_METHOD_UNLOCK = 0x4b,

HIN_METHOD_UNKNOWN = 0x4f,
};

enum  {
HIN_HTTP_VERSION_UNKNOWN = 0x1,
};

enum {
HIN_HTTP_VER10 = 0x1, HIN_HTTP_VER11 = 0x2,
HIN_HTTP_MODIFIED = 0x4, HIN_HTTP_ETAG = 0x8,
HIN_HTTP_KEEPALIVE = 0x10, HIN_HTTP_CHUNKED = 0x20,
HIN_HTTP_DEFLATE = 0x40, HIN_HTTP_GZIP = 0x80,
HIN_HTTP_CACHE = 0x100, HIN_HTTP_BANNER = 0x200,
HIN_HTTP_CHUNKED_UPLOAD = 0x400, HIN_HTTP_LOCAL_CACHE = 0x800,
HIN_HTTP_DATE = 0x1000, HIN_HTTP_POST = 0x2000,
HIN_HTTP_RANGE = 0x4000,
};

#define HIN_HTTP_VERSION (HIN_HTTP_VER10|HIN_HTTP_VER11)
#define HIN_HTTP_COMPRESS (HIN_HTTP_DEFLATE|HIN_HTTP_GZIP)

#define HIN_HTTP_PATH_ACCEPT "%w.%%_-/+&$=,:;~@!'*%(%)%[%]"
//?# // are special characters needed for parsing
#define HIN_HTTP_DATE_FORMAT "%a, %d %b %Y %X GMT"

typedef struct {
  off_t start, end;
  basic_dlist_t list;
} httpd_client_range_t;

typedef struct {
  int fd;
  off_t sz;
  char * post_sep;
  hin_pipe_t * in_pipe;
} httpd_client_post_t;

typedef struct {
  hin_client_t c;
  hin_buffer_t * read_buffer;
  hin_pipe_t * out_pipe;
  char * hostname, * path, * query;
  char * append_headers;
  char * content_type;
  off_t sent_bytes;
  int annoy;
  uint16_t method, status;
  uint32_t state;
  uint32_t peer_flags;
  uint32_t disable;
  uint32_t debug;
  uint64_t etag;
  time_t modified_since;

  struct basic_vfs_node_struct * file;
  struct httpd_vhost_struct * vhost;
  struct httpd_compress_data_struct * compress_data;
  struct hin_timer_struct * wait_timer;

  basic_dlist_t range_requests;
  httpd_client_range_t * current_range;

  httpd_client_post_t * post;

  basic_dlist_t work_list;

  string_t headers;
} httpd_client_t;

typedef struct httpd_annoy_struct {
  int small_read_size;
  int small_read_penalty;

  off_t min_speed;
  int min_speed_penalty;

  off_t max_speed;

  int max_annoy;

  // timeouts
  int idle_timeout_wait;
  int idle_timeout_first;
  int idle_timeout_overload;

  int ssl_timeout;

  uintptr_t max_mem_usage;
  float max_mem_usage_ratio;
} httpd_annoy_t;

int httpd_error (httpd_client_t * http, int status, const char * fmt, ...);

int httpd_respond_text (httpd_client_t * http, int status, const char * body);
int httpd_respond_error (httpd_client_t * http, int status, const char * body);
int httpd_respond_fatal (httpd_client_t * http, int status, const char * body);
int httpd_respond_fatal_and_full (httpd_client_t * http, int status, const char * body);
int httpd_respond_buffer (httpd_client_t * http, int status, hin_buffer_t * data);
int httpd_respond_redirect (httpd_client_t * http, int status, const char * location);
int httpd_respond_redirect_https (httpd_client_t * http);

// httpd state handling
int httpd_client_start_request (httpd_client_t * http);
int httpd_client_finish_request (httpd_client_t * http);
int httpd_client_finish_output (httpd_client_t * http, hin_pipe_t * pipe);
int httpd_client_shutdown (httpd_client_t * http);

// string
int hin_find_line (string_t * source, string_t * line);

const char * hin_http_method_name (int num);
const char * http_status_name (int nr);
const char * hin_http_version_name (uint32_t version);
int hin_http_version (uint32_t flags);

int hin_http_parse_header_line (string_t * line, int * method, string_t * path, int * version);
int hin_httpd_parse_path (httpd_client_t * http, string_t * source);
char * hin_parse_url_encoding (string_t * source, uint32_t flags);

int httpd_append_header (httpd_client_t * http, const char * name, const char * value);

#endif
