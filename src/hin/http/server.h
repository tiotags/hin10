
#ifndef HIN_HTTP_SERVER_H
#define HIN_HTTP_SERVER_H

#include "http.h"

#include "hlisten.h"

typedef struct httpd_server_struct {
  hin_server_t s;
  int (*begin_callback) (httpd_client_t * http);
  int (*process_callback) (httpd_client_t * http);
  int (*finish_callback) (httpd_client_t * http);
  int (*finish_output_callback) (httpd_client_t * http, hin_pipe_t * pipe);
  int (*error_callback) (httpd_client_t * http, int error_code, const char * msg);
  int (*read_callback) (hin_buffer_t * buffer, int received);
} httpd_server_t;

int hin_httpd_start (httpd_server_t * server, const char * addr, const char * port, const char * sock_type, void * ssl_ctx);

#endif
