
#ifndef HIN_HTTP_CLIENT_H
#define HIN_HTTP_CLIENT_H

#include "hin.h"
#include "hclient.h"
#include "uri.h"

#include <basic_pattern.h>

enum {
HIN_HTTP_STATE_NIL,
HIN_HTTP_STATE_CONNECTED = 0x1,
HIN_HTTP_STATE_SEND,
HIN_HTTP_STATE_HEADERS,
HIN_HTTP_STATE_HEADERS_SENT,
HIN_HTTP_STATE_FINISH,
HIN_HTTP_STATE_ERROR = 0x10,
HIN_HTTP_STATE_CONNECTION_FAILED,
HIN_HTTP_STATE_SSL_FAILED,
HIN_HTTP_STATE_HEADERS_FAILED,
};

typedef struct http_client_upload_struct {
  hin_pipe_t * pipe;
  char * content_type;
} hin_http_post_t;

typedef struct hget_client_struct {
  hin_client_t c;
  uint32_t flags;
  uint32_t io_state;
  hin_uri_t uri;
  char * host, * port, * hostname;
  int method;

  int status;
  int save_fd;
  off_t sz, count;

  uint32_t debug;
  hin_buffer_t * read_buffer;
  void * progress;

  // cache
  uint32_t cache_flags;
  time_t cache;

  hin_http_post_t * upload;

  int (*state_callback) (struct hget_client_struct * http, uint32_t state, uintptr_t data);
  int (*read_callback) (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
} hget_client_t;

hget_client_t * hget_find_connection (const char * url);
int hget_client_start (hget_client_t * http);
int hget_client_shutdown (hget_client_t * http);

int hget_client_finish_request (hget_client_t * http);

// temp
hget_client_t * httpd_rproxy_request (httpd_client_t * parent, hget_client_t * http, const char * url);

#endif
