
#ifndef HIN_HTTPD_VHOST_H
#define HIN_HTTPD_VHOST_H

#include "hin.h"
#include "http/http.h"
#include "http/server.h"
#include "hlisten.h"

#include <basic_lists.h>
#include <basic_hashtable.h>

enum {
HIN_HSTS_SUBDOMAINS = 0x1, HIN_HSTS_PRELOAD = 0x2,
HIN_HSTS_NO_REDIRECT = 0x4, HIN_HSTS_NO_HEADER = 0x8,
HIN_DIRECTORY_LISTING = 0x10, HIN_DIRECTORY_NO_REDIRECT = 0x20,
};

enum { HIN_VHOST_MAP_PREFILE = 0, HIN_VHOST_MAP_PROCESS, HIN_VHOST_MAP_FINISH, HIN_VHOST_MAP_LAST };

typedef struct httpd_vhost_scope_struct {
  char * rproxy_url;
  int rproxy_url_len;
  // raw responses
} httpd_vhost_scope_t;

typedef struct httpd_vhost_map_struct {
  int state;
  const char * pattern;
  void * ptr;
  struct httpd_vhost_struct * vhost;
  basic_dlist_t list;

  httpd_vhost_scope_t scope;

  char buffer[];
} httpd_vhost_map_t;

typedef struct httpd_vhost_struct {
  // callback
  uint32_t magic;
  uint32_t disable;
  uint32_t vhost_flags;
  char * hostname;
  struct hin_ssl_ctx_struct * ssl;
  httpd_server_t * bind;
  int hsts;
  basic_ht_t * names;

  uint32_t debug, nodebug;
  void * cwd_dir;
  struct httpd_annoy_struct * annoy;

  httpd_vhost_scope_t scope;

  basic_dlist_t maps[HIN_VHOST_MAP_LAST];

  struct httpd_vhost_struct * parent;
  basic_dlist_t list;
} httpd_vhost_t;

int httpd_vhost_set_work_dir (httpd_vhost_t * vhost, const char * rel_path);

httpd_vhost_t * httpd_vhost_get (httpd_vhost_t * vhost, const char * name, int name_len);
httpd_vhost_t * httpd_vhost_create (const char * name, httpd_server_t * bind, httpd_vhost_t * parent);
int httpd_vhost_set_name (httpd_vhost_t * parent, const char * name, int name_len, httpd_vhost_t * new);
httpd_vhost_t * httpd_vhost_default ();
void httpd_vhost_set_debug (uint32_t debug);

// path maps
httpd_vhost_map_t * hin_vhost_map_path (httpd_vhost_t * vhost, int state, const char * path);
int httpd_vhost_map_callback (httpd_client_t * http, int type);
int hin_vhost_run_scope (httpd_client_t * http, httpd_vhost_t * vhost1, httpd_vhost_map_t * map, string_t * source);

#include "http.h"

int httpd_vhost_switch (httpd_client_t * http, httpd_vhost_t * vhost);
int httpd_vhost_request (httpd_client_t * http, const char * name, int len);


#endif

