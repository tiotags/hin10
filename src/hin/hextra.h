
#ifndef HIN_EXTRA_H
#define HIN_EXTRA_H

#include "hin.h"

#include "liburing/compat.h"
#include <sys/socket.h>
#include <stdarg.h>

#define HIN_CONNECT_MAGIC 0xfeabc321
#define HIN_CLIENT_MAGIC 0xfeabc111
#define HIN_SERVER_MAGIC 0xfcadc123
#define HIN_VHOST_MAGIC 0xeeefcac1
#define HIN_CERT_MAGIC 0xfaaaacc
#define HIN_FCGI_MAGIC 0xeaeaeaea

typedef struct {
  int (*read_callback) (hin_buffer_t * buffer, int received);
  int (*eat_callback) (hin_buffer_t * buffer, int num);
  int (*close_callback) (hin_buffer_t * buffer, int ret);
  int count;
  char * base;
} hin_lines_t;

// uring
int hin_request_timeout (hin_buffer_t * buffer, struct __kernel_timespec * ts, int count, int flags);
int hin_request_connect (hin_buffer_t * buffer, struct sockaddr * ai_addr, int ai_addrlen);
int hin_request_is_overloaded ();

// overload
int hin_overload_cutoff ();
int hin_overload_slowdown ();

// header
int hin_vheader (hin_buffer_t * buffer, const char * fmt, va_list ap);

// addr
enum { HIN_ADDR_ONLY_IP = 0x1 };

int hin_client_addr (char * str, int len, struct sockaddr * ai_addr, socklen_t ai_addrlen, uint32_t flags);

// ssl
typedef struct hin_ssl_ctx_struct {
  int refcount;
  uint32_t magic;
  void * ctx;
  const char * cert;
  const char * key;
  basic_dlist_t list;
} hin_ssl_ctx_t;

struct hin_ssl_ctx_struct * hin_ssl_cert_create (const char * cert, const char * key, uint32_t flags);
void hin_ssl_cert_ref (struct hin_ssl_ctx_struct * box);
void hin_ssl_cert_unref (struct hin_ssl_ctx_struct * box);

#include "hclient.h"

int hin_connect (const char * host, const char * port, hin_callback_t callback, void * parent, struct sockaddr * ai_addr, socklen_t * ai_addrlen);

int hin_ssl_connect_init (hin_client_t * client);

// lists
#include <stddef.h>
#define hin_buffer_list_ptr(elem) (basic_dlist_ptr (elem, offsetof (hin_buffer_t, list)))

// timer
typedef int (hin_timer_callback_t) (struct hin_timer_struct * timer);

typedef struct hin_timer_struct {
  struct timespec time;
  hin_timer_callback_t * callback;
  void * ptr;
  basic_dlist_t list;
} hin_timer_t;

int hin_timer_start (hin_timer_t * timer);
int hin_timer_remove (hin_timer_t * timer);
int hin_timer_free (hin_timer_t * timer);
int hin_timer_set (struct hin_timer_struct ** timer, hin_timer_callback_t * callback, void * ptr, int msec);

// debug
#include <basic_pattern.h>
uint32_t hin_get_debug_flag (string_t * source);


#endif
