
#ifndef HIN_CLIENT_H
#define HIN_CLIENT_H

#include "hin.h"

#include <sys/socket.h>

typedef struct hin_client_struct {
  int sockfd;
  uint32_t flags;
  uint32_t magic;
  void * parent;
  struct sockaddr ai_addr;
  socklen_t ai_addrlen;
  struct hin_ssl_struct * ssl;
  basic_dlist_t list;
} hin_client_t;

void hin_client_close (hin_client_t * client);

#endif

