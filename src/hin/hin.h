
#ifndef HIN_H
#define HIN_H

#include <stdint.h>
#include <sys/types.h>

#include <basic_lists.h>
#include <basic_mem.h>

#define HIN_EXPORT __attribute__ ((visibility ("default")))

#ifdef HIN_USE_THREAD
#include <pthread.h>
#include <unistd.h>
#define hin_struct_lock(s) pthread_mutex_lock (&(s)->mutex)
#define hin_struct_trylock(s) pthread_mutex_trylock (&(s)->mutex)
#define hin_struct_unlock(s) pthread_mutex_unlock (&(s)->mutex)
#define hin_threadsafe_var(ptr, new_val_formula) { \
 int iterations=1000; \
 do {\
  volatile __typeof__ (*ptr) old_val = *(ptr);\
  volatile __typeof__ (*ptr) new_val = (new_val_formula);\
  __typeof__ (*ptr) prev = __sync_val_compare_and_swap ((ptr), old_val, new_val);\
  if (prev == old_val) break;\
  iterations--; \
  if (iterations <= 0) { hin_weird_error (7682321); usleep (1); iterations = 1000; } \
} while (1); }
#define HIN_LOCAL_VAR __thread
#else
#define hin_struct_lock(s)
#define hin_struct_trylock(s)
#define hin_struct_unlock(s)
#define HIN_LOCAL_VAR
#define hin_threadsafe_var(ptr, new_val_formula) *(ptr) = (new_val_formula);
#endif

#define hin_threadsafe_inc(val) hin_threadsafe_var((val), *(val) + 1)
#define hin_threadsafe_dec(val) hin_threadsafe_var((val), *(val) - 1)

enum {
HIN_DONE = 	0x1, 	HIN_SOCKET = 	0x2,
HIN_FILE = 	0x4, 	HIN_OFFSETS = 	0x8,
HIN_SSL = 	0x10, 	HIN_COUNT = 	0x20,
HIN_HASH = 	0x40, 	HIN_SYNC = 	0x80,
HIN_INACTIVE = 	0x100, 	HIN_ACTIVE = 	0x200,
HIN_CONDENSE = 	0x400, 	HIN_CACHE = 	0x800,
HIN_EPOLL_READ = 0x1000, HIN_EPOLL_WRITE = 0x2000,
HIN_EPOLL_ACCEPT = 0x4000, HIN_EPOLL_CONNECT = 0x8000,
HIN_LINKED_REQ = 0x10000, HIN_CORK = 0x20000,
HIN_RECORD_SPEED = 0x40000,
HIN_READ  =	0x100000, HIN_WRITE =	0x200000,
HIN_ACCEPT =	0x400000, HIN_CONNECT = 0x800000,
HIN_CLOSE =	0x1000000,
HIN_DYN_BUFFER = 0x2000000,
};

#define HIN_EPOLL (HIN_EPOLL_READ | HIN_EPOLL_WRITE | HIN_EPOLL_ACCEPT | HIN_EPOLL_CONNECT)
#define HIN_IO (HIN_READ|HIN_WRITE|HIN_ACCEPT|HIN_CONNECT|HIN_CLOSE)
#define HIN_SYS_BUF (HIN_DYN_BUFFER)

enum {
HNDBG_BASIC=	0x1,		HNDBG_CONFIG=	0x2,
HNDBG_RW_ERROR=	0x4,		HNDBG_400ERR=	0x8,
HNDBG_VFS=	0x10,		HNDBG_SSL=	0x20,
HNDBG_SYSCALL=	0x40,		HNDBG_SOCKET=	0x80,
HNDBG_HTTP=	0x100,		HNDBG_CGI=	0x200,
HNDBG_PROXY=	0x400,		HNDBG_HTTP_FILTER=0x800,
HNDBG_POST=	0x1000,		HNDBG_CHILD=	0x2000,
HNDBG_CACHE=	0x4000,		HNDBG_TIMEOUT=	0x8000,
HNDBG_RW=	0x10000,	HNDBG_MEMORY=	0x20000,
HNDBG_PIPE=	0x40000,	HNDBG_INFO=	0x80000,
HNDBG_PROGRESS=	0x100000,	HNDBG_ANNOY=	0x200000,
HNDBG_FDS=	0x400000,	HNDBG_MIME=	0x800000,
HNDBG_URING=	0x1000000,	HNDBG_LAST=	0x2000000,
HNDBG_ALL=	0x7fffffff,
};

typedef struct hin_buffer_struct hin_buffer_t;
typedef int (*hin_callback_t) (struct hin_buffer_struct * buffer, int ret);

typedef struct hin_buffer_struct {
  int fd;
  uint32_t flags;
  uint32_t debug;
  hin_callback_t callback;
  off_t pos;
  int count, sz;
  void * parent;
  basic_dlist_t list, ssl_list, pipe_dlist;
  char * ptr;
  struct hin_ssl_struct * ssl;
  char buffer[];
} hin_buffer_t;

// try to only use 2 pages for each buffer
#define HIN_BUFSZ (8192-(int)sizeof(hin_buffer_t)-100)

enum {
HIN_PIPE_DECODE = 0x1,		// nothing atm
HIN_PIPE_COUNT = 0x2,		// will the buffers added count for the byte count
HIN_PIPE_ALL = 0xff,		// all of the above
};

typedef struct {
  int fd;
  uint32_t flags;
  struct hin_ssl_struct * ssl;
  off_t pos, count;
} hin_pipe_dir_t;

typedef struct {
  off_t speed, max_speed;
  off_t last_count;
  // callback
  int (*callback) (void * timer);
  struct hin_timer_struct * timer;
  uint64_t hash;
} hin_pipe_stats_t;

typedef struct hin_pipe_struct hin_pipe_t;

struct hin_pipe_struct {
  hin_pipe_dir_t in, out;
  off_t sz;			// the amount expected to be read if HIN_COUNT
  uint32_t flags;
  uint32_t debug;

  int (*decode_callback) (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
  int (*read_callback) (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);
  int (*finish_callback) (hin_pipe_t * pipe);
  int (*out_error_callback) (hin_pipe_t * pipe, int err);
  int (*in_error_callback) (hin_pipe_t * pipe, int err);
  hin_buffer_t * (*buffer_callback) (hin_pipe_t * pipe, int sz);

  basic_dlist_t write_que, writing, reading;
  int num_que;
  int write_que_size;

  void * parent;
  hin_pipe_stats_t * stats;
  void * extra;
};

// uring
int hin_request_write (hin_buffer_t * buffer);
int hin_request_read (hin_buffer_t * buffer);
int hin_request_accept (hin_buffer_t * buffer, int flags);

int hin_request_close (hin_buffer_t * buffer);
int hin_request_openat (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mode);
int hin_request_statx (hin_buffer_t * buffer, int dfd, const char * path, int flags, int mask);
int hin_request_cancel (hin_buffer_t * buffer, uint32_t flags);

// pipe
hin_buffer_t * hin_pipe_get_buffer (hin_pipe_t * pipe, int sz);
int hin_pipe_init (hin_pipe_t * pipe);
int hin_pipe_start (hin_pipe_t * pipe);
int hin_pipe_advance (hin_pipe_t * pipe);
int hin_pipe_abort (hin_pipe_t * pipe);
int hin_pipe_finish (hin_pipe_t * pipe);

int hin_pipe_append_raw (hin_pipe_t * pipe, hin_buffer_t * buffer);
int hin_pipe_prepend_raw (hin_pipe_t * pipe, hin_buffer_t * buf);
int hin_pipe_write_process (hin_pipe_t * pipe, hin_buffer_t * buffer, uint32_t flags);

// buffer
hin_buffer_t * hin_buffer_alloc (int sz);
hin_buffer_t * hin_buffer_create_from_data (void * parent, const char * ptr, int sz);
void hin_buffer_clean (hin_buffer_t * buffer);
void hin_buffer_stop_clean (hin_buffer_t * buffer);
int hin_buffer_continue_write (hin_buffer_t * buf, int ret);

hin_buffer_t * hin_lines_create_raw (int sz);
int hin_lines_request (hin_buffer_t * buffer, int min);
int hin_lines_reread (hin_buffer_t * buf);
int hin_lines_write (hin_buffer_t * buf, char * data, int len);
int hin_lines_prepare (hin_buffer_t * buffer, int num);
int hin_lines_eat (hin_buffer_t * buffer, int num);

// header
int hin_header (hin_buffer_t * buffer, const char * fmt, ...);
int hin_header_raw (hin_buffer_t * buffer, const char * data, int len);
void * hin_header_ptr (hin_buffer_t * buffer, int len);
int hin_header_date (hin_buffer_t * buffer, const char * name, time_t time);

// debugging
void hin_error (const char * fmt, ...);
void hin_perror (const char * fmt, ...);
void hin_weird_error (int errcode);
#define hin_debug(fmt, ...) printf (fmt, ##__VA_ARGS__)
#define hin_hunt(fmt, ...) fprintf (stderr, "hunt! %s:%d %s " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__)

#endif
