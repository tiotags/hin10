
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hin.h"
#include "http/client.h"
#include "http/http.h"
#include "http/vhost.h"

#include <stddef.h>
#define hin_map_list_ptr(elem) (basic_dlist_ptr (elem, offsetof (httpd_vhost_map_t, list)))

static int httpd_vhost_map_free (httpd_vhost_map_t * map) {
  if (map->pattern) free ((void*)map->pattern);

  httpd_vhost_scope_t * scope = &map->scope;
  httpd_vhost_scope_clean (scope);

  free (map);
  return 0;
}

void httpd_vhost_map_clean (httpd_vhost_t * vhost) {
  for (int i=0; i<HIN_VHOST_MAP_LAST; i++) {
    basic_dlist_t * head = &vhost->maps[i];
    basic_dlist_t * elem = head->next;
    while (elem) {
      httpd_vhost_map_t * map = hin_map_list_ptr (elem);
      elem = elem->next;

      basic_dlist_remove (head, &map->list);
      httpd_vhost_map_free (map);
    }
  }
}

static int hin_vhost_pattern_match (string_t * source, const char * pattern) {
  const char * ptr = source->ptr;
  while (1) {
    if (*pattern == '*') break;
    if (*pattern == '/'
    && *ptr == '\0'
    && (*(pattern+1) == '*' || *(pattern+1) == '\0')) {
      printf ("redirect\n");
    }
    if (*ptr == '\0') {
      if (*pattern == '\0') break;
      return 0;
    }
    if (*pattern == '\0') return 0;
    if (*ptr != *pattern) return 0;
    pattern++;
    ptr++;
  }
  int used = ptr - source->ptr;
  source->ptr += used;
  source->len -= used;
  return used;
}

HIN_EXPORT int hin_vhost_run_scope (httpd_client_t * http, httpd_vhost_t * vhost1, httpd_vhost_map_t * map, string_t * source) {
  httpd_vhost_t * vhost = http->vhost;
  httpd_vhost_scope_t * scope = &vhost->scope;
  if (map) {
    scope = &map->scope;
  }

  #ifdef HIN_USE_RPROXY
  if (scope->rproxy_url) {
    int len = scope->rproxy_url_len + 1;
    int path_len = source->len;
    int query_len = 0;
    if (http->query) query_len = strlen (http->query) + 1;

    char * url = malloc (len + path_len + query_len);
    sprintf (url, "%s%.*s%s", scope->rproxy_url, (int)source->len, source->ptr, http->query ? http->query : "");
    if (http->debug & HNDBG_HTTP)
      printf ("httpd %d rproxy to '%s' '%s' '%s'\n", http->c.sockfd, url, http->path, http->query);
    httpd_rproxy_request (http, NULL, url);
    free (url);
    return 1;
  }
  #endif
  return 0;
}

static int hin_vhost_run_map (httpd_client_t * http, httpd_vhost_t * vhost1, httpd_vhost_map_t * map, string_t * source) {
  if (http->debug & HNDBG_HTTP)
    hin_debug ("match map %s:%s rest %d '%.*s'\n", vhost1->hostname, map->pattern, (int)source->len, (int)source->len, source->ptr);

  if (hin_vhost_run_scope (http, vhost1, map, source))
    return 1;

  return 0;
}

HIN_EXPORT int httpd_vhost_map_callback (httpd_client_t * http, int type) {
  httpd_vhost_t * vhost = http->vhost;

  if (http->path == NULL) return 0;

  string_t source, orig;
  orig.ptr = http->path;
  orig.len = strlen (orig.ptr);

  while (vhost) {
    //printf ("running maps for %s\n", vhost->hostname);
    basic_dlist_t * elem = vhost->maps[type].next;
    while (elem) {
      httpd_vhost_map_t * map = hin_map_list_ptr (elem);
      elem = elem->next;
      //printf ("match %s on %s\n", http->path, map->pattern);

      source = orig;
      if (hin_vhost_pattern_match (&source, map->pattern) == 0)
        continue;

      if (hin_vhost_run_map (http, vhost, map, &source) != 0) {
        return 1;
      }
    }

    if (vhost->parent == NULL && vhost != httpd_vhost_default ()) {
      vhost = httpd_vhost_default ();
    } else {
      vhost = vhost->parent;
    }
  }
  return 0;
}

HIN_EXPORT httpd_vhost_map_t * hin_vhost_map_path (httpd_vhost_t * vhost, int state, const char * path) {
  if (vhost == NULL) {
    vhost = httpd_vhost_default ();
    printf ("default vhost is %p\n", vhost);
  }
  if (vhost == NULL) {
    hin_error ("map: default vhost is empty");
    return NULL;
  }

  if (state >= HIN_VHOST_MAP_LAST || state < 0) {
    hin_error ("unknown map style added %d for %s:%s", state, vhost->hostname, path);
    return NULL;
  }

  httpd_vhost_map_t * map = calloc (1, sizeof (*map));
  map->pattern = strdup (path);
  map->state = state;
  map->vhost = vhost;

  if (hin_g.debug & HNDBG_CONFIG)
    hin_debug ("add map %d for %s:%s\n", map->state, vhost->hostname, path);

  basic_dlist_append (&vhost->maps[map->state], &map->list);

  return map;
}

