
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "hmaster.h"
#include "http/http.h"
#include "http/server.h"

#include "httpd_internal.h"

int httpd_file_is_multirange (httpd_client_t * http) {
  if (http->status != 206) return 0;
  if (http->range_requests.next == NULL) return 0;
  if (http->range_requests.next->next == NULL) return 0;
  return 1;
}

HIN_EXPORT int httpd_error (httpd_client_t * http, int status, const char * fmt, ...) {
  va_list ap, ap1;
  va_start (ap, fmt);
  va_copy (ap1, ap);

  if ((http->debug & HNDBG_400ERR) && (status > 400 && status < 500)) {
    char buffer[1024];
    vsnprintf (buffer, sizeof (buffer), fmt, ap);
    fprintf (stderr, "error! httpd %d %d %s\n", http->c.sockfd, status, buffer);
  }

  if (status == 0) {
    va_end (ap);
    return 0;
  }

  char * body = NULL;
  if (hin_g.flags & HIN_VERBOSE_ERRORS) {
    char * msg = NULL;
    if (vasprintf ((char**)&msg, fmt, ap1) < 0) {
      hin_perror ("asprintf");
    }
    if (asprintf ((char**)&body, "<html><head></head><body><h1>Error %d: %s</h1><p>%s</p></body></html>\n", status, http_status_name (status), msg) < 0)
      hin_perror ("asprintf");
    free (msg);
  }
  if (status == 404 || status == 403) {
    httpd_respond_error (http, status, body);
  } else {
    httpd_respond_fatal (http, status, body);
  }
  if (body) free (body);

  va_end (ap);
  return 0;
}


