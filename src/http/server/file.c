
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#ifndef STATX_MTIME
#include <linux/stat.h>
#endif

#include <basic_pattern.h>
#include <basic_vfs.h>

#include "hin.h"
#include "hextra.h"
#include "http/http.h"
#include "http/file.h"
#include "conf.h"

#include "httpd_internal.h"

static int httpd_send_file_done (hin_pipe_t * pipe) {
  if (pipe->debug & HNDBG_PIPE)
    hin_debug ("pipe %d>%d file done %lld\n", pipe->in.fd, pipe->out.fd, (long long)pipe->out.count);

  httpd_client_t * http = pipe->parent;

  httpd_client_finish_output (http, pipe);

  return 0;
}

HIN_EXPORT int httpd_send_file (httpd_client_t * http, hin_file_t * file, hin_buffer_t * buf) {
  if ((http->method & HIN_METHOD_GET) == 0) {
    http->method = HIN_METHOD_GET;
    httpd_error (http, 405, "%s on a raw resource", hin_http_method_name (http->method));
    return 0;
  }

  off_t sz = file->size;

  if (buf == NULL) {
    buf = hin_buffer_alloc (HIN_BUFSZ);
    buf->flags |= HIN_SOCKET | (http->c.flags & HIN_SSL);
    buf->fd = http->c.sockfd;
    buf->count = 0;
    buf->parent = http;
    buf->ssl = http->c.ssl;
  }

  if (http->debug & HNDBG_PIPE) {
    const char * path = "";
    hin_debug ("pipe %d>%d file '%s' sz %lld\n", file->fd, http->c.sockfd, path, (long long)sz);
  }

  // do you need to check http->status for 200 or can you return a 304 for a 206
  if ((http->disable & HIN_HTTP_MODIFIED)) {
    http->modified_since = 0;
  }

  if ((http->disable & HIN_HTTP_ETAG)) {
    http->etag = 0;
  }

  if (http->modified_since && http->modified_since < file->modified) {
    http->status = 304;
  }

  if (http->etag) {
    if (http->etag == file->etag) {
      http->status = 304;
    } else {
      http->status = 200;
    }
  }

  if (HIN_HTTPD_MAX_COMPRESS_SIZE && sz > HIN_HTTPD_MAX_COMPRESS_SIZE) {
    http->disable |= HIN_HTTP_COMPRESS;
  }
  http->peer_flags &= ~http->disable;
  http->state |= HIN_REQ_DATA;

  off_t send_sz = 0;
  int num_fragments = 0;
  basic_dlist_t * elem = http->range_requests.next;
  while (elem) {
    httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
    elem = elem->next;

    if (range->start < 0) range->start = sz + range->start;
    if (range->end < 0) range->end = sz;

    if (range->start > sz || range->end > sz || range->start >= range->end) {
      httpd_error (http, 416, "out of range %lld-%lld>%lld", range->start, range->end, sz);
      return 0;
    }

    if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
      hin_debug ("httpd %d range req %lld-%lld/%lld\n", http->c.sockfd, (long long)range->start, (long long)range->end, (long long)sz);

    num_fragments++;
    send_sz += range->end - range->start;
  }
  if (num_fragments > 0
  && num_fragments == 1
  && http->status == 200
  && (http->disable & HIN_HTTP_RANGE) == 0
  && send_sz < sz) {
    http->status = 206;
  } else {
    send_sz = sz;
  }

  hin_pipe_t * pipe = calloc (1, sizeof (*pipe));
  hin_pipe_init (pipe);
  pipe->in.fd = file->fd;
  pipe->in.flags = HIN_OFFSETS | HIN_FILE | HIN_COUNT;
  pipe->finish_callback = httpd_send_file_done;
  pipe->flags |= HIN_CONDENSE;
  pipe->sz = sz;
  http->out_pipe = pipe;

  if (httpd_pipe_set_http11_response_options (http, pipe) < 0) {
    httpd_error (http, 500, "can't set default headers");
    return 0;
  }

  hin_header (buf, "HTTP/1.%d %d %s\r\n", http->peer_flags & HIN_HTTP_VER10 ? 0 : 1, http->status, http_status_name (http->status));
  httpd_write_common_headers (http, buf);

  if ((http->disable & HIN_HTTP_MODIFIED) == 0 && file->modified) {
    hin_header_date (buf, "Last-Modified: " HIN_HTTP_DATE_FORMAT "\r\n", file->modified);
  }
  if ((http->disable & HIN_HTTP_ETAG) == 0 && file->etag) {
    hin_header (buf, "ETag: \"%llx\"\r\n", file->etag);
  }
  if ((http->disable & HIN_HTTP_RANGE) == 0 && (http->peer_flags & HIN_HTTP_CHUNKED) == 0) {
    hin_header (buf, "Accept-Ranges: bytes\r\n");
  }

  elem = http->range_requests.next;
  httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
  if (http->status == 206) {
    http->current_range = range;
    if (httpd_file_is_multirange (http)) {
      // TODO set content length
    } else {
      hin_header (buf, "Content-Range: bytes %lld-%lld/%lld\r\n", range->start, (range->end-1), sz);
    }
    pipe->in.pos = range->start;
    pipe->sz = range->end - range->start;
  }

  if ((http->peer_flags & HIN_HTTP_CHUNKED) == 0) {
    hin_header (buf, "Content-Length: %lld\r\n", send_sz);
  }

  hin_header (buf, "\r\n");

  if (http->debug & HNDBG_RW)
    hin_debug ("httpd %d file response %d '\n%.*s'\n", http->c.sockfd, buf->count, buf->count, buf->ptr);

  hin_pipe_append_raw (pipe, buf);
  hin_pipe_start (pipe);

  return 0;
}

