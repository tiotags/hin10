
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <basic_pattern.h>

#include "hin.h"
#include "hextra.h"
#include "http/http.h"
#include "http/vhost.h"
#include "conf.h"

#include "httpd_internal.h"

static httpd_client_post_t * httpd_post_get_post (httpd_client_t * http) {
  if (http->post) return http->post;
  httpd_client_post_t * post = halloc_z (sizeof (*post));
  http->post = post;
  return post;
}

int httpd_parse_headers_line (httpd_client_t * http, string_t * line) {
  string_t param, param1, param2;
  httpd_client_post_t * post = NULL;

  if (matchi_string (line, "Accept-Encoding: ") > 0) {
    while (match_string (line, "([%w]+)", &param) > 0) {
      if (matchi_string (&param, "deflate") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  can use deflate\n");
        http->peer_flags |= HIN_HTTP_DEFLATE;
      } else if (matchi_string (&param, "gzip") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  can use gzip\n");
        http->peer_flags |= HIN_HTTP_GZIP;
      }
      if (match_string (line, "%s*,%s*") <= 0) break;
    }

  } else if (matchi_string (line, "Host:%s*([%w.]+)", &param1) > 0) {
    if (httpd_vhost_request (http, param1.ptr, param1.len) < 0) {
      // can't find hostname
    }

  } else if (matchi_string (line, "If-Modified-Since:%s*") > 0) {
    time_t tm = hin_date_str_to_time (line->ptr);
    http->modified_since = tm;

  } else if (matchi_string (line, "If-None-Match:%s*[\"]?") > 0) {
    uint64_t etag = strtol (line->ptr, NULL, 16);
    http->etag = etag;

  } else if (matchi_string (line, "Connection:%s*") > 0) {
    while (1) {
      if (matchi_string (line, "close") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  close\n");
        http->peer_flags &= ~HIN_HTTP_KEEPALIVE;
      } else if (matchi_string (line, "keep-alive") > 0) {
        if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
          hin_debug ("  keepalive\n");
        http->peer_flags |= HIN_HTTP_KEEPALIVE;
      } else if (match_string (line, "%w+") > 0) {
      }
      if (match_string (line, "%s*,%s*") < 0) break;
    }

  } else if (matchi_string (line, "Range:%s*") > 0) {
    if (matchi_string (line, "bytes%s*=%s*") <= 0) {
      return 1;
    }
    param2.len = 0;
    while ((match_string (line, "(%d+)-(%d*)", &param1, &param2) > 0)
    || (match_string (line, "(-%d+)", &param1) > 0)) {
      httpd_client_range_t * range = halloc_z (sizeof *range);

      if (param1.len > 0) {
        range->start = atoi (param1.ptr);
      } else {
        range->start = 0;
      }
      if (param2.len > 0) {
        range->end = atoi (param2.ptr) + 1;
      } else {
        range->end = -1;
      }
      basic_dlist_append (&http->range_requests, &range->list);

      if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
        hin_debug ("  range requested is %lld-%lld\n", (long long)range->start, (long long)range->end);

      if (matchi_string (line, "%s*,%s*") <= 0) break;
    }

  } else if (matchi_string (line, "Content-Length: (%d+)", &param) > 0) {
    post = httpd_post_get_post (http);
    post->sz = atoi (param.ptr);
    if (http->debug & (HNDBG_HTTP|HNDBG_POST))
      hin_debug ("  post length is %lld\n", (long long)post->sz);
    http->peer_flags |= HIN_HTTP_POST;

  } else if (matchi_string (line, "Transfer-Encoding:%s*") > 0) {
    post = httpd_post_get_post (http);
    if (matchi_string (line, "chunked") > 0) {
      if (http->debug & (HNDBG_HTTP|HNDBG_POST))
        hin_debug ("  post content encoding is chunked\n");
      http->peer_flags |= (HIN_HTTP_CHUNKED_UPLOAD | HIN_HTTP_POST);
    } else if (matchi_string (line, "identity") > 0) {
    } else {
      httpd_error (http, 501, "doesn't accept post with transfer encoding");
      return 0;
    }
  }
  return 1;
}

int httpd_parse_req (httpd_client_t * http, string_t * source) {
  string_t line;
  string_t orig = *source;

  while (1) {
    if (source->len <= 0) return 0;
    int ret = hin_find_line (source, &line);
    if (ret == 0) return 0;
    if (ret < 0) httpd_error (http, 400, "nul characters found");
    if (line.len == 0) break;
  }

  *source = orig;

  int status = hin_httpd_parse_path (http, source);
  if (status) {
    string_t line;
    hin_find_line (source, &line);
    httpd_error (http, status, "parsing request line %d '%.*s'", (int)line.len, (int)line.len, line.ptr);
    if (http->debug & (HNDBG_RW|HNDBG_RW_ERROR))
      hin_debug (" raw request %d '\n%.*s'\n", (int)orig.len, (int)orig.len, orig.ptr);
    return -1;
  }

  int version = hin_http_version (http->peer_flags);
  if (http->debug & (HNDBG_HTTP|HNDBG_RW))
    hin_debug ("httpd %d method %s path '%s' query '%s' ver %x\n", http->c.sockfd,
      hin_http_method_name (http->method), http->path, http->query, version);

  //http->count = -1;
  if (version != 0x10) http->peer_flags |= HIN_HTTP_KEEPALIVE;

  while (hin_find_line (source, &line)) {
    if (http->debug & HNDBG_RW)
      hin_debug (" %d '%.*s'\n", (int)line.len, (int)line.len, line.ptr);

    if (line.len == 0) break;
    int ret = httpd_parse_headers_line (http, &line);
    if (ret <= 0) {
      *source = orig;
      if (ret < 0) {
        httpd_error (http, 400, "shouldn't happen");
      }
      return -1;
    }
  }

  httpd_client_post_t * post = http->post;
  if (post) {
    if (http->peer_flags & HIN_HTTP_CHUNKED_UPLOAD) {
      post->sz = 0;
    }
    if (post->sz < 0) {
      post->sz = 0;
    }
  }
  if (HIN_HTTPD_ERROR_MISSING_HOSTNAME && (http->peer_flags & HIN_HTTP_VER10) == 0 && http->hostname == NULL) {
    httpd_error (http, 400, "missing hostname");
    return -1;
  }
  if (http->peer_flags & http->disable & HIN_HTTP_POST) {
    httpd_error (http, 405, "post disabled");
    return -1;
  } if ((http->method & HIN_METHOD_POST) && (http->peer_flags & HIN_HTTP_POST) == 0) {
    httpd_error (http, 411, "post missing size");
    return -1;
  } else if (HIN_HTTPD_MAX_POST_SIZE && post && post->sz >= HIN_HTTPD_MAX_POST_SIZE) {
    httpd_error (http, 413, "post size %lld >= %ld", (long long)post->sz, (long)HIN_HTTPD_MAX_POST_SIZE);
    return -1;
  }

  return (uintptr_t)source->ptr - (uintptr_t)orig.ptr;
}

