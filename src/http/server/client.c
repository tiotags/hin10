
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "hin.h"
#include "hextra.h"
#include "hmaster.h"
#include "http/server.h"
#include "http/vhost.h"
#include "conf.h"

#include "httpd_internal.h"

HIN_EXPORT void httpd_client_clean (httpd_client_t * http) {
  if (http->debug & (HNDBG_HTTP|HNDBG_MEMORY)) hin_debug ("httpd %d clean\n", http->c.sockfd);
  if (http->append_headers) free (http->append_headers);
  if (http->hostname) free (http->hostname);
  if (http->path) free (http->path);
  if (http->query) free (http->query);
  if (http->content_type) free (http->content_type);
  if (http->out_pipe) {
    hin_pipe_abort (http->out_pipe);
  }
  //http->annoy = 0; // not good idea/already done, should it be done ?

  httpd_compress_clean (http);

  basic_dlist_t * elem = http->range_requests.next;
  while (elem) {
    httpd_client_range_t * range = basic_dlist_ptr (elem, offsetof (httpd_client_range_t, list));
    elem = elem->next;

    basic_dlist_remove (&http->range_requests, &range->list);
    hfree (range);
  }

  if (http->post) {
    hfree (http->post);
  }

  hin_buffer_t * buf = http->read_buffer;
  void * ptr1 = http->wait_timer;
  uint8_t * ptr = ((uint8_t*)http) + sizeof (hin_client_t);
  memset (ptr, 0, sizeof (httpd_client_t) - sizeof (hin_client_t));
  http->read_buffer = buf;
  http->wait_timer = ptr1;
}

HIN_EXPORT int httpd_client_start_request (httpd_client_t * http) {
  http->state = HIN_REQ_HEADERS | (http->state & (HIN_REQ_STOPPING|HIN_REQ_IDLE));

  httpd_server_t * server = (httpd_server_t*)http->c.parent;

  http->status = 200;
  http->debug = server->s.debug;

  httpd_vhost_t * vhost = (httpd_vhost_t*)server->s.c.parent;
  if (vhost == NULL)
    vhost = httpd_vhost_default ();
  httpd_vhost_switch (http, vhost);

  if (http->debug & HNDBG_HTTP) {
    hin_debug ("http%sd %d request begin %lld\n",
      (http->c.flags & HIN_SSL) ? "s" : "", http->c.sockfd, (long long)time (NULL));
  }

  if (server->begin_callback)
    server->begin_callback (http);

  return 0;
}

HIN_EXPORT int httpd_client_finish_request (httpd_client_t * http) {
  http->state &= ~(HIN_REQ_DATA | HIN_REQ_POST);

  int keep = (http->peer_flags & HIN_HTTP_KEEPALIVE) && ((http->state & HIN_REQ_STOPPING) == 0);
  if (http->debug & HNDBG_HTTP) hin_debug ("httpd %d request done %s\n", http->c.sockfd, keep ? "keep" : "close");

  httpd_server_t * server = http->c.parent;
  if (server->finish_callback)
    server->finish_callback (http);

  hin_lines_eat (http->read_buffer, http->headers.len);

  if (keep) {
    httpd_client_clean (http);
    httpd_client_start_request (http);
    httpd_client_reread (http);

    httpd_client_set_idle (http, 1);
  } else {
    http->state |= HIN_REQ_END;
    httpd_client_shutdown (http);
  }

  return 0;
}

HIN_EXPORT int httpd_client_finish_output (httpd_client_t * http, hin_pipe_t * pipe) {
  http->sent_bytes = pipe->out.count;
  http->state &= ~HIN_REQ_DATA;
  http->out_pipe = NULL;

  httpd_server_t * server = http->c.parent;
  if (server->finish_output_callback)
    server->finish_output_callback (http, pipe);

  if (http->state & (HIN_REQ_DATA|HIN_REQ_POST)) return 0;
  return httpd_client_finish_request (http);
}

static int httpd_client_close_callback (hin_buffer_t * buffer, int ret) {
  httpd_client_t * http = (httpd_client_t*)buffer->parent;
  if (ret < 0) {
    hin_error ("httpd %d client close callback: %s", http->c.sockfd, ret < 0 ? strerror (-ret) : "");
    return -1;
  }
  if (http->debug & (HNDBG_HTTP)) hin_debug ("httpd %d close\n", http->c.sockfd);

  hin_buffer_stop_clean (http->read_buffer);
  http->read_buffer = NULL;

  httpd_client_clean (http);

  hin_buffer_clean (buffer);

  hin_client_close (&http->c);

  return 0;
}

HIN_EXPORT int httpd_client_shutdown (httpd_client_t * http) {
  if (http->state & HIN_REQ_STOPPING) return -1;
  http->state |= HIN_REQ_STOPPING;
  if (http->debug & (HNDBG_HTTP)) hin_debug ("httpd %d shutdown\n", http->c.sockfd);

  httpd_timer_update (http, -1);

  if (http->state & HIN_REQ_IDLE) {
    httpd_client_set_idle (http, -1);
  }

  hin_buffer_t * buf = hin_buffer_alloc (0);
  buf->flags |= HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = httpd_client_close_callback;
  buf->parent = (hin_client_t*)http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  if (hin_request_close (buf) < 0) {
    buf->flags |= HIN_SYNC;
    hin_request_close (buf);
  }
  return 0;
}


