
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hin.h"
#include "http/vhost.h"
#include "hmaster.h"

#include "httpd_internal.h"

HIN_EXPORT httpd_annoy_t default_annoy = {
// minimum read amount while reading headers
.small_read_size = 1000,
.small_read_penalty = 300,

// minimum bandwidth, if client downloads slower than this apply penalty
.min_speed = 10000,
.min_speed_penalty = 100,

// currently a global download speed limit for each connection
.max_speed = 0,

// if annoy gets higher than this client gets booted
.max_annoy = 800,

// timeouts in ms
// when a new connection is waiting and when the connection switched vhosts
.idle_timeout_first = 5000,
// when a request ended and you're waiting for new requests
.idle_timeout_wait = 30000,
 // when overloaded reduce timeouts
.idle_timeout_overload = 500,

.ssl_timeout = 1000,

// absolute memory usage above which server starts to slow down
.max_mem_usage = 0,
// if above is empty then try to set it programatically
.max_mem_usage_ratio = 0.5,
};

int httpd_increase_annoy (httpd_client_t * http, int amount) {
  http->annoy += amount;
  httpd_vhost_t * vhost = http->vhost;
  httpd_annoy_t * annoy = vhost->annoy;

  int drop = (http->annoy > annoy->max_annoy);
  if ((http->debug & HNDBG_ANNOY) ||
  (drop && (http->debug & HNDBG_RW_ERROR))) {
    char addr[1080];
    hin_client_addr (addr, sizeof addr, &http->c.ai_addr, sizeof (http->c.ai_addr), HIN_ADDR_ONLY_IP);
    hin_debug ("http %d '%s' %s annoy %d %d/%d %s\n", http->c.sockfd, http->path, addr, amount, http->annoy, annoy->max_annoy, drop ? "drop" : "ok");
  }
  if (drop) {
    httpd_respond_fatal (http, 429, NULL);
    return -1;
  }
  return 0;
}

static int httpd_wait_timer_callback (hin_timer_t * timer) {
  httpd_client_t * http = timer->ptr;
  if (http->debug & HNDBG_TIMEOUT)
    hin_debug ("httpd %d timedout\n", http->c.sockfd);
  http->wait_timer = NULL;
  httpd_client_shutdown (http);
  return 0;
}

void httpd_timer_update (httpd_client_t * http, int msec) {
  if (http->debug & HNDBG_TIMEOUT)
    hin_debug ("httpd %d timeout %d\n", http->c.sockfd, msec);

  hin_timer_set (&http->wait_timer, httpd_wait_timer_callback, http, msec);
}

void httpd_client_set_idle (httpd_client_t * http, int is) {
  if (is == 1 && (http->state & HIN_REQ_IDLE) == 0) {
    http->state |= HIN_REQ_IDLE;
    basic_dlist_append (&hin_t.httpd_idle, &http->work_list);
  } else if (is == -1 && (http->state & HIN_REQ_IDLE) != 0) {
    http->state &= ~HIN_REQ_IDLE;
    basic_dlist_remove (&hin_t.httpd_idle, &http->work_list);
  } else {
    hin_weird_error (5465765);
    return ;
  }
  hin_threadsafe_var (&hin_g.num_idle, hin_g.num_idle + is);
  hin_t.num_idle += is;
  if (is == 1 && hin_overload_slowdown ()) {
    httpd_flush_idle_connections ();
    return ;
   }
}

static int httpd_client_pipe_timer_callback (hin_timer_t * timer) {
  hin_pipe_t * pipe = timer->ptr;
  hin_pipe_stats_t * stats = pipe->stats;

  httpd_client_t * http = pipe->parent;
  httpd_vhost_t * vhost = http->vhost;
  httpd_annoy_t * annoy = vhost->annoy;

  if (stats->speed < annoy->min_speed) {
    hin_debug ("httpd %d min speed %ld < %ld\n", http->c.sockfd, stats->speed, annoy->min_speed);
    httpd_increase_annoy (http, annoy->min_speed_penalty);
  }
  return 0;
}

int httpd_pipe_set_idle_protections (httpd_client_t * http, hin_pipe_t * pipe) {
  pipe->flags |= HIN_RECORD_SPEED;

  hin_pipe_stats_t * stats = halloc_z (sizeof (hin_pipe_stats_t));
  pipe->stats = stats;
  stats->callback = (int (*)(void *))httpd_client_pipe_timer_callback;

  httpd_vhost_t * vhost = http->vhost;
  httpd_annoy_t * annoy = vhost->annoy;
  stats->max_speed = annoy->max_speed;

  return 0;
}

void httpd_flush_idle_connections () {
  if (hin_t.num_idle <= 0) return ;

  basic_dlist_t * elem = hin_t.httpd_idle.next;
  while (elem) {
    httpd_client_t * http = basic_dlist_ptr (elem, offsetof (httpd_client_t, work_list));
    elem = elem->next;

    if ((http->state & HIN_REQ_IDLE) == 0) continue;

    if (hin_g.debug & HNDBG_TIMEOUT)
      hin_debug ("httpd %d idle stopping\n", http->c.sockfd);

    httpd_client_set_idle (http, -1);
    httpd_timer_update (http, 100);
    //httpd_client_shutdown (http);
  }
}

