
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <basic_hashtable.h>

#include "hin.h"
#include "http/http.h"
#include "http/vhost.h"

#include "httpd_internal.h"

static httpd_vhost_t * default_vhost = NULL;
static basic_ht_t * default_names = NULL;

#define HIN_DEFAULT_NAME_HASH 101

void httpd_vhost_scope_clean (httpd_vhost_scope_t * scope) {
  if (scope->rproxy_url) hfree (scope->rproxy_url);
  memset (scope, 0, sizeof (*scope));
}

static void httpd_vhost_free (httpd_vhost_t * vhost) {
  hin_ssl_ctx_t * box = vhost->ssl;
  if (box)
    hin_ssl_cert_unref (box);

  httpd_vhost_map_clean (vhost);
  httpd_vhost_scope_clean (&vhost->scope);

  if (vhost->hostname) hfree (vhost->hostname);

  if (vhost->names) {
    basic_ht_free (vhost->names);
  }

  basic_dlist_remove (&hin_g.vhost_list, &vhost->list);

  hfree (vhost);
}

void httpd_vhost_clean_all () {
  hin_struct_lock (&hin_g);
  basic_dlist_t * elem = hin_g.vhost_list.next;
  while (elem) {
    httpd_vhost_t * vhost = basic_dlist_ptr (elem, offsetof (httpd_vhost_t, list));
    elem = elem->next;

    httpd_vhost_free (vhost);
  }
  if (default_names) {
    basic_ht_free (default_names);
    default_names = NULL;
  }
  hin_struct_unlock (&hin_g);
}

HIN_EXPORT httpd_vhost_t * httpd_vhost_get (httpd_vhost_t * vhost, const char * name, int name_len) {
  basic_ht_t * ht = NULL;
  if (vhost && vhost->names) {
    ht = vhost->names;
  } else if (vhost && vhost->parent) {
    return httpd_vhost_get (vhost->parent, name, name_len);
  } else {
    ht = default_names;
  }
  if (ht == NULL) return NULL;

  basic_ht_hash_t h1 = 0, h2 = 0;
  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  basic_ht_pair_t * pair = basic_ht_get_pair (ht, h1, h2);

  if (pair == NULL) {
    if (vhost && vhost->parent) {
      return httpd_vhost_get (vhost->parent, name, name_len);
    } else if (vhost) {
      return httpd_vhost_get (NULL, name, name_len);
    } else {
      return NULL;
    }
  }
  return (void*)pair->d.ptr.value2;
}

HIN_EXPORT int httpd_vhost_set_name (httpd_vhost_t * parent, const char * name, int name_len, httpd_vhost_t * new) {
  basic_ht_t * ht = NULL;
  if (parent) {
    if (parent->names == NULL) {
      parent->names = basic_ht_create (1024, HIN_DEFAULT_NAME_HASH);
    }
    ht = parent->names;
  } else {
    if (default_names == NULL) {
      default_names = basic_ht_create (1024, HIN_DEFAULT_NAME_HASH);
    }
    ht = default_names;
  }

  basic_ht_hash_t h1 = 0, h2 = 0;
  basic_ht_hash (name, name_len, ht->seed, &h1, &h2);
  basic_ht_set_pair (ht, h1, h2, 0, (uintptr_t)new);
  return 1;
}

// create vhost
HIN_EXPORT httpd_vhost_t * httpd_vhost_create (const char * name, httpd_server_t * bind, httpd_vhost_t * parent) {
  if (name == NULL) {
    hin_error ("vhost name missing");
    return NULL;
  }

  httpd_vhost_t * vhost = halloc_z (sizeof (httpd_vhost_t));
  vhost->magic = HIN_VHOST_MAGIC;
  vhost->bind = bind;
  vhost->hostname = strdup (name);
  vhost->debug = hin_g.debug;

  vhost->annoy = &default_annoy;

  httpd_vhost_t * patron = NULL;

  if (bind) {
    if (bind->s.c.parent == NULL) {
      bind->s.c.parent = vhost;
      patron = vhost;
    } else {
      patron = bind->s.c.parent;
    }
  }
  if (default_vhost == NULL || (default_vhost->bind == NULL && bind)) {
    default_vhost = vhost;
  }

  httpd_vhost_set_name (patron, name, strlen (name), vhost);

  basic_dlist_append (&hin_g.vhost_list, &vhost->list);

  return vhost;
}

HIN_EXPORT int httpd_vhost_switch (httpd_client_t * http, httpd_vhost_t * vhost) {
  if (vhost == NULL) return -1;
  http->vhost = vhost;
  http->debug = hin_g.debug | vhost->debug;
  http->debug &= ~vhost->nodebug;
  http->disable = vhost->disable;

  httpd_annoy_t * annoy = vhost->annoy;
  int timeout;
  if (hin_overload_slowdown ()) {
    timeout = annoy->idle_timeout_overload;
  } else if (http->state & HIN_REQ_IDLE) {
    timeout = annoy->idle_timeout_wait;
  } else {
    timeout = annoy->idle_timeout_first;
  }

  httpd_timer_update (http, timeout);

  return 0;
}

int httpd_vhost_request (httpd_client_t * http, const char * name, int len) {
  if (http->hostname) {
    if ((len == (int)strlen (http->hostname)) && strncmp (http->hostname, name, len) == 0)
      return 0;
    return -1;
  }

  httpd_vhost_t * vhost = httpd_vhost_get (http->vhost, name, len);
  if (http->debug & (HNDBG_HTTP|HNDBG_INFO)) {
    hin_debug ("  hostname '%.*s' vhost %s\n", len, name, vhost ? vhost->hostname : "not found");
  }

  if (vhost == NULL) {
    return -1;
  }

  http->hostname = strndup (name, len);
  httpd_vhost_switch (http, vhost);

  return 0;
}

HIN_EXPORT httpd_vhost_t * httpd_vhost_default () {
  if (default_vhost == NULL) {
    //hin_error ("vhost default missing");
    //exit (1);
  }
  return default_vhost;
}

/*
int httpd_vhost_set_default (hin_server_t * bind, httpd_vhost_t * new) {
  // TODO make set default vhost function
  // needs to move old vhosts to new hashtable
  return 0;
}*/




