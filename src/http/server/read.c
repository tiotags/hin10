
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>

#include "hin.h"
#include "hextra.h"
#include "http/http.h"
#include "http/server.h"
#include "http/vhost.h"

#include "httpd_internal.h"

int httpd_client_read_callback (hin_buffer_t * buffer, int received);

int httpd_client_reread (httpd_client_t * http) {
  hin_buffer_t * buffer = http->read_buffer;

  if (http->state & HIN_REQ_STOPPING) return 0;

  hin_lines_reread (buffer);
  return 0;
}

static int httpd_client_post_handle_cb (hin_pipe_t * pipe) {
  if (pipe->debug & HNDBG_POST)
    hin_debug ("httpd %d post done %d\n", pipe->in.fd, pipe->out.fd);

  httpd_client_t * http = (httpd_client_t*)pipe->parent;

  http->state &= ~HIN_REQ_POST;
  if (http->state & HIN_REQ_DATA) return 0;
  return httpd_client_finish_request (http);
}

static int httpd_client_post_handle (httpd_client_t * http, string_t * source) {
  httpd_client_post_t * post = http->post;
  int consume = source->len;
  off_t sz = post->sz;
  if (consume > sz) consume = sz;

  if (http->state & HIN_REQ_ERROR) return consume;
  http->state |= HIN_REQ_POST;

  hin_pipe_t * pipe = calloc (1, sizeof (*pipe));
  hin_pipe_init (pipe);
  pipe->in.fd = http->c.sockfd;
  pipe->in.flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  pipe->in.ssl = http->c.ssl;
  pipe->in.pos = 0;
  // out fd, flags and post set in the backend handler
  pipe->parent = http;
  pipe->finish_callback = httpd_client_post_handle_cb;
  pipe->debug = http->debug;
  post->in_pipe = pipe;

  if (http->peer_flags & HIN_HTTP_CHUNKED_UPLOAD) {
    httpd_pipe_upload_chunked (http, pipe);
  } else if (sz) {
    pipe->in.flags |= HIN_COUNT;
    pipe->sz = sz;
  }
  if (consume) {
    hin_buffer_t * buf1 = hin_buffer_create_from_data (pipe, source->ptr, consume);
    hin_pipe_write_process (pipe, buf1, HIN_PIPE_ALL);
  }

  return consume;
}

int httpd_client_read_callback (hin_buffer_t * buffer, int received) {
  string_t source1, * source = &source1;
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  source->ptr = lines->base;
  source->len = lines->count;

  httpd_client_t * http = (httpd_client_t*)buffer->parent;

  if (source->len <= 0) return 0;

  if (http->state & HIN_REQ_IDLE) {
    // change from idle to non idle
    httpd_client_set_idle (http, -1);
  }

  if (source->len >= HIN_HTTPD_MAX_HEADER_SIZE) {
    httpd_respond_fatal (http, 413, NULL);
    return -1;
  }

  int used = httpd_parse_req (http, source);
  if (used <= 0) {
    httpd_vhost_t * vhost = http->vhost;
    httpd_annoy_t * annoy = vhost->annoy;
    if (used == 0 && (received < annoy->small_read_size)) {
      used = httpd_increase_annoy (http, annoy->small_read_penalty);
    }
    return used;
  }

  httpd_client_post_t * post = http->post;

  http->peer_flags &= ~http->disable;
  http->state &= ~HIN_REQ_HEADERS;

  http->headers.ptr = lines->base;
  http->headers.len = used;

  // time serving files is not counted
  httpd_timer_update (http, -1);

  int new = 0;
  if (post) {
    new = httpd_client_post_handle (http, source);
    used += new;
    http->headers.len += new;
  } else if (http->method == HIN_METHOD_OPTIONS) {
    httpd_append_header (http, "Allow", "GET,POST,HEAD,OPTIONS");
    httpd_respond_text (http, 204, "");
    return used;
  }

  // run server processing
  httpd_server_t * server = http->c.parent;
  if (server->process_callback)
    server->process_callback (http);

  http->peer_flags &= ~http->disable;

  if (http->state & HIN_REQ_END) {
    httpd_error (http, 0, "forced shutdown");
    return -1;
  } else if (http->peer_flags & http->disable & HIN_HTTP_CHUNKED_UPLOAD) {
    httpd_error (http, 411, "chunked upload disabled");
    return -1;
  } else if ((http->state & (HIN_REQ_DATA)) == 0) {
    httpd_error (http, 500, "missing request");
    return -1;
  } else if (http->state & (HIN_REQ_ERROR)) {
  }

  return used;
}


