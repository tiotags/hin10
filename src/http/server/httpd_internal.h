
#ifndef HTTP_SERVER_INTERNAL_H
#define HTTP_SERVER_INTERNAL_H

#include "http/http.h"

int httpd_client_read_callback (hin_buffer_t * buffer, int received);

int httpd_client_reread (httpd_client_t * http);

int httpd_parse_req (httpd_client_t * http, string_t * source);

int httpd_write_common_headers (httpd_client_t * http, hin_buffer_t * buf);

void httpd_vhost_clean_all ();

#include "http/vhost.h"
void httpd_vhost_scope_clean (httpd_vhost_scope_t * scope);
void httpd_vhost_map_clean (httpd_vhost_t * vhost);

int httpd_file_is_multirange (httpd_client_t * http);

time_t hin_date_str_to_time (const char * str);

int httpd_pipe_set_http11_response_options (httpd_client_t * http, hin_pipe_t * pipe);

int httpd_compress_setup (httpd_client_t * http);
int httpd_compress_clean (httpd_client_t * http);

int httpd_request_chunked (httpd_client_t * http);
int httpd_pipe_upload_chunked (httpd_client_t * http, hin_pipe_t * pipe);

int httpd_pipe_set_idle_protections (httpd_client_t * http, hin_pipe_t * pipe);

// idle
void httpd_timer_update (httpd_client_t * http, int msec);
void httpd_client_set_idle (httpd_client_t * http, int is);
void httpd_flush_idle_connections ();

#endif

