
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "hextra.h"
#include "http/http.h"
#include "conf.h"

#include "http_internal.h"

static int hget_parse_headers_line (hget_client_t * http, string_t * line) {
  string_t param;
  if (matchi_string_equal (line, "Content-Length: (%d+)", &param) > 0) {
    http->sz = atoi (param.ptr);
    if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
      hin_debug ("  content length is %lld\n", (long long)http->sz);
  } else if (matchi_string (line, "Transfer-Encoding:%s*") > 0) {
    if (matchi_string (line, "chunked") > 0) {
      if (http->debug & (HNDBG_HTTP|HNDBG_HTTP_FILTER))
        hin_debug ("  transfer encoding is chunked\n");
      http->flags |= HIN_HTTP_CHUNKED;
    } else if (matchi_string (line, "identity") > 0) {
    } else {
      hin_error ("http %d transport encoding '%.*s' not supported", http->c.sockfd, (int)line->len, line->ptr);
      return -1;
    }
  //} else if (matchi_string (line, "Cache-Control:") > 0) {
  //  httpd_parse_cache_str (line->ptr, line->len, &http->cache_flags, &http->cache);
  }
  return 1;
}

static int hget_read_headers_callback (hin_buffer_t * buffer, int received) {
  hget_client_t * http = (hget_client_t*)buffer->parent;
  hin_lines_t * lines = (hin_lines_t*)&buffer->buffer;
  string_t data, *source;
  data.ptr = lines->base;
  data.len = lines->count;
  source = &data;

  if (http->io_state & HIN_REQ_IDLE) return 0;

  string_t line, param1;
  string_t orig = *source;

  while (1) {
    if (hin_find_line (source, &line) == 0) return 0;
    if (line.len == 0) break;
    if (source->len <= 0) return 0;
  }

  *source = orig;

  if (hin_find_line (source, &line) == 0 || match_string (&line, "HTTP/1.%d ([%d]+) %w+", &param1) <= 0) {
    hget_error (http, 0, "http parsing error");
    // close connection return error
    return -1;
  }

  http->status = atoi (param1.ptr);

  if (http->debug & HNDBG_RW)
    hin_debug ("http %d headers status %d\n", http->c.sockfd, http->status);

  while (hin_find_line (source, &line)) {

    if (http->debug & HNDBG_RW)
      hin_debug (" %d '%.*s'\n", (int)line.len, (int)line.len, line.ptr);

    if (line.len == 0) break;
    if (hget_parse_headers_line (http, &line) < 0) {
      *source = orig;
      return -1;
    }
  }

  hin_pipe_t * pipe = hget_start_pipe (http, source);

  hin_pipe_start (pipe);

  return (uintptr_t)source->ptr - (uintptr_t)orig.ptr;
}

static int hget_client_restart (hget_client_t * old, httpd_client_t * parent) {
  if (parent == NULL) return 0;

  hget_client_t * http = hget_find_connection (old->uri.all.ptr);

  http->c.parent = parent;
  http->debug = parent->debug;

  http->read_callback = old->read_callback;
  http->state_callback = old->state_callback;

  hget_client_start (http);

  old->c.parent = NULL;
  old->read_callback = NULL;
  old->state_callback = NULL;

  return 0;
}

static int hget_close_headers_callback (hin_buffer_t * buf, int ret) {
  hget_client_t * http = buf->parent;
  httpd_client_t * parent = http->c.parent;

  if (ret != 0)
    hin_error ("headers closed %d bc %d: %s", buf->fd, ret, strerror (-ret));

  if (http->io_state & (HIN_REQ_HEADERS|HIN_REQ_DATA)) {
    //if (ret != 0)
    //  hget_switch_state (http, HIN_HTTP_STATE_HEADERS_FAILED, ret);

    if ((http->io_state & HIN_REQ_IDLE) == 0)
      hget_client_restart (http, parent);
  }

  hget_client_shutdown (http);

  return 0;
}

static int hget_send_headers_callback (hin_buffer_t * buf, int ret) {
  hget_client_t * http = buf->parent;
  http->io_state &= ~HIN_REQ_HEADERS;

  if (http->io_state & HIN_REQ_STOPPING) {
    hget_client_shutdown (http);
    return 1;
  }

  if (ret <= 0) {
    hin_error ("http %d send: %s", buf->fd, strerror (-ret));
    hget_switch_state (http, HIN_HTTP_STATE_HEADERS_FAILED, ret);
    hget_client_shutdown (http);
  } else {
    hget_switch_state (http, HIN_HTTP_STATE_HEADERS_SENT, ret);
  }

  return 1;
}

int hget_send_headers (hget_client_t * http, hin_buffer_t * received) {
  if (http->debug & HNDBG_HTTP)
    hin_debug ("http %d '%s' begin\n", http->c.sockfd, http->uri.all.ptr);

  hin_lines_t * lines = (hin_lines_t*)&http->read_buffer->buffer;
  lines->read_callback = hget_read_headers_callback;
  lines->close_callback = hget_close_headers_callback;

  http->io_state |= HIN_REQ_HEADERS|HIN_REQ_DATA;;

  hin_buffer_t * buf = hin_buffer_alloc (HIN_BUFSZ);
  buf->flags |= HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = hget_send_headers_callback;
  buf->count = 0;
  buf->parent = http;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  if (http->upload) {
    http->io_state |= HIN_REQ_POST;
    if (http->method == 0 || http->method == HIN_METHOD_GET) {
      http->method = HIN_METHOD_POST;
    }
  }

  char * path = http->uri.path.ptr;
  char * path_max = path + http->uri.path.len;
  if (http->uri.query.len > 0) {
    path_max = http->uri.query.ptr + http->uri.query.len;
  }

  const char * method = hin_http_method_name (http->method);

  hin_header (buf, "%s %.*s HTTP/1.1\r\n", method, path_max - path, path);
  if (http->hostname) {
    hin_header (buf, "Host: %s\r\n", http->hostname);
  } else if (http->uri.port.len > 0) {
    hin_header (buf, "Host: %.*s:%.*s\r\n", http->uri.host.len, http->uri.host.ptr, http->uri.port.len, http->uri.port.ptr);
  } else {
    hin_header (buf, "Host: %.*s\r\n", http->uri.host.len, http->uri.host.ptr);
  }
  if (http->flags & HIN_HTTP_KEEPALIVE) {
    hin_header (buf, "Connection: keep-alive\r\n");
  } else {
    hin_header (buf, "Connection: close\r\n");
  }

  if (http->upload) {
    hin_http_post_t * post = http->upload;
    hin_pipe_t * pipe = post->pipe;
    pipe->out.fd = http->c.sockfd;
    pipe->out.flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
    pipe->out.ssl = http->c.ssl;

    if (post->content_type) {
      hin_header (buf, "Content-Type: %s\r\n", post->content_type);
    }
    if ((pipe->in.flags & HIN_COUNT)) {
      hin_header (buf, "Content-Length: %ld\r\n", pipe->sz);
    } else {
      // TODO
      hin_header (buf, "Transfer-Encoding: chunked\r\n");
      hin_error ("upload doesn't support chunked");
    }
  }

  if (hget_switch_state (http, HIN_HTTP_STATE_SEND, (uintptr_t)buf) == 0)
    hin_header (buf, "\r\n");

  if (http->debug & HNDBG_RW) hin_debug ("http %d request '\n%.*s'\n", http->c.sockfd, buf->count, buf->ptr);

  if (hin_request_write (buf) < 0) {
    hin_weird_error (54686786);
    return -1;
  }

  if (http->upload) {
    hin_pipe_start (http->upload->pipe);
  }

  return 0;
}

