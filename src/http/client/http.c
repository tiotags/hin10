
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"
#include "hextra.h"
#include "hmaster.h"

#include "http_internal.h"

int hget_switch_state (hget_client_t * http, int state, uintptr_t data) {
  string_t url = http->uri.all;
  switch (state) {
  case HIN_HTTP_STATE_CONNECTED:
    hget_send_headers (http, (void*)data);
  break;
  case HIN_HTTP_STATE_SSL_FAILED: // fall-through
  case HIN_HTTP_STATE_CONNECTION_FAILED:
    if (http->debug & (HNDBG_HTTP|HNDBG_RW_ERROR))
      hin_error ("%.*s connection failed: %s", (int)url.len, url.ptr, strerror (-data));
  break;
  case HIN_HTTP_STATE_HEADERS_FAILED:
    if (http->debug & (HNDBG_HTTP|HNDBG_RW_ERROR))
      hin_error ("%.*s failed to download headers", (int)url.len, url.ptr);
  break;
  case HIN_HTTP_STATE_ERROR:
    if (http->debug & (HNDBG_HTTP|HNDBG_RW_ERROR))
      hin_error ("%.*s generic error", (int)url.len, url.ptr);
  break;
  default:
    if (http->debug & HNDBG_HTTP)
      hin_debug ("http %d state is %d\n", http->c.sockfd, state);
  break;
  }
  if (http->state_callback) {
    http->state_callback (http, state, data);
  }
  return 0;
}

static void hget_client_unlink_wrapped (hget_client_t * http) {

  http->io_state |= HIN_REQ_END;
  http->io_state &= ~(HIN_REQ_HEADERS|HIN_REQ_POST);
  http->flags &= ~HIN_HTTP_KEEPALIVE;

  hget_client_unlink (http);
}

static int hget_connected (hin_buffer_t * buffer, int ret) {
  hget_client_t * http = (hget_client_t*)buffer->parent;

  http->c.sockfd = ret;

  if (ret < 0) {
    hget_switch_state (http, HIN_HTTP_STATE_CONNECTION_FAILED, ret);
    hget_client_unlink_wrapped (http);
    return 0;
  }

  if (http->uri.https) {
    if (hin_ssl_connect_init (&http->c) < 0) {
      hget_switch_state (http, HIN_HTTP_STATE_SSL_FAILED, -EPROTO);
      hget_client_unlink_wrapped (http);
      return 0;
    }
  }

  if (http->read_buffer == NULL) {
    hin_buffer_t * buf = hin_lines_create_raw (HIN_BUFSZ);
    buf->flags |= HIN_SOCKET | (http->c.flags & HIN_SSL);
    buf->fd = http->c.sockfd;
    buf->parent = http;
    buf->ssl = http->c.ssl;
    buf->debug = http->debug;

    http->read_buffer = buf;
  }

  if (hin_lines_request (http->read_buffer, 0)) {
    hin_weird_error (943547909);
    return 0;
  }

  hget_switch_state (http, HIN_HTTP_STATE_CONNECTED, (uintptr_t)http->read_buffer);
  return 0;
}

HIN_EXPORT int hget_client_start (hget_client_t * http) {
  if (http->io_state & HIN_REQ_ERROR) {
    hget_switch_state (http, HIN_HTTP_STATE_CONNECTION_FAILED, 0);
    return 0;
  }

  if (http->c.sockfd >= 0) {
    hget_switch_state (http, HIN_HTTP_STATE_CONNECTED, (uintptr_t)http->read_buffer);

    hin_buffer_t * buf = http->read_buffer;
    buf->debug = http->debug;

    if (http->debug & HNDBG_HTTP)
      hin_debug ("http %d reuse %s:%s\n", http->c.sockfd, http->host, http->port);

    if (hin_lines_request (buf, 0)) {
      hin_weird_error (953909);
      return 0;
    }

    return 0;
  }

  http->c.sockfd = -1;
  http->c.magic = HIN_CONNECT_MAGIC;
  http->c.ai_addrlen = sizeof (http->c.ai_addr);

  hin_connect (http->host, http->port, &hget_connected, http, &http->c.ai_addr, &http->c.ai_addrlen);

  return 0;
}



