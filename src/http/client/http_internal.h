#ifndef HIN_HTTP_INTERNAL_H
#define HIN_HTTP_INTERNAL_H

#include "hin.h"
#include "http/http.h"
#include "http/client.h"

int hget_switch_state (hget_client_t * http, int state, uintptr_t data);

int hget_client_unlink (hget_client_t * http);

int hget_error (hget_client_t * http, int status, const char * fmt, ...);

hin_pipe_t * hget_start_pipe (hget_client_t * http, string_t * source);

int hget_str_equal (const char * a, string_t * b);

#endif

