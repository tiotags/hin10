
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"

#include "http_internal.h"

int hget_error (hget_client_t * http, int status, const char * fmt, ...) {
  va_list ap;
  va_start (ap, fmt);

  char buffer[1024];
  vsnprintf (buffer, sizeof (buffer), fmt, ap);

  hin_error ("http %d %d %s", http->c.sockfd, status, buffer);

  va_end (ap);
  return 0;
}

int hget_str_equal (const char * a, string_t * b) {
  const char * ptr = b->ptr;
  const char * max = b->ptr + b->len;
  while (1) {
    if (ptr >= max) break;
    if (*a != *ptr) return 0;
    a++; ptr++;
  }
  if (*a == '\0') return 1;
  return 0;
}


