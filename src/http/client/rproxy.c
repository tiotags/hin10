
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"
#include "http/client.h"
#include "hextra.h"
#include "conf.h"

#include "http/server/httpd_internal.h"

static int httpd_rproxy_pipe_error_callback (hin_pipe_t * pipe, int err) {
  hin_error ("http %d rproxy", pipe->out.fd);
  hget_client_t * http = pipe->parent;
  httpd_client_shutdown (http->c.parent);
  return 0;
}

static int httpd_rproxy_receive_headers (hget_client_t * http, hin_pipe_t * pipe) {
  httpd_client_t * parent = http->c.parent;
  pipe->out.flags &= ~(HIN_FILE | HIN_OFFSETS);

  //parent->count = http->sz;
  parent->peer_flags |= http->flags & HIN_HTTP_CHUNKED;

  httpd_pipe_set_http11_response_options (parent, pipe);
  pipe->out_error_callback = httpd_rproxy_pipe_error_callback;
  pipe->parent = http;
  //pipe->parent1 = parent;

  if (http->method == HIN_METHOD_HEAD) {
    pipe->in.flags |= HIN_COUNT;
    pipe->sz = 0;
  } else if ((http->flags & HIN_HTTP_CHUNKED)) {
    pipe->in.flags &= ~HIN_COUNT;
    pipe->sz = 0;
  } else {
    pipe->in.flags |= HIN_COUNT;
    pipe->sz = http->sz;
  }

  hin_buffer_t * header_buf = http->read_buffer;
  hin_lines_t * lines = (hin_lines_t*)&header_buf->buffer;
  string_t source, line, param;
  source.ptr = lines->base;
  source.len = lines->count;

  hin_buffer_t * buf = hin_buffer_alloc (HIN_BUFSZ);
  buf->fd = parent->c.sockfd;
  buf->flags |= parent->c.flags;
  buf->ssl = parent->c.ssl;
  buf->count = 0;
  buf->parent = pipe;
  buf->debug = parent->debug;

  hin_find_line (&source, &line);
  if (matchi_string (&line, "HTTP/1.%d (%d+)", &param) <= 0) {
    hin_weird_error (43534645);
  }
  parent->status = atoi (param.ptr);

  hin_header (buf, "HTTP/1.%d %d %s\r\n", parent->peer_flags & HIN_HTTP_VER10 ? 0 : 1, parent->status, http_status_name (parent->status));
  httpd_write_common_headers (parent, buf);
  if (http->sz && (parent->peer_flags & HIN_HTTP_CHUNKED) == 0)
    hin_header (buf, "Content-Length: %ld\r\n", http->sz);

  while (1) {
    if (hin_find_line (&source, &line) == 0) { return 0; }
    if (line.len == 0) break;
    if (matchi_string (&line, "Content-Length:") > 0) {
    } else if (matchi_string (&line, "Transfer-Encoding:") > 0) {
    } else if (matchi_string (&line, "Content-Encoding:") > 0) {
    } else if (matchi_string (&line, "Connection:") > 0) {
    } else if (matchi_string (&line, "Cache-Control:") > 0) {
    } else if (matchi_string (&line, "Server:") > 0) {
    } else if (matchi_string (&line, "Date:") > 0) {
    } else if (matchi_string (&line, "Accept:") > 0) {
    } else if (matchi_string (&line, "Accept-Encoding:") > 0) {
    } else if (matchi_string (&line, "Accept-Ranges:") > 0) {
    } else if (matchi_string (&line, "Keep-Alive:") > 0) {
    } else {
      hin_header (buf, "%.*s\r\n", line.len, line.ptr);
    }
  }

  hin_header (buf, "\r\n");

  if (http->debug & HNDBG_RW)
    hin_debug ("httpd %d proxy response %d '\n%.*s'\n", parent->c.sockfd, buf->count, buf->count, buf->ptr);

  hin_pipe_prepend_raw (pipe, buf);

  return 0;
}

static int httpd_rproxy_finish (hget_client_t * http, hin_pipe_t * pipe) {
  httpd_client_t * parent = http->c.parent;
  http->save_fd = 0;
  parent->state &= ~(HIN_REQ_PROXY | HIN_REQ_DATA);

  if (pipe) {
    httpd_client_finish_output (parent, pipe);
  } else {
    httpd_client_finish_request (parent);
  }
  return 0;
}

static int httpd_rproxy_error (hget_client_t * http, int error_code, hin_pipe_t * pipe) {
  httpd_client_t * parent = http->c.parent;
  http->save_fd = 0;
  parent->state &= ~(HIN_REQ_PROXY | HIN_REQ_DATA);

  httpd_error (parent, error_code, "rproxy error");

  return 0;
}

static int httpd_rproxy_send_headers (hget_client_t * http, hin_buffer_t * buf) {
  httpd_client_t * parent = http->c.parent;
  string_t source, line, orig;
  source = parent->headers;

  off_t sz = 0;
  int https = 0;

  if (parent->c.flags & HIN_SSL) {
    https = 1;
  }

  hin_find_line (&source, &line);
  while (1) {
    if (hin_find_line (&source, &line) == 0) { return 0; }
    if (line.len == 0) break;
    orig = line;
    if (matchi_string (&line, "Host:%s*") > 0) {
      hin_header (buf, "X-Forwarded-Host: %.*s\r\n", line.len, line.ptr);
      if (1) {
        http->hostname = strndup (line.ptr, line.len);
      }
    } else if (matchi_string (&line, "Content-Length:") > 0) {
      match_string (&line, "%s*");
      sz = atoi (line.ptr);
    } else if (matchi_string (&line, "Connection:") > 0) {
    } else if (matchi_string (&line, "Accept-Encoding:") > 0) {
    } else if (matchi_string (&line, "Transfer-Encoding:") > 0) {
    } else if (matchi_string (&line, "Content-Encoding:") > 0) {
    // for the moment ignore these
    } else if (matchi_string (&line, "Forwarded:") > 0) {
    } else if (matchi_string (&line, "X-Forwarded-For:") > 0) {
    } else if (matchi_string (&line, "X-Forwarded-Host:") > 0) {
    // other hop-by-hop headers
    } else if (matchi_string (&line, "Keep-Alive:") > 0) {
    } else if (matchi_string (&line, "Proxy-Authenticate:") > 0) {
    } else if (matchi_string (&line, "Proxy-Authorization:") > 0) {
    } else if (matchi_string (&line, "TE:") > 0) {
    } else if (matchi_string (&line, "Trailers:") > 0) {
    } else if (matchi_string (&line, "Upgrade:") > 0) {
    } else {
      hin_header (buf, "%.*s\r\n", orig.len, orig.ptr);
    }
  }

  hin_header (buf, "X-Forwarded-Proto: %s\r\n", https ? "https" : "http");

  char buf1[1080];
  hin_client_addr (buf1, sizeof buf1, &http->c.ai_addr, sizeof (http->c.ai_addr), HIN_ADDR_ONLY_IP);
  hin_header (buf, "X-Forwarded-For: %s\r\n", buf1);

  if (sz > 0) {
    hin_header (buf, "Content-Length: %ld\r\n", sz);
  }

  return 0;
}

static int httpd_rproxy_state_callback (hget_client_t * http, uint32_t state, uintptr_t data) {
  switch (state) {
  case HIN_HTTP_STATE_CONNECTED:
  break;
  case HIN_HTTP_STATE_SEND:
    return httpd_rproxy_send_headers (http, (hin_buffer_t*)data);
  break;
  case HIN_HTTP_STATE_HEADERS:
    return httpd_rproxy_receive_headers (http, (hin_pipe_t*)data);
  break;
  case HIN_HTTP_STATE_HEADERS_SENT: {
    httpd_client_t * parent = http->c.parent;
    if (parent->post) {
      httpd_client_post_t * post = parent->post;
      hin_pipe_t * pipe = post->in_pipe;
      pipe->out.fd = http->c.sockfd;
      pipe->out.flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
      pipe->out.ssl = http->c.ssl;
      hin_pipe_start (pipe);
    }
  break; }
  case HIN_HTTP_STATE_FINISH:
    return httpd_rproxy_finish (http, (hin_pipe_t*)data);
  break;
  case HIN_HTTP_STATE_CONNECTION_FAILED:// fall-through
  case HIN_HTTP_STATE_SSL_FAILED:	// fall-through
  case HIN_HTTP_STATE_HEADERS_FAILED:	// fall-through
  case HIN_HTTP_STATE_ERROR:		// fall-through
    return httpd_rproxy_error (http, 502, NULL);
  break;
  default:
    hin_error ("http %d rproxy unhandled state %x", http->c.sockfd, state);
    return httpd_rproxy_error (http, 502, NULL);
  break;
  }
  return 0;
}

hget_client_t * httpd_rproxy_request (httpd_client_t * parent, hget_client_t * http, const char * url) {
  if (parent->state & HIN_REQ_DATA) {
    hin_error ("rproxy on a bound client");
    return NULL;
  }
  parent->state |= HIN_REQ_DATA | HIN_REQ_PROXY;

  if (http == NULL) {
    http = hget_find_connection (url);
  }

  http->c.parent = parent;
  http->debug = parent->debug;
  http->method = parent->method;

  http->state_callback = httpd_rproxy_state_callback;

  hget_client_start (http);

  return http;
}


