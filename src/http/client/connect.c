
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <unistd.h>

#include "hin.h"
#include "hmaster.h"
#include "http/http.h"

#include "netcode/hin_internal.h"
#include "http_internal.h"

void hget_client_clean (hget_client_t * http) {
  if (http->debug & HNDBG_MEMORY) hin_debug ("http %d clean\n", http->c.sockfd);
  if (http->uri.all.ptr) {
    hfree ((void*)http->uri.all.ptr);
    http->uri.all.ptr = NULL;
  }
  if (http->save_fd) { // TODO should it clean save_fd ?
    if (http->debug & HNDBG_SYSCALL) hin_debug ("  close save_fd %d\n", http->save_fd);
    close (http->save_fd);
    http->save_fd = 0;
  }
  if (http->progress) {
    hfree (http->progress);
    http->progress = NULL;
  }
  if (http->upload) {
    hfree (http->upload);
    http->upload = NULL;
  }
  if (http->hostname) {
    hfree (http->hostname);
    http->hostname = NULL;
  }
  http->c.parent = NULL;
  http->flags = 0;
  http->method = http->status = 0;
  http->sz = http->count = 0;
  http->cache_flags = 0;
  http->cache = 0;

  http->io_state &= HIN_REQ_HEADERS|HIN_REQ_STOPPING;
}

int hget_connection_release (hget_client_t * http) {
  if ((http->flags & HIN_HTTP_KEEPALIVE) == 0) {
    hget_client_shutdown (http);
    return 0;
  }

  if (http->debug & HNDBG_HTTP)
    hin_debug ("http %d idled\n", http->c.sockfd);

  hget_client_clean (http);
  http->c.parent = NULL;

  basic_dlist_remove (&hin_t.connection_list, &http->c.list);
  hin_struct_lock (&hin_g);
  basic_dlist_append (&hin_g.connection_idle, &http->c.list);
  http->io_state |= HIN_REQ_IDLE;
  hin_struct_unlock (&hin_g);

  if (hin_lines_request (http->read_buffer, 0)) {
    hin_weird_error (943547909);
    return 0;
  }

  return 0;
}

int hget_client_unlink (hget_client_t * http) {
  if ((http->io_state & HIN_REQ_END) == 0) { return 0; }
  if ((http->io_state & (HIN_REQ_HEADERS|HIN_REQ_POST))) { return 0; }
  if (http->read_buffer && (http->read_buffer->flags & HIN_ACTIVE)) { return 0; }

  if (http->debug & HNDBG_HTTP)
    hin_debug ("http %d unlink\n", http->c.sockfd);

  hin_threadsafe_dec (&hin_g.num_connection);
  hin_threadsafe_dec (&hin_g.num_fds);
  // TODO message about fds

  basic_dlist_remove (&hin_t.connection_list, &http->c.list);

  hget_client_clean (http);
  if (http->host) hfree (http->host);
  if (http->port) hfree (http->port);
  //http->host = http->port = NULL; // useless, freed moments later

  if (http->read_buffer) {
    hin_buffer_stop_clean (http->read_buffer);
    //http->read_buffer = NULL;
  }

  if (http->c.flags & HIN_SSL)
    hin_client_ssl_cleanup (&http->c);

  hfree (http);
  hin_check_alive ();

  return 1;
}

static int hget_client_close_callback (hin_buffer_t * buffer, int ret) {
  hget_client_t * http = (hget_client_t*)buffer->parent;
  if (ret < 0) {
    hin_error ("http %d client close callback error: %s", http->c.sockfd, strerror (-ret));
    return -1;
  }
  hin_buffer_clean (buffer);
  http->io_state |= HIN_REQ_END;
  hget_client_unlink (http);
  return 0;
}

int hget_client_shutdown (hget_client_t * http) {
  if (hget_client_unlink (http)) return 0;

  if (http->io_state & HIN_REQ_STOPPING) return 0;
  http->io_state |= HIN_REQ_STOPPING;

  if (http->io_state & HIN_REQ_IDLE) {
    hin_struct_lock (&hin_g);
    basic_dlist_remove (&hin_g.connection_idle, &http->c.list);
    http->io_state &= ~HIN_REQ_IDLE;
    hin_struct_unlock (&hin_g);
    basic_dlist_append (&hin_t.connection_list, &http->c.list);
  }

  if (http->debug & HNDBG_HTTP) hin_debug ("http %d shutdown\n", http->c.sockfd);

  hin_buffer_t * buf = hin_buffer_alloc (0);
  buf->flags |= HIN_SOCKET | (http->c.flags & HIN_SSL);
  buf->fd = http->c.sockfd;
  buf->callback = hget_client_close_callback;
  buf->parent = &http->c;
  buf->ssl = http->c.ssl;
  buf->debug = http->debug;

  if (hin_request_close (buf) < 0) {
    buf->flags |= HIN_SYNC;
    hin_request_close (buf);
  }

  return 0;
}

HIN_EXPORT int hget_client_finish_request (hget_client_t * http) {
  if (http->io_state & (HIN_REQ_DATA|HIN_REQ_POST)) return 0;

  if (http->debug & HNDBG_HTTP) hin_debug ("http %d request done\n", http->c.sockfd);

  hget_switch_state (http, HIN_HTTP_STATE_FINISH, 0);

  hget_connection_release (http);

  return 0;
}

HIN_EXPORT hget_client_t * hget_find_connection (const char * url1) {
  hget_client_t * http = NULL;

  hin_uri_t info;
  char * url = strdup (url1);
  if (hin_parse_uri (url, 0, &info) < 0) {
    hin_error ("can't parse uri '%s'", url);
    hfree (url);
    http = halloc_z (sizeof (*http));
    http->io_state |= HIN_REQ_ERROR;
    return http;
  }

  hin_struct_lock (&hin_g);
  basic_dlist_t * elem = hin_g.connection_idle.next;
  while (elem) {
    hget_client_t * new = basic_dlist_ptr (elem, offsetof (hin_client_t, list));
    elem = elem->next;
    break;

    if (new->io_state & HIN_REQ_HEADERS) continue;
    if (hget_str_equal (new->host, &info.host) == 0) continue;
    if (hget_str_equal (new->port, &info.port) == 0) continue;
    http = new;

    basic_dlist_remove (&hin_g.connection_idle, &http->c.list);
    http->io_state &= ~HIN_REQ_IDLE;

    break;
  }
  hin_struct_unlock (&hin_g);

  if (http == NULL) {
    http = halloc_z (sizeof (*http));
    http->host = strndup (info.host.ptr, info.host.len);
    if (info.port.len > 0) {
      http->port = strndup (info.port.ptr, info.port.len);
    } else if (info.https) {
      http->port = strdup ("443");
    } else {
      http->port = strdup ("80");
    }
    http->c.sockfd = -1;
  }

  basic_dlist_append (&hin_t.connection_list, &http->c.list);

  http->uri = info;

  if (hin_g.flags & HIN_HTTP_REUSE_CONNECTION) {
    // TODO fix this
    http->flags |= HIN_HTTP_KEEPALIVE;
  }

  return http;
}

