#ifndef HIN_HTTP_UTILS_INTERNAL_H
#define HIN_HTTP_UTILS_INTERNAL_H

#include "hin.h"
#include "http/http.h"

int hin_pipe_decode_chunked (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush);

#endif

