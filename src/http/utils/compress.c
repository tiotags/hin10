
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <zlib.h>

#include "hin.h"
#include "hextra.h"
#include "http/http.h"

#include "../server/httpd_internal.h"

typedef struct httpd_compress_data_struct {
  z_stream z;
} httpd_compress_data_t;

int hin_pipe_copy_deflate (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush) {
  hin_client_t * c = (hin_client_t*)pipe->parent;
  uint32_t peer_flags = 0;
  httpd_client_t * http = (httpd_client_t*)pipe->parent;
  if (c->magic == HIN_CONNECT_MAGIC) {
    http = c->parent;
  }
  httpd_compress_data_t * cdata = http->compress_data;
  z_stream *z = &cdata->z;
  peer_flags = http->peer_flags;

  int have;
  z->avail_in = num;
  z->next_in = (Bytef *)buffer->ptr;
  buffer->count = num;

  if (pipe->debug & HNDBG_HTTP_FILTER)
    hin_debug ("http(d) %d deflate num %d flush %d\n", c->sockfd, num, flush);
  char numbuf[10]; // size of max nr (7 bytes) + crlf + \0

  do {
    hin_buffer_t * new = hin_pipe_get_buffer (pipe, HIN_BUFSZ);
    new->count = 0;
    if (peer_flags & HIN_HTTP_CHUNKED) {
      new->sz -= (sizeof (numbuf) + 8); // crlf + 0+crlfcrlf + \0
      new->count = sizeof (numbuf);
    }
    z->avail_out = new->sz;
    z->next_out = (Bytef *)&new->buffer[new->count];
    deflate (z, flush ? Z_FINISH : Z_NO_FLUSH);
    have = new->sz - z->avail_out;

    if (have > 0) {
      new->count += have;
      new->fd = pipe->out.fd;

      if (peer_flags & HIN_HTTP_CHUNKED) {
        new->sz += sizeof (numbuf) + 8;
        hin_header (new, "\r\n");
        if (flush && (z->avail_out != 0)) {
          hin_header (new, "0\r\n\r\n");
        }

        int num = snprintf (numbuf, sizeof (numbuf), "%x\r\n", have);
        if (num < 0 || num >= (int)sizeof (numbuf)) {
          hin_weird_error (46121234);
        }
        int offset = sizeof (numbuf) - num;
        char * ptr = new->buffer + offset;
        memcpy (ptr, numbuf, num);
        new->ptr = ptr;
        new->count -= offset;
      }
      if (pipe->debug & HNDBG_HTTP_FILTER)
        hin_debug ("  deflate write %d total %d %s\n", have, new->count, flush ? "flush" : "cont");
      hin_pipe_append_raw (pipe, new);
    } else {
      hin_buffer_clean (new);
    }
  } while (z->avail_out == 0);
  return 1;
}

int httpd_compress_clean (httpd_client_t * http) {
  httpd_compress_data_t * data = http->compress_data;
  if (data == NULL) return 0;

  int ret = deflateEnd (&data->z);
  if (ret != Z_OK) {
  }
  http->compress_data = NULL;
  free (data);
  return 0;
}

int httpd_compress_setup (httpd_client_t * http) {
  if ((http->peer_flags & HIN_HTTP_COMPRESS) == 0) {
    return 0;
  }
  httpd_compress_data_t * data = http->compress_data;
  if (data == NULL) {
    data = halloc_z (sizeof (httpd_compress_data_t));
    http->compress_data = data;
    data->z.zalloc = Z_NULL;
    data->z.zfree = Z_NULL;
    data->z.opaque = Z_NULL;
  }
  int windowsBits = 15;
  if (http->peer_flags & HIN_HTTP_DEFLATE) {
    http->peer_flags = (http->peer_flags & ~HIN_HTTP_COMPRESS) | HIN_HTTP_DEFLATE;
  } else if (http->peer_flags & HIN_HTTP_GZIP) {
    http->peer_flags = (http->peer_flags & ~HIN_HTTP_COMPRESS) | HIN_HTTP_GZIP;
    windowsBits |= 16;
  } else {
    hin_error ("useless zlib init");
    return -1;
  }
  int ret = deflateInit2 (&data->z, Z_BEST_COMPRESSION, Z_DEFLATED, windowsBits, MAX_MEM_LEVEL, Z_DEFAULT_STRATEGY);
  if (ret != Z_OK) {
    hin_error ("deflate init failed: %s", zError (ret));
    return -1;
  }
  httpd_request_chunked (http);
  return 0;
}


