
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <basic_pattern.h>

#include "hin.h"
#include "http/http.h"
#include "http/uri.h"

HIN_EXPORT const char * http_status_name (int nr) {
  switch (nr) {
  case 100: return "Continue";
  case 101: return "Switching Protocols";
  case 102: return "Processing";
  case 103: return "Early Hints";
  case 200: return "OK";
  case 201: return "Created";
  case 202: return "Accepted";
  case 203: return "Non-Authoritative Information";
  case 204: return "No Content";
  case 205: return "Reset Content";
  case 206: return "Partial Content";
  case 207: return "Multi-Status";
  case 208: return "Already Reported";
  case 300: return "Multiple Choice";
  case 301: return "Moved Permanently";
  case 302: return "Found";
  case 303: return "See Other";
  case 304: return "Not Modified";
  case 305: return "Use Proxy";
  case 306: return "Switch Proxy";
  case 307: return "Temporary Redirect";
  case 308: return "Permanent Redirect";
  case 400: return "Bad Request";
  case 401: return "Unauthorized";
  case 402: return "Payment Required";
  case 403: return "Forbidden";
  case 404: return "Not Found";
  case 405: return "Method Not Allowed";
  case 406: return "Not Acceptable";
  case 407: return "Proxy Authentication Required";
  case 408: return "Request Timeout";
  case 409: return "Conflict";
  case 410: return "Gone";
  case 411: return "Length Required";
  case 412: return "Precondition Failed";
  case 413: return "Payload Too Large";
  case 414: return "URI Too Long";
  case 415: return "Unsupported Media Type";
  case 416: return "Range Not Satisfiable";
  case 417: return "Expectation Failed";
  case 418: return "I'm a teapot";
  case 421: return "Misdirected Request";
  case 425: return "Too Early";
  case 426: return "Upgrade Required";
  case 428: return "Precondition Required";
  case 429: return "Too Many Requests";
  case 431: return "Request Header Fields Too Large";
  case 451: return "Unavailable For Legal Reasons";
  case 500: return "Server error";
  case 501: return "Not Implemented";
  case 502: return "Bad Gateway";
  case 503: return "Service Unavailable";
  case 504: return "Gateway Timeout";
  case 505: return "HTTP Version Not Supported";
  case 511: return "Network Authentication Required";

  default: return "Unknown";
  }
}

HIN_EXPORT const char * hin_http_method_name (int num) {
  switch (num) {
  case HIN_METHOD_GET: return "GET";
  case HIN_METHOD_HEAD: return "HEAD";
  case HIN_METHOD_OPTIONS: return "OPTIONS";

  case HIN_METHOD_POST: return "POST";

  case HIN_METHOD_PUT: return "PUT";
  case HIN_METHOD_DELETE: return "DELETE";
  case HIN_METHOD_CONNECT: return "CONNECT";
  case HIN_METHOD_TRACE: return "TRACE";

  case HIN_METHOD_COPY: return "COPY";
  case HIN_METHOD_LOCK: return "LOCK";
  case HIN_METHOD_MKCOL: return "MKCOL";
  case HIN_METHOD_MOVE: return "MOVE";
  case HIN_METHOD_PROPFIND: return "PROPFIND";
  case HIN_METHOD_PROPPATCH: return "PROPPATCH";
  case HIN_METHOD_UNLOCK: return "UNLOCK";
  }
  return NULL;
}

static int hin_http_method_parse (string_t * source) {
  int ret = 0;
  if (matchi_string_equal (source, "GET") > 0) {
    ret = HIN_METHOD_GET;
  } else if (matchi_string_equal (source, "POST") > 0) {
    ret = HIN_METHOD_POST;
  } else if (matchi_string_equal (source, "HEAD") > 0) {
    ret = HIN_METHOD_HEAD;
  } else if (matchi_string_equal (source, "OPTIONS") > 0) {
    ret = HIN_METHOD_OPTIONS;

  } else if (matchi_string_equal (source, "PUT") > 0) {
    ret = HIN_METHOD_PUT;
  } else if (matchi_string_equal (source, "DELETE") > 0) {
    ret = HIN_METHOD_DELETE;
  } else if (matchi_string_equal (source, "CONNECT") > 0) {
    ret = HIN_METHOD_CONNECT;
  } else if (matchi_string_equal (source, "TRACE") > 0) {
    ret = HIN_METHOD_TRACE;

  } else if (matchi_string_equal (source, "COPY") > 0) {
    ret = HIN_METHOD_COPY;
  } else if (matchi_string_equal (source, "LOCK") > 0) {
    ret = HIN_METHOD_LOCK;
  } else if (matchi_string_equal (source, "MKCOL") > 0) {
    ret = HIN_METHOD_MKCOL;
  } else if (matchi_string_equal (source, "MOVE") > 0) {
    ret = HIN_METHOD_MOVE;
  } else if (matchi_string_equal (source, "PROPFIND") > 0) {
    ret = HIN_METHOD_PROPFIND;
  } else if (matchi_string_equal (source, "PROPPATCH") > 0) {
    ret = HIN_METHOD_PROPPATCH;
  } else if (matchi_string_equal (source, "UNLOCK") > 0) {
    ret = HIN_METHOD_UNLOCK;

  } else {
    ret = HIN_METHOD_UNKNOWN;
  }
  return ret;
}

HIN_EXPORT int hin_http_parse_header_line (string_t * line, int * method, string_t * path, int * version) {
  string_t methods, paths, versions;
  if (match_string_equal (line, "(%a+) (["HIN_HTTP_PATH_ACCEPT"?#]+) HTTP/([%d.]+)", &methods, &paths, &versions) <= 0) return -1;

  if (method) {
    *method = hin_http_method_parse (&methods);
  }

  if (version) {
    if (match_string_equal (&versions, "1.1") > 0) {
      *version = 0x11;
    } else if (match_string_equal (&versions, "2") > 0) {
      *version = 0x20;
    } else if (match_string_equal (&versions, "1.0") > 0) {
      *version = 0x10;
    } else {
      *version = HIN_HTTP_VERSION_UNKNOWN;
    }
  }

  if (path) {
    *path = paths;
  }

  return 0;
}

static unsigned long int my_strtoul (const char* str, const char** endptr, int base) {
  int ch;
  const char * ptr = str;
  unsigned long num = 0;
  while (1) {
    ch = *ptr;
    if (ch >= '0' && ch <= '9') ch = ch - '0';
    else if (ch >= 'A' && ch <= 'Z') ch = 10 + ch - 'A';
    else if (ch >= 'a' && ch <= 'z') ch = 10 + ch - 'a';
    else break;
    if (ch >= base) break;
    ptr++;
    num = num * base + ch;
  }
  if (endptr) *endptr = ptr;
  return num;
}

HIN_EXPORT char * hin_parse_url_encoding (string_t * source, uint32_t flags) {
  const char * p1 = source->ptr;
  const char * max = source->ptr + source->len;

  char * new = halloc_q (source->len + 1);
  char * p2 = new;

  while (1) {
    if (p1 >= max) break;
    if (*p1 == '%') {
      p1++;
      int utf = my_strtoul (p1, &p1, 16);
      if (utf > 0x1F)
        *p2 = utf;
    } else {
      *p2 = *p1;
      p1++;
    }
    p2++;
  }
  *p2 = '\0';

  //printf ("old '%.*s'\n", source->len, source->ptr);
  //printf ("new '%s'\n", new);

  return new;
}

HIN_EXPORT int hin_httpd_parse_path (httpd_client_t * http, string_t * source) {
  if (http->path) return 0;
  if (source == NULL) return 400;

  string_t line, path, temp;
  int method, version;
  temp = *source;
  if ((hin_find_line (&temp, &line) == 0) || hin_http_parse_header_line (&line, &method, &path, &version) < 0) {
    hin_weird_error (678768312);
    return 400;
  }

  hin_uri_t uri;
  memset (&uri, 0, sizeof uri);
  if (hin_parse_uri (path.ptr, path.len, &uri) < 0) {
    hin_error ("parsing uri");
    return 400;
  }

  line = uri.path;
  char * parsed = hin_parse_url_encoding (&line, 0);

  http->path = parsed;
  http->method = method;
  if (uri.query.len > 0)
    http->query = strndup (uri.query.ptr - 1, uri.query.len + 1);

  switch (version) {
  case 0x10: http->peer_flags |= HIN_HTTP_VER10; break;
  case 0x11: http->peer_flags |= HIN_HTTP_VER11; break;
  //case 0x20: break;
  default:
    http->peer_flags |= HIN_HTTP_VERSION;
    return 505;
  break;
  }
  // 405 resource doesn't support this method
  // 501 server doesn't support this method
  if (method == HIN_METHOD_UNKNOWN) return 501;

  *source = temp;

  return 0;
}

HIN_EXPORT int hin_http_version (uint32_t flags) {
  if (flags & HIN_HTTP_VER11) return 0x11;
  if (flags & HIN_HTTP_VER10) return 0x10;
  //if (flags & HIN_HTTP_VER20) return 0x20;
  if ((flags & HIN_HTTP_VERSION) == HIN_HTTP_VERSION)
    return HIN_HTTP_VERSION_UNKNOWN;
  return 0;
}

HIN_EXPORT const char * hin_http_version_name (uint32_t version) {
  switch (version) {
  case 0x10: return "1.0";
  case 0x11: return "1.1";
  case 0x20: return "2";
  }
  return NULL;
}

HIN_EXPORT int hin_find_line (string_t * source, string_t * line) {
  char * ptr = source->ptr;
  char * max = ptr + source->len;
  char * last = ptr;
  line->ptr = ptr;
  line->len = 0;
  for (;ptr <= max; ptr++) {
    switch (*ptr) {
    case '\n':
      line->ptr = source->ptr;
      line->len = last - source->ptr;
      ptr++;
      source->len = max - ptr;
      source->ptr = ptr;
      return 1;
    break;
    case '\r':
      last = ptr;
    break;
    case '\0':
      return -1;
    break;
    default:
      last = ptr + 1;
    break;
    }
  }
  return 0;
}

HIN_EXPORT int hin_parse_uri (const char * url, int len, hin_uri_t * info) {
  if (len <= 0) len = strlen (url);
  string_t c;
  memset (info, 0, sizeof (*info));

  c.ptr = (char*)url;
  c.len = len;
  int err = 0;

  if (match_string (&c, "(https)://", &info->scheme) > 0) {
    info->https = 1;
  } else if (match_string (&c, "(http)://", &info->scheme) > 0) {
    info->https = 0;
  } else if (match_string (&c, "([%w+-.]+)://", &info->scheme) > 0) {
  } else {
  }// generic scheme is letter+number, +, -, .
  if (match_string (&c, "([%w.-_]+)", &info->host) < 0) {
    //return -1;
    memset (&info->host, 0, sizeof (string_t));
  }
  if (match_string (&c, ":(%d+)", &info->port) > 0) {}
  if ((err = match_string (&c, "(["HIN_HTTP_PATH_ACCEPT"]+)", &info->path)) < 0) {
    hin_error ("no path");
    return -1;
  }
  if (match_string (&c, "%?(["HIN_HTTP_PATH_ACCEPT"]+)", &info->query) > 0) {}
  if (match_string (&c, "%#(["HIN_HTTP_PATH_ACCEPT"]+)", &info->fragment) > 0) {}
  int used = (uintptr_t)c.ptr - (uintptr_t)url;
  info->all.ptr = (char*)url;
  info->all.len = used;
  //*uri = c;
  if (0) {
    hin_debug ("url '%.*s'\nhost '%.*s'\nport '%.*s'\npath '%.*s'\nquery '%.*s'\nfragment '%.*s'\nhttps %d\n",
    len, url,
    (int)info->host.len, info->host.ptr,
    (int)info->port.len, info->port.ptr,
    (int)info->path.len, info->path.ptr,
    (int)info->query.len, info->query.ptr,
    (int)info->fragment.len, info->fragment.ptr,
    info->https
    );
  }

  return used;
}

time_t hin_date_str_to_time (const char * str) {
  struct tm tm;
  time_t t;
  if (strptime (str, "%a, %d %b %Y %X GMT", &tm) == NULL) {
    hin_error ("can't strptime '%s'", str);
    return 0;
  }
  tm.tm_isdst = -1; // Not set by strptime(); tells mktime() to determine whether daylight saving time is in effect
  t = mktime (&tm);
  if (t == -1) {
    hin_error ("can't mktime");
    return 0;
  }
  return t;
}

