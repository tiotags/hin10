
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "hin.h"
#include "http/http.h"

#include "http_utils_internal.h"

int hin_pipe_copy_chunked (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush) {
  hin_client_t * client = (hin_client_t*)pipe->parent;

  if (buffer->debug & HNDBG_HTTP_FILTER)
    hin_debug ("http(d) %d chunked num %d flush %d\n", client->sockfd, num, flush);

  hin_buffer_t * buf = hin_buffer_alloc (num + 50);
  buf->parent = (void*)pipe;
  buf->count = 0;
  buf->ssl = pipe->out.ssl;
  buf->debug = buffer->debug;

  //buffer->count = num;
  if (num > 0) {
    hin_header (buf, "%x\r\n", num);

    memcpy (buf->ptr + buf->count, buffer->ptr, num);
    buf->count += num;

    hin_header (buf, "\r\n");
  }

  if (flush) {
    hin_header (buf, "0\r\n\r\n");
  }

  hin_pipe_append_raw (pipe, buf);

  return 1;
}

HIN_EXPORT int httpd_request_chunked (httpd_client_t * http) {
  if ((http->peer_flags & HIN_HTTP_VERSION) == HIN_HTTP_VER10) {
    http->peer_flags &= ~(HIN_HTTP_KEEPALIVE | HIN_HTTP_CHUNKED);
  } else {
    if (http->peer_flags & HIN_HTTP_KEEPALIVE) {
      http->peer_flags |= HIN_HTTP_CHUNKED;
    }
    return 1;
  }
  return 0;
}

int httpd_pipe_set_chunked (httpd_client_t * http, hin_pipe_t * pipe) {
  if (http->compress_data && http->peer_flags & HIN_HTTP_COMPRESS) {
    httpd_request_chunked (http);
    pipe->read_callback = hin_pipe_copy_deflate;
  } else if ((http->peer_flags & HIN_HTTP_CHUNKED)) {
    if (httpd_request_chunked (http)) {
      pipe->read_callback = hin_pipe_copy_chunked;
    }
  }
  return 0;
}

static int httpd_client_pipe_error_callback (hin_pipe_t * pipe, int err) {
  hin_error ("httpd %d pipe: %s", pipe->out.fd, strerror (-err));
  httpd_client_shutdown (pipe->parent);
  return 0;
}

HIN_EXPORT int httpd_pipe_set_http11_response_options (httpd_client_t * http, hin_pipe_t * pipe) {
  pipe->out.fd = http->c.sockfd;
  pipe->out.flags = HIN_SOCKET | (http->c.flags & HIN_SSL);
  pipe->out.ssl = http->c.ssl;
  pipe->out.pos = 0;
  pipe->parent = http;
  pipe->debug = http->debug;
  pipe->out_error_callback = httpd_client_pipe_error_callback;

  httpd_pipe_set_idle_protections (http, pipe);

  if (http->peer_flags & HIN_HTTP_COMPRESS) {
    if (httpd_compress_setup (http) < 0) {
      return -1;
    }
  }

  if (http->status == 304 || http->method == HIN_METHOD_HEAD) {
    http->peer_flags &= ~(HIN_HTTP_CHUNKED | HIN_HTTP_COMPRESS);
    pipe->sz = 0;
  } else {
    httpd_pipe_set_chunked (http, pipe);
  }

  return 0;
}

int httpd_pipe_upload_chunked (httpd_client_t * http, hin_pipe_t * pipe) {
  if (http->disable & HIN_HTTP_CHUNKED_UPLOAD) return 0;
  if (http->peer_flags & HIN_HTTP_CHUNKED_UPLOAD) {
    pipe->decode_callback = hin_pipe_decode_chunked;
    pipe->read_callback = hin_pipe_copy_chunked;
  }
  return 0;
}

