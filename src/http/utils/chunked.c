
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <basic_pattern.h>

#include "hin.h"
#include "http_utils_internal.h"

typedef struct {
  off_t chunk_sz;
  int left_over;
} hin_pipe_chunked_decode_t;

static void hin_pipe_decode_prepare_half_read (hin_pipe_t * pipe, hin_buffer_t * buffer, int left, int num) {
  hin_pipe_chunked_decode_t * decode = pipe->extra;
  decode->left_over = left;
  hin_debug ("chunk needs more space left %d num %d\n", left, num);
  buffer->count = buffer->sz;
  if (left > 0) {
    memmove (buffer->ptr, buffer->ptr + num - left, left);
    buffer->count -= left;
  }
  hin_debug ("left in buffer is '%.*s'\n", left, buffer->ptr);
}

int hin_pipe_decode_chunked (hin_pipe_t * pipe, hin_buffer_t * buffer, int num, int flush) {
  hin_pipe_chunked_decode_t * decode = pipe->extra;
  if (decode == NULL) {
    decode = halloc_z (sizeof (*decode));
    pipe->extra = decode;
  }
  string_t source, orig, param1;
  orig.ptr = buffer->ptr - decode->left_over;
  orig.len = num + decode->left_over;
  source = orig;

  if (pipe->debug & HNDBG_HTTP_FILTER)
    hin_debug ("pipe %d>%d decode chunk sz %d left %d %s\n", pipe->in.fd, pipe->out.fd, num, decode->left_over, flush ? "flush" : "cont");
  while (1) {
    if (pipe->debug & HNDBG_HTTP_FILTER)
      hin_debug ("  chunk sz left %lld\n", (long long)decode->chunk_sz);
    if (decode->chunk_sz > 0) {
      uintptr_t consume = decode->chunk_sz;
      if (consume > source.len) consume = source.len;
      if (pipe->debug & HNDBG_HTTP_FILTER)
        hin_debug ("  chunk consume %lld\n", (long long)consume);
      hin_buffer_t * buf = hin_pipe_get_buffer (pipe, consume);
      memcpy (buf->ptr, source.ptr, consume);
      if (pipe->read_callback (pipe, buf, consume, 0))
        hin_buffer_clean (buf);
      if (decode->chunk_sz > (off_t)consume) {
        // want more;
        decode->chunk_sz -= consume;
        return 1;
      }
      source.ptr += consume;
      source.len -= consume;
      decode->chunk_sz -= consume;
      if (source.len < 2) {
        hin_pipe_decode_prepare_half_read (pipe, buffer, source.len-2, num);
        return 1;
      }
      if (match_string (&source, "\r\n") < 0) {
        hin_error ("chunk decode format error");
        return -1;
      }
    }
    int err = 0;
    if ((err = match_string (&source, "(%x+)\r\n", &param1)) <= 0) {
      // save stuff
      if (source.len < 10) {
        hin_pipe_decode_prepare_half_read (pipe, buffer, source.len, num);
        return 1;
      }
      hin_error ("chunk decode couldn't find in '%.*s'", (int)(source.len > 20 ? 20 : source.len), source.ptr);
      return -1;
    }
    decode->chunk_sz = strtol (param1.ptr, NULL, 16);
    if (pipe->debug & HNDBG_HTTP_FILTER)
      hin_debug ("  chunk sz %lld found\n", (long long)decode->chunk_sz);
    if (decode->chunk_sz == 0 && param1.len > 0) {
      int ret = pipe->read_callback (pipe, buffer, 0, 1);
      pipe->in.flags |= (HIN_DONE|HIN_INACTIVE);
      if (ret) {}
      return 1;
    }
  }
  return 0;
}

